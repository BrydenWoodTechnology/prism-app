﻿using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.UIElements;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A Unity Event class for the change of the layout
    /// </summary>
    [System.Serializable]
    public class LayoutChanged : UnityEvent<ProceduralApartmentLayout>
    {

    }

    /// <summary>
    /// A MonoBehaviour component for the Procedural Apartment Layout
    /// </summary>
    public class ProceduralApartmentLayout : MonoBehaviour
    {
        #region Public Properties
        public TextAsset roomData;
        public TextAsset verticalModulesData;
        public TextAsset horizontalModulesData;
        public Text roomIssue;
        public GameObject proceduralRoomPrefab;
        public GameObject maxLayoutPrefab;
        public GameObject minLayoutPrefab;
        public GameObject minMaxRenderer;
        public Text viewSubTitle;
        public SelectedInfoDisplay selectedInfoDisplay;
        public Vector3 origin;
        public float wallWidth = 0.15f;
        public float width = 8f;
        public float height = 6.8f;

        public float partyWall = 0.3f;
        public float exteriorWall = 0.45f;
        public float corridorWall = 0.2f;
        public string content;
        public float modulesScale = 1f;

        public string apartmentType = "1b1p";

        public bool initializeOnStart = false;
        public Gradient panelsGradient;
        public float totalArea;
        public float NIA;

        public bool hasLeft = true;
        public bool hasRight = true;

        [Space(10)]
        [Header("Rooms included:")]
        public bool[] roomInclusion;// = new bool[24];
        public string[] roomNames;

        public List<Color> roomColors;
        public List<string> types;
        public float maxPanelWidth
        {
            get
            {
                return Standards.TaggedObject.ConstructionFeatures["PanelLength"];
            }
        }
        public List<ProceduralRoom> rooms { get; set; }
        public List<GameObject> verticalModules { get; set; }
        public List<GameObject> panels { get; set; }
        public Transform verticalModulesParent;
        public Transform panelsParent;
        public Transform roomParent;
        public Transform lineLoadsParent;
        [HideInInspector]
        public float totalHeight = 3;
        public ManufacturingSystem manufacturingSystem { get; set; }
        public bool readOnly { get; set; }
        public List<List<float>> lengths { get; set; }
        public List<List<float>> originalLengths { get; set; }
        public bool hasCorridor { get; set; }
        public float corridorWidth { get; set; }

        public float area { get; set; }
        public bool issue;
        public float outterHeight { get; set; }
        public float outterWidth { get; set; }
        public float maxOutterWidth { get; set; }
        public float minOutterWidth { get; set; }
        public double AmenitySpace
        {
            get
            {
                double area = 0;

                if (rooms != null && rooms.Count > 0)
                {
                    for (int i = 0; i < rooms.Count; i++)
                    {
                        if (rooms[i].name == "AILR" || rooms[i].name == "AILRR" || rooms[i].name == "ALRR" || rooms[i].name == "ALR")
                        {
                            area += rooms[i].polygon.Area;
                        }
                    }
                }

                return area;
            }
        }

        public List<GameObject> corridorLineLoads;
        public List<GameObject> facadeLineLoads;

        public float dbVal { get { return Standards.TaggedObject.customVals[apartmentType][0]; } }
        public float pbVal { get { return Standards.TaggedObject.customVals[apartmentType][1]; } }
        public float baVal { get { return Standards.TaggedObject.customVals[apartmentType][2]; } }
        public float baaVal { get { return Standards.TaggedObject.customVals[apartmentType][3]; } }
        public float wcVal { get { return Standards.TaggedObject.customVals[apartmentType][4]; } }
        public float wccVal { get { return Standards.TaggedObject.customVals[apartmentType][5]; } }
        public float dbbVal { get { return Standards.TaggedObject.customVals[apartmentType][6]; } }
        public float pbbVal { get { return Standards.TaggedObject.customVals[apartmentType][7]; } }
        public float psbVal { get { return Standards.TaggedObject.customVals[apartmentType][8]; } }
        public float minAmenity { get { return Standards.TaggedObject.MinimumAmenitiesArea[apartmentType] + 1; } }
        #endregion

        #region Private Properties
        private Dictionary<string, List<string>> edges { get; set; }
        private List<string> origins { get; set; }
        private float m_width { get; set; }
        private float m_height { get; set; }
        private LineRenderer outline { get; set; }
        private GameObject maxLayout { get; set; }
        private GameObject minLayout { get; set; }
        #endregion

        #region Events

        #endregion

        #region MonoBehaviour Methods

        // Use this for initialization
        void Start()
        {
            if (initializeOnStart)
            {
                Initialize(roomData.text);
            }
            modulesScale = 1;
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Initializes the Instance
        /// </summary>
        /// <param name="content">The txt content to be used for initialization</param>
        /// <param name="isExt">Whether the apartment is on an external corner</param>
        public void Initialize(string content, bool isExt = true, bool getMax = false)
        {
            this.content = content;

            partyWall = Standards.TaggedObject.ConstructionFeatures["PartyWall"];
            exteriorWall = Standards.TaggedObject.ConstructionFeatures["ExteriorWall"];
            corridorWall = Standards.TaggedObject.ConstructionFeatures["CorridorWall"];

            GetEdgesFromContent();
            GetLengthsFromEdges();
            ResetLineLoads();
            GenerateRooms();
            GenerateVolumetricModules();
            GeneratePanels();
            if (getMax)
            {
                if (viewSubTitle != null) viewSubTitle.text = "Total area = " + Math.Round(NIA) + "m\xB2";
                GenerateMaxLayout();
                GenerateMinimumLayout();
                if (selectedInfoDisplay != null)
                {
                    selectedInfoDisplay.SetSelected(this);
                }
            }
            transform.SetLayerToChildren(16);
            if (selectedInfoDisplay != null)
            {
                selectedInfoDisplay.SetSelected(this);
            }

            EvaluateLineLoads();
            Standards.TaggedObject.EvaluateStandardRooms();
            Standards.TaggedObject.EvaluateAmenitiesArea();
            Standards.TaggedObject.EvaluateStructuralWalls();
        }

        /// <summary>
        /// Returns the information of the apartment layout
        /// </summary>
        /// <returns>Array of Strings</returns>
        public string[] GetInfoText()
        {
            string title = string.Format("{0}\r\n", apartmentType);

            string info = "";
            info += string.Format("Apartment id = {0}\r\n", "N/A");
            info += string.Format("Apartment type = {0}\r\n", apartmentType);
            info += string.Format("Apartment area = {0}m\xB2\r\n", Math.Round(NIA));
            int num = 0;
            if (apartmentType != "Studio")
            {
                num = (int)Char.GetNumericValue(apartmentType[apartmentType.Length - 2]);
            }
            else
            {
                num = 2;
            }
            info += string.Format("Number of people = {0}\r\n", num);

            return new string[2] { title, info };
        }

        /// <summary>
        /// Resets the line-loads of the apartment layout
        /// </summary>
        public void ResetLineLoads()
        {
            if (facadeLineLoads != null)
            {
                for (int i = 0; i < facadeLineLoads.Count; i++)
                {
                    if (facadeLineLoads[i] != null)
                        DestroyImmediate(facadeLineLoads[i]);
                }
                facadeLineLoads = new List<GameObject>();
            }

            if (corridorLineLoads != null)
            {
                for (int i = 0; i < corridorLineLoads.Count; i++)
                {
                    if (corridorLineLoads[i] != null)
                        DestroyImmediate(corridorLineLoads[i]);
                }
                corridorLineLoads = new List<GameObject>();
            }
        }

        /// <summary>
        /// Adds a line-load to the apartment layout
        /// </summary>
        /// <param name="lineLoad">The line-load to be added</param>
        /// <param name="side">Which side the line load is on (corridor or facade)</param>
        public void AddLineLoad(GameObject lineLoad, int side)
        {
            if (facadeLineLoads == null)
            {
                facadeLineLoads = new List<GameObject>();
            }

            if (corridorLineLoads == null)
            {
                corridorLineLoads = new List<GameObject>();
            }

            if (side == 1)
                facadeLineLoads.Add(lineLoad);
            else
                corridorLineLoads.Add(lineLoad);

            facadeLineLoads = facadeLineLoads.OrderBy(x => transform.InverseTransformPoint(x.transform.position).x).ToList();
            corridorLineLoads = corridorLineLoads.OrderBy(x => transform.InverseTransformPoint(x.transform.position).x).ToList();
        }

        /// <summary>
        /// Evaluates the line-loads
        /// </summary>
        public void EvaluateLineLoads()
        {
            for (int i = 0; i < facadeLineLoads.Count; i++)
            {
                var xVal = transform.InverseTransformPoint(facadeLineLoads[i].transform.position).x;
                var maxSpan = Standards.TaggedObject.ConstructionFeatures["LineSpan"];
                if (i == 0)
                {
                    if (xVal > maxSpan) facadeLineLoads[i].GetComponent<LineLoad>().MarkAsRed(true);
                    else facadeLineLoads[i].GetComponent<LineLoad>().MarkAsRed(false);
                }
                else
                {
                    var prevXVal = transform.InverseTransformPoint(facadeLineLoads[i - 1].transform.position).x;
                    if ((xVal - prevXVal) > maxSpan)
                    {
                        facadeLineLoads[i].GetComponent<LineLoad>().MarkAsRed(true);
                    }
                    else
                    {
                        facadeLineLoads[i].GetComponent<LineLoad>().MarkAsRed(false);
                    }
                }

                if (i == facadeLineLoads.Count - 1)
                {
                    if (outterWidth - xVal > maxSpan)
                        facadeLineLoads[i].GetComponent<LineLoad>().MarkAsRed(true);
                    else
                        facadeLineLoads[i].GetComponent<LineLoad>().MarkAsRed(false);
                }
            }

            for (int i = 0; i < corridorLineLoads.Count; i++)
            {
                var xVal = transform.InverseTransformPoint(corridorLineLoads[i].transform.position).x;
                var maxSpan = Standards.TaggedObject.ConstructionFeatures["LineSpan"];
                if (i == 0)
                {
                    if (xVal > maxSpan) corridorLineLoads[i].GetComponent<LineLoad>().MarkAsRed(true);
                    else corridorLineLoads[i].GetComponent<LineLoad>().MarkAsRed(false);
                }
                else
                {
                    var prevXVal = transform.InverseTransformPoint(corridorLineLoads[i - 1].transform.position).x;
                    if ((xVal - prevXVal) > maxSpan)
                    {
                        corridorLineLoads[i].GetComponent<LineLoad>().MarkAsRed(true);
                    }
                    else
                    {
                        corridorLineLoads[i].GetComponent<LineLoad>().MarkAsRed(false);
                    }
                }

                if (i == corridorLineLoads.Count - 1)
                {
                    if (outterWidth - xVal > maxSpan)
                        corridorLineLoads[i].GetComponent<LineLoad>().MarkAsRed(true);
                    else
                        corridorLineLoads[i].GetComponent<LineLoad>().MarkAsRed(false);
                }
            }
        }

        /// <summary>
        /// Initializes the Instance
        /// </summary>
        /// <param name="showMinMax">Whether the maximum and minimum areas should be visualized</param>
        /// <param name="isExt">Whether the apartment is on an external corner</param>
        public void Initialize(bool showMinMax = true, bool isExt = true)
        {
            Initialize(roomData.text, isExt, showMinMax);
        }

        /// <summary>
        /// Re-Initializes the Instance
        /// </summary>
        public void ReInitialize()
        {
            GenerateRooms();
            GenerateVolumetricModules();
            GeneratePanels();
            transform.SetLayerToChildren(16);
        }

        /// <summary>
        /// Sets the line loads for the apartment layout
        /// </summary>
        public void SetLineLoads()
        {
            if (lineLoadsParent == null)
            {
                lineLoadsParent = new GameObject("LineLoadsParent").transform;
                lineLoadsParent.SetParent(transform);
                lineLoadsParent.localPosition = Vector3.zero;
                lineLoadsParent.localEulerAngles = Vector3.zero;
                lineLoadsParent.localScale = Vector3.one;
            }

            List<string> lineLoadRooms;
            if (Standards.TaggedObject.ValidLineLoads.TryGetValue(apartmentType, out lineLoadRooms))
            {
                for (int i = 0; i < lineLoadRooms.Count; i++)
                {
                    for (int j = 0; j < rooms.Count; j++)
                    {
                        if (rooms[j].gameObject.name == lineLoadRooms[i])
                        {
                            var loads = rooms[j].GetLineLoads(totalHeight);
                            for (int k = 0; k < loads.Count; k++)
                            {
                                loads[k].transform.SetParent(lineLoadsParent);
                            }
                        }
                    }
                }

                EvaluateLineLoads();
            }


            //var obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
            //obj.GetComponent<MeshRenderer>().material = Resources.Load("Materials/AlignCorrectMaterial") as Material;
            //obj.transform.SetParent(lineLoadsParent);
            //obj.transform.localScale = new Vector3(outterWidth, totalHeight * 0.98f, 0.03f);
            //obj.transform.localPosition = new Vector3(obj.transform.localScale.x / 2.0f, totalHeight / 2.0f, outterHeight + obj.transform.localScale.z / 2.0f);

            //var obj2 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            //obj2.GetComponent<MeshRenderer>().material = Resources.Load("Materials/AlignCorrectMaterial") as Material;
            //obj2.transform.SetParent(lineLoadsParent);
            //obj2.transform.localScale = new Vector3(outterWidth, totalHeight * 0.98f, 0.03f);
            //obj2.transform.localPosition = new Vector3(obj2.transform.localScale.x / 2.0f, totalHeight / 2.0f, 0 + obj2.transform.localScale.z / 2.0f);

            //var obj3 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            //obj3.GetComponent<MeshRenderer>().material = Resources.Load("Materials/AlignCorrectMaterial") as Material;
            //obj3.transform.SetParent(lineLoadsParent);
            //obj3.transform.localScale = new Vector3(0.03f, totalHeight * 0.98f, outterHeight);
            //obj3.transform.localPosition = new Vector3(obj3.transform.localScale.x / 2.0f, totalHeight / 2.0f, obj3.transform.localScale.z / 2.0f);

            //var obj4 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            //obj4.GetComponent<MeshRenderer>().material = Resources.Load("Materials/AlignCorrectMaterial") as Material;
            //obj4.transform.SetParent(lineLoadsParent);
            //obj4.transform.localScale = new Vector3(0.03f, totalHeight * 0.98f, outterHeight);
            //obj4.transform.localPosition = new Vector3(outterWidth - obj4.transform.localScale.x / 2.0f, totalHeight / 2.0f, obj4.transform.localScale.z / 2.0f);
        }

        /// <summary>
        /// Gets the Line Loads for the apartment layout
        /// </summary>
        public void GetLineLoads()
        {
            if (lineLoadsParent == null)
            {
                lineLoadsParent = new GameObject("LineLoadsParent").transform;
                lineLoadsParent.SetParent(transform);
                lineLoadsParent.localPosition = Vector3.zero;
                lineLoadsParent.localEulerAngles = Vector3.zero;
                lineLoadsParent.localScale = Vector3.one;
            }
            for (int i = 0; i < rooms.Count; i++)
            {
                var loads = rooms[i].GetLineLoads();
                for (int j = 0; j < loads.Count; j++)
                {
                    loads[j].transform.localScale = new Vector3(loads[j].transform.localScale.x, totalHeight, loads[j].transform.localScale.z);
                    loads[j].transform.localPosition = new Vector3(loads[j].transform.localPosition.x, totalHeight / 2.0f, loads[j].transform.localPosition.z);
                    loads[j].transform.SetParent(lineLoadsParent);
                }
            }
        }

        /// <summary>
        /// Turns the Line Loads on or off
        /// </summary>
        /// <param name="show">Toggle</param>
        public void ToggleLineLoads(bool show)
        {
            for (int i = 0; i < rooms.Count; i++)
            {
                rooms[i].ToggleLineLoads(show);
            }
        }

        /// <summary>
        /// Sets the manufacturing system
        /// </summary>
        /// <param name="manufacturingSystem">The new manufacturing system</param>
        public void SetManufacturingSystem(ManufacturingSystem manufacturingSystem)
        {
            this.manufacturingSystem = manufacturingSystem;
            switch (manufacturingSystem)
            {
                case ManufacturingSystem.Off:
                    verticalModulesParent.gameObject.SetActive(false);
                    panelsParent.gameObject.SetActive(false);
                    break;
                case ManufacturingSystem.VolumetricModules:
                    verticalModulesParent.gameObject.SetActive(true);
                    panelsParent.gameObject.SetActive(false);
                    break;
                case ManufacturingSystem.Panels:
                    verticalModulesParent.gameObject.SetActive(false);
                    panelsParent.gameObject.SetActive(true);
                    break;
                case ManufacturingSystem.LineLoads:
                    verticalModulesParent.gameObject.SetActive(false);
                    panelsParent.gameObject.SetActive(false);
                    break;
            }
            ToggleLineLoads(manufacturingSystem == ManufacturingSystem.LineLoads);
        }

        /// <summary>
        /// Toggles through the manufacturing systems
        /// </summary>
        public void ToggleManufacturingSystem()
        {
            switch (manufacturingSystem)
            {
                case ManufacturingSystem.Off:
                    verticalModulesParent.gameObject.SetActive(false);
                    panelsParent.gameObject.SetActive(false);
                    break;
                case ManufacturingSystem.VolumetricModules:
                    verticalModulesParent.gameObject.SetActive(true);
                    panelsParent.gameObject.SetActive(false);
                    break;
                case ManufacturingSystem.Panels:
                    verticalModulesParent.gameObject.SetActive(false);
                    panelsParent.gameObject.SetActive(true);
                    break;
                case ManufacturingSystem.LineLoads:
                    verticalModulesParent.gameObject.SetActive(false);
                    panelsParent.gameObject.SetActive(false);
                    break;
            }
            ToggleLineLoads(manufacturingSystem == ManufacturingSystem.LineLoads);
        }

        /// <summary>
        /// Resets the apartment layout
        /// </summary>
        /// <returns>IEnumerator</returns>
        public IEnumerator ResetLayout()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
            ShowOutline(false);
            ResetLineLoads();
            yield return new WaitForEndOfFrame();
        }

        /// <summary>
        /// Updates the amenities of the apartment layout
        /// </summary>
        /// <param name="roomId">The unique id of the room</param>
        /// <param name="length">The length of the room</param>
        public void UpdateAmenities(int roomId, float length)
        {
            StartCoroutine(UpdateBalconiesCoroutine(roomId, length));
        }

        /// <summary>
        /// Sets the total width of the apartment layout
        /// </summary>
        /// <param name="width">The total width</param>
        public void SetWidth(float width)
        {
            this.width = width;
            GetEdgesFromContent();
            GetLengthsFromEdges();
            UpdateRooms();
        }

        /// <summary>
        /// Sets the total depth of the apartment
        /// </summary>
        /// <param name="depth">The total depth</param>
        public void SetDepth(float depth)
        {
            this.height = depth;
            GetEdgesFromContent();
            GetLengthsFromEdges();
            UpdateRooms();
        }

        /// <summary>
        /// Sets the width of the interior walls
        /// </summary>
        /// <param name="wallWidth">The width of the interior walls</param>
        public void SetWall(float wallWidth)
        {
            this.wallWidth = wallWidth;
            GetEdgesFromContent();
            GetLengthsFromEdges();
            UpdateRooms();
        }

        /// <summary>
        /// Sets the rooms of the apartment layout
        /// </summary>
        /// <param name="roomBools">A list of which rooms should be included</param>
        public void SetRooms(bool[] roomBools)
        {
            for (int i = 0; i < roomInclusion.Length; i++)
            {
                roomInclusion[i] = roomBools[i];
            }
        }

        /// <summary>
        /// Sets the rooms of the apartment layout
        /// </summary>
        /// <param name="has">Whether the layout has rooms or not</param>
        public void SetRooms(bool has)
        {
            for (int i = 0; i < roomInclusion.Length; i++)
            {
                roomInclusion[i] = has;
            }
        }

        /// <summary>
        /// Returns the GameObject with the combined meshes of the rooms
        /// </summary>
        /// <param name="type">The type of the apartment layout</param>
        /// <returns>GameObject</returns>
        public GameObject GetCombinedMesh(string type)
        {
            GameObject gObj = new GameObject(type);
            gObj.AddComponent<MeshFilter>();
            gObj.AddComponent<MeshRenderer>().material = Resources.Load("Materials/VertexLit") as Material;

            Mesh m = new Mesh();
            m.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

            List<CombineInstance> instances = new List<CombineInstance>();

            for (int i = 0; i < rooms.Count; i++)
            {
                instances.Add(new CombineInstance() { mesh = rooms[i].polygon.GetComponent<MeshFilter>().sharedMesh, transform = rooms[i].transform.localToWorldMatrix });
                instances.Add(new CombineInstance() { mesh = rooms[i].polygon.extrusion.GetComponent<MeshFilter>().sharedMesh, transform = rooms[i].transform.localToWorldMatrix });
            }

            m.CombineMeshes(instances.ToArray());
            m.RecalculateNormals();
            m.RecalculateBounds();

            return gObj;
        }

        /// <summary>
        /// Combines the meshes of the rooms
        /// </summary>
        /// <param name="type">The type of the apartment layout</param>
        public void CombineRoomsMeshes(string type)
        {
            for (int i = 0; i < roomParent.childCount; i++)
            {
                Destroy(roomParent.GetChild(i).gameObject);
            }

            GameObject gObj = new GameObject(type);
            gObj.transform.SetParent(transform);
            gObj.AddComponent<MeshFilter>();
            gObj.AddComponent<MeshRenderer>().material = Resources.Load("Materials/VertexLit") as Material;

            Mesh m = new Mesh();
            m.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

            List<CombineInstance> instances = new List<CombineInstance>();

            for (int i = 0; i < rooms.Count; i++)
            {
                instances.Add(new CombineInstance() { mesh = rooms[i].polygon.GetComponent<MeshFilter>().sharedMesh, transform = rooms[i].transform.localToWorldMatrix });
                instances.Add(new CombineInstance() { mesh = rooms[i].polygon.extrusion.GetComponent<MeshFilter>().sharedMesh, transform = rooms[i].transform.localToWorldMatrix });
            }

            m.CombineMeshes(instances.ToArray());
            m.RecalculateNormals();
            m.RecalculateBounds();

            gObj.GetComponent<MeshFilter>().sharedMesh = m;

            gObj.transform.localEulerAngles = Vector3.zero;
            gObj.transform.localPosition = Vector3.zero;
        }

        /// <summary>
        /// Toggles the outline of the apartment
        /// </summary>
        /// <param name="show">Toggle</param>
        public void ShowOutline(bool show)
        {
            if (show)
            {
                if (outline == null)
                {
                    outline = new GameObject("Outline").AddComponent<LineRenderer>();
                    outline.gameObject.layer = gameObject.layer;
                    outline.transform.SetParent(transform);
                    outline.transform.localPosition = Vector3.zero;
                    outline.transform.localEulerAngles = Vector3.zero;
                    outline.material = new Material(Shader.Find("Standard"));
                    outline.material.color = Color.black;
                    outline.loop = true;
                    outline.endWidth = 0.03f;
                    outline.startWidth = 0.03f;
                }

                var sideOffset = (partyWall - wallWidth);
                var topOffset = (exteriorWall) + (corridorWall) - wallWidth;
                outline.useWorldSpace = false;
                outline.positionCount = 5;
                outline.SetPosition(0, new Vector3(0, 0, 0));
                outline.SetPosition(1, new Vector3(0, 0, outterHeight));
                outline.SetPosition(2, new Vector3(outterWidth, 0, outterHeight));
                outline.SetPosition(3, new Vector3(outterWidth, 0, 0));
                outline.SetPosition(4, new Vector3(0, 0, 0));
            }
            else
            {
                if (outline != null)
                {
                    DestroyImmediate(outline.gameObject);
                    outline = null;
                }
            }
        }

        /// <summary>
        /// Checks if there is an issue with the Line Load panels
        /// </summary>
        /// <returns>Boolean</returns>
        public bool CheckIfPanelIssues()
        {
            int counter = 0;
            for (int i = 0; i < rooms.Count; i++)
            {
                if (rooms[i].CheckIfPanelIssues())
                {
                    counter++;
                }
            }
            return counter != 0;
        }
        #endregion

        #region Private Methods
        private IEnumerator UpdateBalconiesCoroutine(int roomId, float length)
        {
            if (Standards.TaggedObject.balconies == Balconies.External)
            {
                int indexOfBalcony = -1;
                if (Standards.TaggedObject.amenitiesIndices["Exterior"].TryGetValue(roomId, out indexOfBalcony))
                {
                    roomInclusion[indexOfBalcony] = !roomInclusion[indexOfBalcony];
                    if (!roomInclusion[indexOfBalcony])
                    {
                        //Standards.TaggedObject.amenitiesAreas[apartmentType] = 0;
                        Standards.TaggedObject.TrySetAmenitiesArea(apartmentType, 0);

                    }
                    else
                    {
                        float lrLength = length;
                        var amenityArea = Standards.TaggedObject.MinimumAmenitiesArea[apartmentType];//lrLength * 1.75;
                        //float depth = amenityArea / lrLength;
                        var keys = Standards.TaggedObject.RoomNames.Keys.ToList();
                        if (keys[indexOfBalcony] == "AILR" || keys[indexOfBalcony] == "AILRR" || keys[indexOfBalcony] == "ALRR" || keys[indexOfBalcony] == "ALR")
                        {
                            //Standards.TaggedObject.amenitiesAreas[apartmentType] = (float)amenityArea * -1;
                            Standards.TaggedObject.TrySetAmenitiesArea(apartmentType, amenityArea * -1);
                        }
                    }
                    Standards.TaggedObject.ApartmentTypeRooms[apartmentType][indexOfBalcony] = roomInclusion[indexOfBalcony];
                    yield return StartCoroutine(ResetLayout());
                    Initialize();
                    ShowOutline(true);
                    SetManufacturingSystem(manufacturingSystem);
                }
            }
            else
            {
                //refreshNotification.gameObject.SetActive(true);
                float lrLength = length;
                var amenityArea = Standards.TaggedObject.MinimumAmenitiesArea[apartmentType];//lrLength * 1.75;
                int indexOfBalcony = -1;
                if (Standards.TaggedObject.amenitiesIndices["Interior"].TryGetValue(roomId, out indexOfBalcony))
                {
                    roomInclusion[indexOfBalcony] = !roomInclusion[indexOfBalcony];
                    Standards.TaggedObject.ApartmentTypeRooms[apartmentType][indexOfBalcony] = roomInclusion[indexOfBalcony];
                    if (!roomInclusion[indexOfBalcony])
                    {
                        //Standards.TaggedObject.amenitiesAreas[apartmentType] = 0;
                        Standards.TaggedObject.TrySetAmenitiesArea(apartmentType, 0);
                        //Standards.TaggedObject.ApartmentTypesMinimumSizes[apartmentType] += amenityArea;
                    }
                    else
                    {
                        //Standards.TaggedObject.amenitiesAreas[apartmentType] = (float)amenityArea * -1;
                        Standards.TaggedObject.TrySetAmenitiesArea(apartmentType, amenityArea/* * -1*/);
                        //Standards.TaggedObject.ApartmentTypesMinimumSizes[apartmentType] -= amenityArea;
                    }
                    width = (float)Standards.TaggedObject.DesiredAreas[apartmentType] / height;
                    yield return StartCoroutine(ResetLayout());
                    //Initialize();
                    //GetComponent<ApartmentDimensions>().ToggleDimensions(true);
                    //ShowOutline(true);
                    SetManufacturingSystem(manufacturingSystem);
                }
            }
        }

        private void GenerateMaxLayout()
        {
            var delta = maxOutterWidth - outterWidth;
            if (maxLayout != null)
            {
                DestroyImmediate(maxLayout);
            }
            maxLayout = Instantiate(minMaxRenderer, transform);
            maxLayout.name = "maxLayout";
            var lr = maxLayout.GetComponent<LineRenderer>();
            lr.positionCount = 4;
            lr.SetPosition(0, new Vector3(outterWidth, 5, outterHeight));
            lr.SetPosition(1, new Vector3(outterWidth + delta, 5, outterHeight));
            lr.SetPosition(2, new Vector3(outterWidth + delta, 5, 0));
            lr.SetPosition(3, new Vector3(outterWidth, 5, 0));
        }

        private void GenerateMinimumLayout()
        {
            var delta = minOutterWidth;
            if (delta > 0)
            {
                if (minLayout != null)
                {
                    DestroyImmediate(minLayout);
                }
                minLayout = Instantiate(minMaxRenderer, transform);
                minLayout.name = "minLayout";
                var lr = minLayout.GetComponent<LineRenderer>();
                lr.positionCount = 2;
                lr.SetPosition(0, new Vector3(minOutterWidth, 5, outterHeight));
                lr.SetPosition(1, new Vector3(minOutterWidth, 5, 0));
                //minLayout.transform.localPosition = new Vector3(delta / 2, 5, outterHeight / 2);
                //minLayout.transform.localScale = new Vector3(delta / 10, 1, outterHeight / 10);
            }
            else
            {
                return;
            }
        }

        private void GeneratePanels()
        {
            Vector3 localOrigin = Vector3.zero;
            if (panelsParent != null)
            {
                DestroyImmediate(panelsParent.gameObject);
            }
            panelsParent = new GameObject("PanelsParent").transform;
            panelsParent.SetParent(transform);
            Material panelMaterial = null;
            panels = new List<GameObject>();
            float m_width = outterWidth;

            if (!hasRight) m_width += (Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] - wallWidth);
            if (!hasLeft)
            {
                m_width += (Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] - wallWidth);
                localOrigin = new Vector3(-(Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] - wallWidth), 0, 0);
            }

            if (m_width <= maxPanelWidth)
            {
                //For the facade
                var temp_scale = new Vector3(m_width, totalHeight, wallWidth);
                var temp_pos = new Vector3(localOrigin.x + temp_scale.x / 2.0f, totalHeight / 2.0f, outterHeight + temp_scale.z / 2.0f);
                panels.Add(GetSingleFacadePanel(
                            m_width,
                            temp_pos,
                            temp_scale,
                            new Vector3(0, 0, 0),
                            panelMaterial
                            ));

                //For the corridor
                temp_scale = new Vector3(m_width, totalHeight, Standards.TaggedObject.ConstructionFeatures["CorridorWall"]);
                temp_pos = new Vector3(localOrigin.x + temp_scale.x / 2.0f, totalHeight / 2.0f, temp_scale.z / 2.0f);
                panels.Add(GetSingleFacadePanel(
                            m_width,
                            temp_pos,
                            temp_scale,
                            new Vector3(0, 0, 0),
                            panelMaterial
                            ));
                localOrigin += new Vector3(temp_scale.x, 0, 0);
            }
            else
            {
                panels.AddRange(GetFacadePanelsForApartment(true));
                panels.AddRange(GetFacadePanelsForApartment(false));
            }
            panelsParent.gameObject.SetActive(false);

            if (!hasLeft)
            {
                float m_length = outterHeight;
                if (m_length <= maxPanelWidth)
                {
                    panels.Add(GetSingleFacadePanel(
                            m_length,
                            new Vector3(-Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] + wallWidth / 2.0f, totalHeight / 2, m_length / 2),
                            new Vector3(m_length, totalHeight, wallWidth),
                            new Vector3(0, 90, 0),
                            panelMaterial
                            ));
                }
                else
                {
                    int num = Mathf.CeilToInt(m_length / maxPanelWidth);
                    float length = outterHeight / num;
                    for (int i = 0; i < num; i++)
                    {
                        panels.Add(GetSingleFacadePanel(
                            length,
                            new Vector3(-Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] + wallWidth / 2.0f, totalHeight / 2, length * i + length / 2),
                            new Vector3(length, totalHeight, wallWidth),
                            new Vector3(0, 90, 0),
                            panelMaterial
                            ));
                    }
                }
            }
            else
            {
                float m_length = outterHeight;
                if (m_length <= maxPanelWidth)
                {
                    panels.Add(GetSingleFacadePanel(
                            m_length,
                            new Vector3(Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 4, totalHeight / 2, m_length / 2),
                            new Vector3(m_length, totalHeight, Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 2),
                            new Vector3(0, 90, 0),
                            panelMaterial
                            ));
                }
                else
                {
                    int num = Mathf.CeilToInt(m_length / maxPanelWidth);
                    float length = outterHeight / num;
                    for (int i = 0; i < num; i++)
                    {
                        panels.Add(GetSingleFacadePanel(
                            length,
                            new Vector3(Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 4, totalHeight / 2, length * i + length / 2),
                            new Vector3(length, totalHeight, Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 2),
                            new Vector3(0, 90, 0),
                            panelMaterial
                            ));
                    }
                }

            }

            if (!hasRight)
            {
                float m_length = outterHeight;// m_height + wallWidth;
                if (m_length <= maxPanelWidth)
                {
                    panels.Add(GetSingleFacadePanel(
                            m_length,
                            new Vector3(outterWidth + Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] - wallWidth / 2.0f, totalHeight / 2, m_length / 2),
                            new Vector3(m_length, totalHeight, wallWidth),
                            new Vector3(0, 90, 0),
                            panelMaterial
                            ));
                }
                else
                {
                    int num = Mathf.CeilToInt(m_length / maxPanelWidth);
                    float length = outterHeight / num;
                    for (int i = 0; i < num; i++)
                    {
                        panels.Add(GetSingleFacadePanel(
                            length,
                            new Vector3(outterWidth + Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] - wallWidth / 2.0f, totalHeight / 2, length * i + length / 2),
                            new Vector3(length, totalHeight, wallWidth),
                            new Vector3(0, 90, 0),
                            panelMaterial
                            ));
                    }
                }
            }
            else
            {
                float m_length = outterHeight;// m_height + wallWidth;
                if (m_length <= maxPanelWidth)
                {
                    panels.Add(GetSingleFacadePanel(
                            m_length,
                            new Vector3(outterWidth - Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 4, totalHeight / 2, m_length / 2),
                            new Vector3(m_length, totalHeight, Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 2),
                            new Vector3(0, 90, 0),
                            panelMaterial
                            ));
                }
                else
                {
                    int num = Mathf.CeilToInt(m_length / maxPanelWidth);
                    float length = outterHeight / num;
                    for (int i = 0; i < num; i++)
                    {
                        panels.Add(GetSingleFacadePanel(
                            length,
                            new Vector3(outterWidth - Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 4, totalHeight / 2, length * i + length / 2),
                            new Vector3(length, totalHeight, Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 2),
                            new Vector3(0, 90, 0),
                            panelMaterial
                            ));
                    }
                }
            }



            for (int i = 0; i < corridorLineLoads.Count; i++)
            {
                var obj = Instantiate(corridorLineLoads[i], panelsParent);
                obj.transform.localScale = new Vector3(corridorLineLoads[i].transform.localScale.z, totalHeight, wallWidth);
                obj.name = "Panel" + corridorLineLoads[i].transform.localScale.z.ToString("0.00");
                var temp_Pos = panelsParent.InverseTransformPoint(corridorLineLoads[i].transform.localPosition);
                obj.transform.position = corridorLineLoads[i].transform.position;
                obj.transform.localPosition = new Vector3(obj.transform.localPosition.x, totalHeight / 2.0f, obj.transform.localPosition.z);
                obj.transform.localEulerAngles = new Vector3(0, 90, 0);
                obj.GetComponent<MeshRenderer>().enabled = true;
                if (Standards.TaggedObject.panelsMaterials.ContainsKey(obj.name))
                {
                    panelMaterial = Standards.TaggedObject.panelsMaterials[obj.name];
                }
                else
                {
                    panelMaterial = new Material(Resources.Load("Materials/Modules/Orange") as Material);
                    panelMaterial.color = panelsGradient.Evaluate(corridorLineLoads[i].transform.localScale.z / maxPanelWidth);
                    Standards.TaggedObject.panelsMaterials.Add(obj.name, panelMaterial);
                }
                obj.GetComponent<MeshRenderer>().material = panelMaterial;
                panels.Add(obj);
            }

            for (int i = 0; i < facadeLineLoads.Count; i++)
            {
                var obj = Instantiate(facadeLineLoads[i], panelsParent);
                obj.transform.localScale = new Vector3(facadeLineLoads[i].transform.localScale.z, totalHeight, wallWidth);
                obj.name = "Panel" + facadeLineLoads[i].transform.localScale.z.ToString("0.00");
                var temp_Pos = panelsParent.InverseTransformPoint(facadeLineLoads[i].transform.localPosition);
                obj.transform.position = facadeLineLoads[i].transform.position;
                obj.transform.localPosition = new Vector3(obj.transform.localPosition.x, totalHeight / 2.0f, obj.transform.localPosition.z);
                obj.transform.localEulerAngles = new Vector3(0, 90, 0);
                obj.GetComponent<MeshRenderer>().enabled = true;
                if (Standards.TaggedObject.panelsMaterials.ContainsKey(obj.name))
                {
                    panelMaterial = Standards.TaggedObject.panelsMaterials[obj.name];
                }
                else
                {
                    panelMaterial = new Material(Resources.Load("Materials/Modules/Orange") as Material);
                    panelMaterial.color = panelsGradient.Evaluate(facadeLineLoads[i].transform.localScale.z / maxPanelWidth);
                    Standards.TaggedObject.panelsMaterials.Add(obj.name, panelMaterial);
                }
                obj.GetComponent<MeshRenderer>().material = panelMaterial;
                panels.Add(obj);
            }
        }

        private GameObject GetSingleFacadePanel(float length, Vector3 position, Vector3 scale, Vector3 euler, Material panelMaterial)
        {
            GameObject singleFacadePanel = GameObject.CreatePrimitive(PrimitiveType.Cube);
            singleFacadePanel.name = "Panel" + length.ToString("0.00");
            singleFacadePanel.transform.localScale = scale;
            singleFacadePanel.transform.SetParent(panelsParent);
            singleFacadePanel.transform.localEulerAngles = euler;
            singleFacadePanel.transform.localPosition = position;
            if (Standards.TaggedObject.panelsMaterials.ContainsKey(singleFacadePanel.name))
            {
                panelMaterial = Standards.TaggedObject.panelsMaterials[singleFacadePanel.name];
            }
            else
            {
                panelMaterial = new Material(Resources.Load("Materials/Modules/Orange") as Material);
                panelMaterial.color = panelsGradient.Evaluate(length / maxPanelWidth);
                Standards.TaggedObject.panelsMaterials.Add(singleFacadePanel.name, panelMaterial);
            }
            singleFacadePanel.GetComponent<MeshRenderer>().material = panelMaterial;
            return singleFacadePanel;
        }

        private void GenerateVolumetricModules()
        {
            if (verticalModulesParent != null)
            {
                DestroyImmediate(verticalModulesParent.gameObject);
            }
            verticalModulesParent = new GameObject("VerticalModulesParent").transform;
            verticalModulesParent.SetParent(transform);
            verticalModules = new List<GameObject>();
            verticalModules.AddRange(GetModulesForApartment());
        }

        private void IssuedFlagged(bool show)
        {
            if (roomIssue != null)
            {
                roomIssue.text = "The room dimensions you have set are not fit for this size of apartment!";
                roomIssue.gameObject.SetActive(show);
            }
        }

        private void GenerateRooms()
        {
            if (roomParent != null)
            {
                DestroyImmediate(roomParent.gameObject);
            }
            roomParent = new GameObject("Rooms").transform;
            roomParent.SetParent(transform);

            rooms = new List<ProceduralRoom>();
            for (int i = 0; i < lengths.Count; i++)
            {
                GameObject obj = Instantiate(proceduralRoomPrefab, roomParent);
                obj.name = edges.Keys.ToList()[i];
                if (origins[i] == "orig")
                {
                    obj.transform.localPosition = origin;
                }
                else
                {
                    var inds = origins[i].Split(';');
                    var vert = rooms[int.Parse(inds[0])].GetVertex(int.Parse(inds[1]));
                    obj.transform.position = vert;
                }
                var procedural = obj.GetComponent<ProceduralRoom>();
                procedural.aptLayout = this;
                if (i < roomColors.Count)
                {
                    procedural.color = roomColors[i];
                }
                else
                {
                    procedural.color = roomColors.Last();
                }
                procedural.Initialize(lengths[i], types[i], totalHeight, i, Standards.TaggedObject.RoomDimensionBools[obj.name]);
                rooms.Add(procedural);
            }
        }

        private void UpdateRooms()
        {
            for (int i = 0; i < rooms.Count; i++)
            {
                var obj = rooms[i].gameObject;
                if (origins[i] == "orig")
                {
                    obj.transform.localPosition = origin;
                }
                else
                {
                    var inds = origins[i].Split(';');
                    var vert = rooms[int.Parse(inds[0])].GetVertex(int.Parse(inds[1]));
                    obj.transform.position = vert;
                }
                rooms[i].UpdateRoom(lengths[i]);
            }
        }

        private void GetEdgesFromContent()
        {
            var sideOffset = (partyWall);
            var topOffset = (exteriorWall) + (corridorWall);
            outterWidth = width + sideOffset;
            outterHeight = height + topOffset;
            maxOutterWidth = (float)Standards.TaggedObject.ApartmentTypesMaximumSizes[apartmentType] / height + sideOffset;
            minOutterWidth = (float)Standards.TaggedObject.ApartmentTypesMinimumSizes[apartmentType] / height + sideOffset;
            m_width = width + wallWidth;
            m_height = height + wallWidth;


            totalArea = width * height;
            NIA = width * height;

            origin = new Vector3(sideOffset * 0.5f - wallWidth * 0.5f, 0, corridorWall - wallWidth * 0.5f);

            edges = new Dictionary<string, List<string>>();
            origins = new List<string>();
            var lines = content.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < lines.Length; i++)
            {
                var values = lines[i].Split(',').ToList();
                string name = values[0];
                origins.Add(values[1]);
                values.RemoveAt(0);
                values.RemoveAt(0);
                for (int j = 0; j < values.Count; j++)
                {
                    float v = 0.0f;
                    if (float.TryParse(values[j], out v))
                    {
                        values[j] = (v).ToString();
                    }
                }
                for (int j = 0; j < roomInclusion.Length; j++)
                {
                    if (name == roomNames[j])
                    {
                        if (!roomInclusion[j])
                        {
                            for (int k = 0; k < values.Count; k++)
                            {
                                values[k] = 0.ToString();
                            }
                        }
                    }
                }
                edges.Add(name, values);
            }
        }

        private void GetLengthsFromEdges()
        {
            lengths = new List<List<float>>();
            foreach (var item in edges)
            {
                List<float> m_lengths = new List<float>();
                for (int i = 0; i < item.Value.Count; i++)
                {
                    var length = ParseEdgeValue(item.Value[i], item.Key, edges, m_height, m_width);
                    m_lengths.Add(length);
                }
                lengths.Add(m_lengths);
            }
            originalLengths = new List<List<float>>();
            for (int i = 0; i < lengths.Count; i++)
            {
                originalLengths.Add(new List<float>());
                for (int j = 0; j < lengths[i].Count; j++)
                {
                    originalLengths[i].Add(lengths[i][j]);
                }
            }
        }

        private float ParseEdgeValue(string val, string name, Dictionary<string, List<string>> edges, float height, float width)
        {
            try
            {
                //Print("in: " + val);
                float m_val = 0;

                if (float.TryParse(val, out m_val))
                {
                    return m_val;
                }
                else
                {
                    if (val.Contains("-"))
                    {
                        var parts = val.Split('-');
                        float v = ParseEdgeValue(parts[0], name, edges, height, width);
                        for (int i = 1; i < parts.Length; i++)
                        {
                            v -= ParseEdgeValue(parts[i], name, edges, height, width);
                        }
                        m_val = v;
                    }
                    else if (val.Contains("+"))
                    {
                        var parts = val.Split('+');
                        float v = 0;
                        for (int i = 0; i < parts.Length; i++)
                        {
                            var m_v = ParseEdgeValue(parts[i], name, edges, height, width);
                            v += m_v;
                        }
                        m_val = v;
                    }
                    else if (val.Contains("\""))
                    {
                        int m_index = -1;
                        val = val.Replace("\"", String.Empty);
                        if (val == "height")
                        {
                            m_val = height;
                        }
                        else if (val == "width")
                        {
                            m_val = width;
                        }
                        else if (val == "dbVal")
                        {
                            m_val = dbVal;
                        }
                        else if (val == "pbVal")
                        {
                            m_val = pbVal;
                        }
                        else if (val == "baVal")
                        {
                            m_val = baVal;
                        }
                        else if (val == "baaVal")
                        {
                            m_val = baaVal;
                        }
                        else if (val == "wcVal")
                        {
                            m_val = wcVal;
                        }
                        else if (val == "wccVal")
                        {
                            m_val = wccVal;
                        }
                        else if (val == "dbbVal")
                        {
                            m_val = dbbVal;
                        }
                        else if (val == "pbbVal")
                        {
                            m_val = pbbVal;
                        }
                        else if (val == "psbVal")
                        {
                            m_val = psbVal;
                        }
                        else if (val == "balcVal")
                        {
                            if ( name == "AILRR" || name == "LRR")
                            {
                                var depth = ParseEdgeValue(edges["LRR"][2], name, edges, height, width);
                                m_val = Math.Abs(minAmenity / depth);
                            }
                            else if (name == "AILR" || name == "LR")
                            {
                                var depth = ParseEdgeValue(edges["LR"][2], name, edges, height, width);
                                m_val = Math.Abs(minAmenity / depth);
                            }
                        }
                        else if (val == "balcVal2")
                        {
                            if (name == "ALRR" )
                            {
                                m_val = Math.Abs(minAmenity / 1.5f);
                            }
                            else if (name == "ALR")
                            {
                                m_val = Math.Abs(minAmenity / 1.5f);
                            }
                        }
                        else if (int.TryParse(val, out m_index))
                        {
                            if (val.Contains("-"))
                            {
                                m_val = -1 * ParseEdgeValue(edges[name][Math.Abs(m_index)], name, edges, height, width);
                            }
                            else
                            {
                                m_val = ParseEdgeValue(edges[name][m_index], name, edges, height, width);
                            }
                        }
                        else if (val.Contains("_"))
                        {
                            var parts = val.Split('_');
                            m_val = ParseEdgeValue(edges[parts[0]][int.Parse(parts[1])], name, edges, height, width);
                        }
                    }
                    else
                    {
                        if (val == "height")
                        {
                            m_val = height;
                        }
                        else if (val == "width")
                        {
                            m_val = width;
                        }
                        else if (val == "dbVal")
                        {
                            m_val = dbVal;
                        }
                        else if (val == "pbVal")
                        {
                            m_val = pbVal;
                        }
                        else if (val == "baVal")
                        {
                            m_val = baVal;
                        }
                        else if (val == "baaVal")
                        {
                            m_val = baaVal;
                        }
                        else if (val == "wcVal")
                        {
                            m_val = wcVal;
                        }
                        else if (val == "wccVal")
                        {
                            m_val = wccVal;
                        }
                        else if (val == "dbbVal")
                        {
                            m_val = dbbVal;
                        }
                        else if (val == "pbbVal")
                        {
                            m_val = pbbVal;
                        }
                        else if (val == "psbVal")
                        {
                            m_val = psbVal;
                        }
                        else if (val == "balcVal")
                        {
                            if (name == "AILRR" || name == "LRR")
                            {
                                m_val = Math.Abs(minAmenity / ParseEdgeValue(edges["LRR"][2], name, edges, height, width));
                            }
                            else if (name == "AILR" || name == "LR")
                            {
                                m_val = Math.Abs(minAmenity / ParseEdgeValue(edges["LR"][2], name, edges, height, width));
                            }
                        }
                        else if (val == "balcVal2")
                        {
                            if (name == "ALRR")
                            {
                                m_val = Math.Abs(minAmenity / 1.5f);
                            }
                            else if (name == "ALR")
                            {
                                m_val = Math.Abs(minAmenity / 1.5f);
                            }
                        }
                        else if (val.Contains("_"))
                        {
                            var parts = val.Split('_');
                            m_val = ParseEdgeValue(edges[parts[0]][int.Parse(parts[1])], name, edges, height, width);
                        }
                    }
                    return m_val;
                }
            }
            catch (Exception e)
            {
                Debug.Log(val + ": " + e);
                return 0;
            }
        }
        #endregion

        #region Panels and Modules Methods

        private List<GameObject> GetFacadePanelsForApartment(bool facade)
        {
            Material panelMaterial;
            List<GameObject> panels = new List<GameObject>();
            Vector3 localOrigin = !hasLeft ? new Vector3(-(Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] - wallWidth), 0, 0) : Vector3.zero;

            int[] roomIndices = Standards.TaggedObject.ModulesPerType[apartmentType];

            for (int i = 0; i < roomIndices.Length; i++)
            {
                GameObject panel = GameObject.CreatePrimitive(PrimitiveType.Cube);
                float length = 0;
                if (roomIndices[i] == 1)
                    length = lengths[roomIndices[i]][4];
                else
                    length = lengths[roomIndices[i]][0];
                if (i == 0)
                {
                    panel.transform.localScale = !hasLeft ? new Vector3(Mathf.Abs(length) + wallWidth / 2.0f + (Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] - Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 2), totalHeight, wallWidth) : new Vector3(Mathf.Abs(length) + wallWidth / 2.0f, totalHeight, facade ? wallWidth : Standards.TaggedObject.ConstructionFeatures["CorridorWall"]);
                }
                else
                {
                    panel.transform.localScale = new Vector3(Mathf.Abs(length), totalHeight, facade ? wallWidth : Standards.TaggedObject.ConstructionFeatures["CorridorWall"]);
                }
                panel.transform.SetParent(panelsParent);
                var temp_pos1 = new Vector3(localOrigin.x + panel.transform.localScale.x / 2.0f, totalHeight / 2.0f, facade ? outterHeight + panel.transform.localScale.z / 2.0f : Standards.TaggedObject.ConstructionFeatures["CorridorWall"] / 2.0f);
                panel.transform.localPosition = temp_pos1;
                panels.Add(panel);
                localOrigin += new Vector3(panel.transform.localScale.x, 0, 0);
                panel.transform.localScale *= modulesScale;
                panel.name = "Panel" + length.ToString("0.00");
                if (Standards.TaggedObject.panelsMaterials.ContainsKey(panel.name))
                {
                    panelMaterial = Standards.TaggedObject.panelsMaterials[panel.name];
                }
                else
                {
                    panelMaterial = new Material(Resources.Load("Materials/Modules/Orange") as Material);
                    panelMaterial.color = panelsGradient.Evaluate(length / Standards.TaggedObject.ConstructionFeatures["PanelLength"]);
                    Standards.TaggedObject.panelsMaterials.Add(panel.name, panelMaterial);
                }
                panel.GetComponent<MeshRenderer>().material = panelMaterial;
            }

            float leftOver = !hasRight ? m_width + partyWall / 2.0f - localOrigin.x + (Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] - Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 2) : m_width + partyWall / 2.0f - localOrigin.x;
            GameObject livingRoomPanel = GameObject.CreatePrimitive(PrimitiveType.Cube);
            livingRoomPanel.transform.localScale = new Vector3(leftOver, totalHeight, facade ? wallWidth : Standards.TaggedObject.ConstructionFeatures["CorridorWall"]);
            livingRoomPanel.transform.SetParent(panelsParent);
            var temp_pos = new Vector3(localOrigin.x + livingRoomPanel.transform.localScale.x / 2.0f, totalHeight / 2.0f, facade ? outterHeight + livingRoomPanel.transform.localScale.z / 2.0f : Standards.TaggedObject.ConstructionFeatures["CorridorWall"] / 2.0f);
            livingRoomPanel.transform.localPosition = temp_pos;
            livingRoomPanel.transform.localScale *= modulesScale;
            livingRoomPanel.name = "Panel" + leftOver.ToString("0.00");
            panels.Add(livingRoomPanel);
            if (Standards.TaggedObject.panelsMaterials.ContainsKey(livingRoomPanel.name))
            {
                panelMaterial = Standards.TaggedObject.panelsMaterials[livingRoomPanel.name];
            }
            else
            {
                panelMaterial = new Material(Resources.Load("Materials/Modules/Orange") as Material);
                panelMaterial.color = panelsGradient.Evaluate(leftOver / Standards.TaggedObject.ConstructionFeatures["PanelLength"]);
                Standards.TaggedObject.panelsMaterials.Add(livingRoomPanel.name, panelMaterial);
            }
            livingRoomPanel.GetComponent<MeshRenderer>().material = panelMaterial;

            return panels;
        }

        private List<GameObject> GetModulesForApartment()
        {
            if (!Standards.TaggedObject.modulesMaterials.ContainsKey("Not Modular"))
            {
                var mat = new Material(Resources.Load("Materials/Modules/Orange") as Material);
                mat.color = Color.gray;
                Standards.TaggedObject.modulesMaterials.Add("Not Modular", mat);
            }
            Vector3 localOrigin;
            float m_height;
            Material moduleMaterial;
            if (hasCorridor)
            {
                localOrigin = new Vector3(0, 0, -corridorWidth);
                m_height = height + corridorWidth;
            }
            else
            {
                localOrigin = Vector3.zero;
                m_height = height;
            }

            List<GameObject> modules = new List<GameObject>();

            int[] roomIndices = Standards.TaggedObject.ModulesPerType[apartmentType];

            for (int i = 0; i < roomIndices.Length; i++)
            {
                GameObject module = GameObject.CreatePrimitive(PrimitiveType.Cube);
                float length = 0;
                if (roomIndices[i] == 1 || roomIndices[i] == 2)
                    length = lengths[roomIndices[i]][4];
                else
                    length = lengths[roomIndices[i]][0];
                if (i == 0)
                {
                    module.transform.localScale = new Vector3(Mathf.Abs(length) + Standards.TaggedObject.ConstructionFeatures["PartyWall"] / 2.0f - wallWidth / 2.0f, totalHeight, m_height + corridorWall);
                }
                else
                {
                    module.transform.localScale = new Vector3(Mathf.Abs(length), totalHeight, m_height + corridorWall);
                }
                //module.transform.localScale = new Vector3(Mathf.Abs(length)/* + wallWidth / 2.0f*/, totalHeight, m_height + corridorWall);
                module.transform.SetParent(verticalModulesParent);
                module.transform.localPosition = localOrigin + module.transform.localScale * 0.5f;
                modules.Add(module);
                localOrigin += new Vector3(module.transform.localScale.x, 0, 0);
                module.transform.localScale *= modulesScale;
                module.name = GetModulesInclusions(module.transform);
                if (Standards.TaggedObject.modulesMaterials.ContainsKey(module.name))
                {
                    moduleMaterial = Standards.TaggedObject.modulesMaterials[module.name];
                }
                else
                {
                    moduleMaterial = new Material(Resources.Load("Materials/Modules/Orange") as Material);
                    moduleMaterial.color = panelsGradient.Evaluate(BrydenWoodUtils.Remap(length, 2, 4, 0, 1));//length / Standards.TaggedObject.ConstructionFeatures["PanelLength"]);
                    moduleMaterial.color = new Color(moduleMaterial.color.r, moduleMaterial.color.g, moduleMaterial.color.b, 0.6f);
                    Standards.TaggedObject.modulesMaterials.Add(module.name, moduleMaterial);
                }
                module.GetComponent<MeshRenderer>().material = moduleMaterial;
            }

            float leftOver = m_width + partyWall / 2.0f - localOrigin.x;
            GameObject livingRoomModule = GameObject.CreatePrimitive(PrimitiveType.Cube);
            livingRoomModule.transform.localScale = new Vector3(leftOver, totalHeight, m_height + corridorWall);
            livingRoomModule.transform.SetParent(verticalModulesParent);
            livingRoomModule.transform.localPosition = localOrigin + livingRoomModule.transform.localScale * 0.5f;
            livingRoomModule.transform.localScale *= modulesScale;
            livingRoomModule.name = GetModulesInclusions(livingRoomModule.transform);
            modules.Add(livingRoomModule);
            if (Standards.TaggedObject.modulesMaterials.ContainsKey(livingRoomModule.name))
            {
                moduleMaterial = Standards.TaggedObject.modulesMaterials[livingRoomModule.name];
            }
            else
            {
                moduleMaterial = new Material(Resources.Load("Materials/Modules/Orange") as Material);
                moduleMaterial.color = panelsGradient.Evaluate(BrydenWoodUtils.Remap(leftOver, 2, 4, 0, 1));//leftOver / Standards.TaggedObject.ConstructionFeatures["PanelLength"]);
                moduleMaterial.color = new Color(moduleMaterial.color.r, moduleMaterial.color.g, moduleMaterial.color.b, 0.6f);
                Standards.TaggedObject.modulesMaterials.Add(livingRoomModule.name, moduleMaterial);
            }
            livingRoomModule.GetComponent<MeshRenderer>().material = moduleMaterial;

            return modules;
        }

        private string GetModulesInclusions(Transform module)
        {
            string m_name = "Module:";
            List<string> completeInclusion = new List<string>();
            List<string> partialInclusion = new List<string>();

            Vector3[] boxOutline = new Vector3[]
            {
                module.rotation*(new Vector3(-module.localScale.x/2,-module.localScale.y/2,-module.localScale.z/2-0.05f)) + module.transform.position,
                module.rotation*(new Vector3(module.localScale.x/2,-module.localScale.y/2,-module.localScale.z/2-0.05f)) + module.transform.position,
                module.rotation*(new Vector3(module.localScale.x/2,-module.localScale.y/2,module.localScale.z/2+0.05f)) + module.transform.position,
                module.rotation*(new Vector3(-module.localScale.x/2,-module.localScale.y/2,module.localScale.z/2+0.05f)) + module.transform.position,
            };

            for (int i = 0; i < roomInclusion.Length; i++)
            {
                if (roomInclusion[i])
                {
                    int inclusionCounter = 0;
                    List<string> vertexInclusion = new List<string>();
                    Mesh m = rooms[i].polygon.GetComponent<MeshFilter>().mesh;
                    for (int j = 0; j < m.vertices.Length; j++)
                    {
                        Vector3 point = rooms[i].polygon.transform.localToWorldMatrix.MultiplyPoint3x4(m.vertices[j]) + new Vector3(0, 0.5f, 0);
                        if (Polygon.IsIncluded(boxOutline, point))
                        {
                            vertexInclusion.Add(j.ToString());
                            inclusionCounter++;
                        }
                    }

                    if (inclusionCounter != 0)
                    {
                        if (inclusionCounter == rooms[i].polygon.Count)
                            completeInclusion.Add(roomNames[i]/* + "-" + String.Join("-", vertexInclusion.ToArray())*/);
                        else
                            partialInclusion.Add(roomNames[i] + "-" + String.Join("-", vertexInclusion.ToArray()));
                    }
                }
            }

            m_name += String.Join("_", completeInclusion.ToArray()) + "|" + String.Join("_", partialInclusion.ToArray());
            return m_name;
        }
        #endregion
    }
}
