﻿using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.UIElements;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A MonoBehaviour component for the Procedural Floor Layout
    /// </summary>
    public class ProceduralFloor : BaseDesignData
    {
        #region Public Fields and Properties
        [Header("Prefabs:")]
        public GameObject polygonPrefab;
        public GameObject polygonVertexPrefab;
        public GameObject controlPointUIPrefab;
        public GameObject apartmentPrefab;
        public GameObject extrusionPrefab;
        public GameObject proceduralApartmentLayout;

        [Header("Scene Objects:")]
        public Transform polygonVertexParent;
        public Transform controlPointUiParent;
        //public ProceduralBuildingManager buildingManager;
        public DrawBuildingLines drawBuildingLines;
        public int linesIndex;
        public Polygon exteriorPolygon;

        [Header("Materials:")]
        public Material typeMaterial;
        public Material originalMaterial;

        //[Header("Distribution Parameters")]
        public LinearTypology typology { get { return building.typology; } }

        [Header("Core Properties:")]
        public float coresRadius = 50.0f;
        public float coreCornerOffset = 7.5f;
        public float minimumCorridorLengthForCore = 15.0f;
        public float coreWidth = 4.8f;
        public float coreLength = 7.0f;
        public CoreAllignment coreAllignment { get { return building.coreAllignment; } }


        public float corridorWidth = 1.2f;//{ get { return building.corridorWidth; } }
        [Header("Corridor Properties:")]
        public float floorActiveCorridorLength;

        [Header("Floor Properties:")]
        public float apartmentDepth = 1.0f;
        public string key { get; set; }
        public float updateDelay = 2;
        public Polygon centreLine { get; set; }
        public Polygon masterPolyline { get; set; }
        public List<List<Polygon>> offsets { get; set; }
        public List<List<Polygon>> corridorsPolygons { get; set; }
        public Dictionary<string, float> percentages { get; set; }
        public List<Polygon> commercialPolygons { get; set; }
        public Transform commercialParent { get; set; }
        public float floor2floor;
        public int[] levels;
        public GameObject coresParent { get; set; }
        public GameObject corridors { get; set; }
        public int Index { get { return building.floors.IndexOf(this); } }
        public bool hasGeometry;
        public virtual float GEA
        {
            get
            {
                return GetTotalFloorArea();
            }
        }
        protected float gia;
        public virtual float GIA
        {
            get
            {
                return GetGIA(); //GetTotalFloorArea() * giaRation;
            }
        }
        protected float nia;
        public virtual float NIA
        {
            get
            {
                return GetNIA();
            }
        }
        public float yVal { get; set; }
        public float apartmentsArea
        {
            get
            {
                float ar = 0;
                if (apartments != null)
                {
                    for (int i = 0; i < apartments.Count; i++)
                    {
                        ar += apartments[i].Area;
                    }
                }
                return ar;
            }
        }
        public float numberOfPeople
        {
            get
            {
                int peopl = 0;
                if (apartments != null)
                {
                    for (int i = 0; i < apartments.Count; i++)
                    {
                        peopl += apartments[i].numberOfPeople;
                    }
                }
                return peopl;
            }
        }
        public int id { get { return building.floors.IndexOf(this); } }
        public List<ApartmentUnity> apartments { get; set; }
        public List<float> coresParams { get; set; }
        public List<Vector3> coresPositions
        {
            get
            {
                List<Vector3> positions = new List<Vector3>();
                for (int i = 0; i < coresParams.Count; i++)
                {
                    positions.Add(centreLine.PointOnCurve(coresParams[i]));
                }
                return positions;
            }
        }
        public float facadeArea
        {
            get
            {
                return GetFacadeArea();
            }
        }
        public float facadePerimeter
        {
            get
            {
                return GetFacadePerimeter();
            }
        }
        public ProceduralFloor masterFloor { get; set; }
        public bool projectedCores { get; set; }
        public List<int> apartmentNumbers
        {
            get
            {
                return GetApartmentNumbers();
            }
        }

        public Dictionary<string, float[]> ApartmentStats
        {
            get
            {
                return GetApartmentStats();
            }
        }

        public static string infoLabels
        {
            get
            {
                List<string> vals = new List<string>() { "GEA", "GIA", "NIA", "GrossToNet", "ApartmentsArea", "FacadeArea", "FacadePerimeter", "WallToFloor", "NumberOfPeople", "Floor2Floor", "Levels", "CorridorWidth", "ApartmentDepth" };
                foreach (var item in Standards.TaggedObject.ApartmentTypesMinimumSizes)
                {
                    vals.Add(item.Key);
                }
                return string.Join(",", vals.ToArray());
            }
        }

        public float MaxAptDepth
        {
            get
            {
                if (percentages.ContainsKey("Commercial") || percentages.ContainsKey("Other"))
                {
                    return 0;
                }
                else
                {
                    float maxDepth = float.MinValue;
                    for (int i = 0; i < apartments.Count; i++)
                    {
                        var depth = apartments[i].Depth;
                        if (depth > maxDepth) maxDepth = depth;
                    }
                    return maxDepth;
                }
            }
        }

        public float unitsPerHectar
        {
            get
            {
                var site = building.buildingManager.sitePolygon;
                if (site != null)
                {
                    float dens = 0;
                    float siteArea = site.Area / 10000.0f; //Converting to hectares
                    dens = apartments.Count / siteArea;
                    return dens;
                }
                else
                {
                    return 0;
                }
            }
        }

        public float habitRoomPerHectar
        {
            get
            {
                var site = building.buildingManager.sitePolygon;
                if (site != null)
                {
                    float dens = 0;
                    float habNum = 0;
                    for (int i = 0; i < apartments.Count; i++)
                    {
                        int num = 0;
                        if (Standards.TaggedObject.HabitableRoomsPerApartment.TryGetValue(apartments[i].ApartmentType, out num))
                        {
                            habNum += num;
                        }
                    }
                    float siteArea = site.Area / 10000.0f; //Converting to hectares
                    dens = habNum / siteArea;
                    return dens;
                }
                else
                {
                    return 0;
                }
            }
        }

        public float habitRoomsCount
        {
            get
            {
                float habNum = 0;
                for (int i = 0; i < apartments.Count; i++)
                {
                    int num = 0;
                    if (Standards.TaggedObject.HabitableRoomsPerApartment.TryGetValue(apartments[i].ApartmentType, out num))
                    {
                        habNum += num;
                    }
                }
                return habNum;
            }
        }

        public bool hasCustom { get; set; }
        public GameObject aptParent { get; protected set; }
        public ProceduralBuilding building { get; set; }
        public List<ApartmentUnity> unresolvables { get; private set; }

        #endregion

        #region Private Properties
        private List<List<Vector3[]>> cores { get; set; }
        private List<List<Line>>[] activeCorridorLengths { get; set; }
        protected List<GeometryVertex> floorVertices { get; set; }
        private List<Vector3[]> cornerAptPoints { get; set; }
        private List<bool> isExteriorCorner { get; set; }
        private List<float> cornersParams { get; set; }
        private List<Vector3[]> coresContinuous { get; set; }
        private List<Polygon> corridorsOffsets { get; set; }
        protected Polygon plantRoom { get; set; }
        protected Color plantRoomColor { get; set; }
        protected WaitForEndOfFrame waitFrame { get; set; }
        private bool startEditing { get; set; }
        private DateTime lastEditTime { get; set; }
        private Polygon sender { get; set; }
        protected FloorLayoutState saveState { get; set; }
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            waitFrame = new WaitForEndOfFrame();
        }

        // Update is called once per frame
        void Update()
        {

            if (startEditing)
            {
                var dif = DateTime.Now - lastEditTime;
                if (dif.TotalSeconds > updateDelay)
                {
                    StartCoroutine(DelayedUpdate(sender));
                    startEditing = false;
                }
            }
        }

        private void OnDestroy()
        {
            if (drawBuildingLines != null)
            {
                drawBuildingLines.RemoveBuildingLines(key);
                drawBuildingLines.RemovePlantRoomLines(key);
            }
            if (centreLine != null)
            {
                Destroy(centreLine.gameObject);
            }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Initializes the Instance
        /// </summary>
        /// <param name="polygon">The centre line of the floor</param>
        /// <param name="levels">The levels of the floor layout as a list</param>
        /// <param name="hasCustom">Whether is has a custom centre line or it uses the master centre line</param>
        public void Initialize(Polygon polygon, int[] levels, bool hasCustom = false)
        {
            masterPolyline = polygon;
            this.hasCustom = hasCustom;
            centreLine = masterPolyline;
            transform.position = new Vector3(transform.position.x, centreLine[0].currentPosition.y, transform.position.z);
            //centreLine.updated += UpdateBuilding;
            centreLine.updated.AddListener(UpdateBuilding);
            ProceduralBuildingManager.apartmentsChanged += OnApartmentsChanged;
            this.levels = levels;
            drawBuildingLines = Camera.main.GetComponent<DrawBuildingLines>();
            GeometryVertex.destroyed += OnVertexDestroyed;
        }

        /// <summary>
        /// Initializes the Instance
        /// </summary>
        /// <param name="polygon">The centre line of the floor</param>
        /// <param name="hasCustom">Whether is has a custom centre line or it uses the master centre line</param>
        public void Initialize(Polygon polygon, bool hasCustom = false)
        {
            masterPolyline = polygon;
            this.hasCustom = hasCustom;
            centreLine = masterPolyline;
            if (centreLine == null)
            {
#if UNITY_EDITOR
                throw new Exception("You have to set the centre-line before generating the geometry!");
#elif UNITY_WEBGL
                ResourcesLoader.TaggedObject.NotificationHelp("You have to set the centre-line before generating the geometry!");
                return;
#endif
            }
            transform.position = new Vector3(transform.position.x, centreLine[0].currentPosition.y, transform.position.z);
            //centreLine.updated += UpdateBuilding;
            centreLine.updated.AddListener(UpdateBuilding);
            ProceduralBuildingManager.apartmentsChanged += OnApartmentsChanged;
            drawBuildingLines = Camera.main.GetComponent<DrawBuildingLines>();
            GeometryVertex.destroyed += OnVertexDestroyed;
        }

        /// <summary>
        /// Initializes the Instance
        /// </summary>
        public void Initialize()
        {
            levels = new int[0];
            percentages = new Dictionary<string, float>();
        }

        /// <summary>
        /// Called when this floor is deleted
        /// </summary>
        public void Delete()
        {
            building.OnFloorDeleted(this);
        }

        /// <summary>
        /// Used to resolve a Special Apartment to its neighbours
        /// </summary>
        /// <param name="aptsToCombine">The apartments to be combined</param>
        public void OnResolveApartmentWithNext(List<BaseDesignData> aptsToCombine)
        {
            building.OnResolveApartmentWithNext(aptsToCombine);
        }

        /// <summary>
        /// Logs the Undo-able resolve action
        /// </summary>
        /// <param name="action">The action to be logged</param>
        public void SetResolveAction(ResolveAction action)
        {
            building.SetResolveAction(action);
        }

        /// <summary>
        /// Sets the apartments'material to transparent
        /// </summary>
        /// <param name="set">Whether it should be transparent or not</param>
        public void SetTransparentApartmentMaterial(bool set)
        {
            for (int i = 0; i < apartments.Count; i++)
            {
                if (set)
                    apartments[i].editableMesh.meshExtrusion.GetComponent<MeshRenderer>().material = Resources.Load("Materials/TransparentApartments") as Material;
                else
                    apartments[i].editableMesh.meshExtrusion.GetComponent<MeshRenderer>().material = Resources.Load("Materials/ground_basic 1") as Material;
            }
        }


        /// <summary>
        /// Returns the maximum module width for this floor
        /// </summary>
        /// <returns>Float</returns>
        public float GetMaximumModuleWidth()
        {
            float maxWidth = float.MinValue;
            if (apartments != null)
            {
                for (int i = 0; i < apartments.Count; i++)
                {
                    float width = apartments[i].GetMaximumModuleWidth();
                    if (width > maxWidth)
                    {
                        maxWidth = width;
                    }
                }
            }

            return maxWidth;
        }

        /// <summary>
        /// Returns the maximum line-load span in the floor
        /// </summary>
        /// <returns></returns>
        public float GetMaximumLineLoadSpan()
        {
            float maxSpan = float.MinValue;
            if (apartments != null)
            {
                for (int i = 0; i < apartments.Count; i++)
                {
                    float width = apartments[i].GetMaximumLineLoadSpan();
                    if (width > maxSpan)
                    {
                        maxSpan = width;
                    }
                }
            }

            return maxSpan;
        }

        /// <summary>
        /// Returns the required values for the systems analysis
        /// </summary>
        /// <param name="width">The maximum Module Width / Panel Length in the floor</param>
        /// <param name="span">The maximum line-load span in the floor</param>
        public void GetSystemNumbers(ref float width, ref float span)
        {
            width = float.MinValue;
            span = float.MinValue;
            if (apartments != null)
            {
                for (int i = 0; i < apartments.Count; i++)
                {
                    if (apartments[i].ApartmentType != "SpecialApt")
                    {
                        float _width = apartments[i].GetMaximumModuleWidth();
                        if (_width > width) width = _width;
                        float _span = apartments[i].GetMaximumLineLoadSpan();
                        if (_span > span) span = _span;
                    }
                }
            }
        }

        /// <summary>
        /// Returns the mximum panel width for this floor
        /// </summary>
        /// <returns>Float</returns>
        public float GetMaximumPanelWidth()
        {
            float maxWidth = float.MinValue;
            if (apartments != null)
            {
                for (int i = 0; i < apartments.Count; i++)
                {
                    float width = apartments[i].GetMaximumPanelLength();
                    if (width > maxWidth)
                    {
                        maxWidth = width;
                    }
                }
            }

            return maxWidth;
        }

        /// <summary>
        /// Called when this floor is selected
        /// </summary>
        /// <param name="triggerEvent">Whether the selection should trigger an event</param>
        public override void Select(bool triggerEvent = true)
        {
            StartCoroutine(building.OnFloorSelected(this));
        }

        /// <summary>
        /// Returns the actual apartment distribution based on a target one
        /// </summary>
        /// <param name="percentages">The target distribution</param>
        /// <param name="totalNum">The total number of actual apartments</param>
        /// <returns>String Float Dictionary</returns>
        public Dictionary<string, float> CheckDistribution(Dictionary<string, float> percentages, out int totalNum)
        {
            Dictionary<string, float> newPercs = new Dictionary<string, float>();
            List<Polygon>[] temp_Offsets = new List<Polygon>[2];
            List<Line>[] tempactive = new List<Line>[2];
            tempactive = ReorderActiveCorridorsEnds2CentreOpen(out temp_Offsets);

            var temp_percentages = percentages.OrderBy(x => Standards.TaggedObject.DesiredAreas[x.Key]).ToList();
            temp_percentages.Reverse();
            Dictionary<string, float> widths = new Dictionary<string, float>();
            Dictionary<string, float> availableLengths = new Dictionary<string, float>();
            Dictionary<string, int> aptNumbers = new Dictionary<string, int>();
            foreach (var item in temp_percentages)
            {
                double aptEnvelopeWidth = (Standards.TaggedObject.DesiredAreas[item.Key] / apartmentDepth) + Standards.TaggedObject.ConstructionFeatures["PartyWall"];
                widths.Add(item.Key, (float)(aptEnvelopeWidth));
                availableLengths.Add(item.Key, floorActiveCorridorLength * (item.Value));
                aptNumbers.Add(item.Key, Mathf.FloorToInt(availableLengths[item.Key] / widths[item.Key]));
            }

            var nums = EvaluateAptsNumbers(widths, aptNumbers, tempactive, temp_Offsets);
            totalNum = 0;
            foreach (var item in nums)
            {
                totalNum += item.Value;
            }
            foreach (var item in nums)
            {
                if (percentages.ContainsKey(item.Key))
                {
                    newPercs.Add(item.Key, (float)nums[item.Key] / totalNum);
                }
            }

            return newPercs;
        }

        /// <summary>
        /// Updates the floors outlines
        /// </summary>
        /// <param name="mode">The current preview mode</param>
        public virtual void UpdateFloorOutline(PreviewMode mode)
        {
            List<Line> lines = new List<Line>();
            float offsetDist = Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] + 0.1f;
            switch (mode)
            {
                case PreviewMode.Floors:
                    lines.Add(new Line(offsets[0][0][0].currentPosition, offsets[1][0][0].currentPosition, Color.black, Vector3.down, offsetDist));
                    lines.Add(new Line(offsets[1].Last()[1].currentPosition, offsets[0].Last()[1].currentPosition, Color.black, Vector3.down, offsetDist));
                    for (int i = 0; i < offsets.Count; i++)
                    {
                        for (int j = 0; j < offsets[i].Count; j++)
                        {
                            for (int k = 0; k < offsets[i][j].Count - 1; k++)
                            {
                                if (i % 2 == 0)
                                {
                                    lines.Add(new Line(offsets[i][j][k].currentPosition, offsets[i][j][k + 1].currentPosition, Color.black, Vector3.up, offsetDist));
                                }
                                else
                                {
                                    lines.Add(new Line(offsets[i][j][k].currentPosition, offsets[i][j][k + 1].currentPosition, Color.black, Vector3.down, offsetDist));
                                }
                            }
                        }
                    }
                    break;
                case PreviewMode.Buildings:
                    for (int m = 0; m < levels.Length; m++)
                    {
                        Vector3 addHeight = new Vector3(0, m * floor2floor, 0);
                        lines.Add(new Line(offsets[0][0][0].currentPosition + addHeight, offsets[1][0][0].currentPosition + addHeight, Color.black, Vector3.down, offsetDist));
                        lines.Add(new Line(offsets[1].Last()[1].currentPosition + addHeight, offsets[0].Last()[1].currentPosition + addHeight, Color.black, Vector3.down, offsetDist));
                        for (int i = 0; i < offsets.Count; i++)
                        {
                            for (int j = 0; j < offsets[i].Count; j++)
                            {
                                for (int k = 0; k < offsets[i][j].Count - 1; k++)
                                {
                                    if (i % 2 == 0)
                                    {
                                        lines.Add(new Line(offsets[i][j][k].currentPosition + addHeight, offsets[i][j][k + 1].currentPosition + addHeight, Color.black, Vector3.up, offsetDist));
                                    }
                                    else
                                    {
                                        lines.Add(new Line(offsets[i][j][k].currentPosition + addHeight, offsets[i][j][k + 1].currentPosition + addHeight, Color.black, Vector3.down, offsetDist));
                                    }
                                }
                            }
                        }
                    }
                    break;
            }

            if (plantRoom != null)
            {
                List<Line> plantRoomLines = new List<Line>();
                for (int i = 0; i < plantRoom.Count; i++)
                {
                    int index = (i + 1) % plantRoom.Count;
                    Line line = new Line(plantRoom[i].currentPosition, plantRoom[index].currentPosition, plantRoomColor);
                    Line line2 = new Line(plantRoom[i].currentPosition + new Vector3(0, floor2floor, 0), plantRoom[index].currentPosition + new Vector3(0, floor2floor, 0), plantRoomColor);
                    Line line3 = new Line(plantRoom[i].currentPosition + new Vector3(0, floor2floor, 0), plantRoom[i].currentPosition, plantRoomColor);

                    plantRoomLines.Add(line);
                    plantRoomLines.Add(line2);
                    plantRoomLines.Add(line3);
                }
                drawBuildingLines.AddPlantRoomLines(new KeyValuePair<string, List<Line>>(this.key, plantRoomLines));
            }
            drawBuildingLines.AddBuildingLines(new KeyValuePair<string, List<Line>>(this.key, lines));
        }

        /// <summary>
        /// Returns the statistics for this floor layout
        /// </summary>
        /// <returns>String</returns>
        public string GetFloorStats()
        {

            string levels = "";
            for (int i = 0; i < this.levels.Length; i++)
            {
                if (i > 0)
                {
                    levels += ("_" + this.levels[i]);
                }
                else
                {
                    levels += (this.levels[i]);
                }
            }
            //"GEA", "GIA", "NIA", "GrossToNet", "ApartmentsArea", "FacadeArea", "FacadePerimeter", "WallToFloor", "NumberOfPeople", "Floor2Floor", "Levels", "CorridorWidth", "ApartmentDepth" };
            var NIA = this.NIA;
            var GIA = this.GIA;
            var facadeArea = Mathf.Round(this.facadeArea);
            var GEA = this.GEA;
            List<string> data = new List<string>() { GEA.ToString(), GIA.ToString(), NIA.ToString(), (NIA / GIA).ToString(), Mathf.Round(apartmentsArea).ToString(), facadeArea.ToString(), Mathf.Round(facadePerimeter).ToString(), (facadeArea / GEA).ToString(), numberOfPeople.ToString(), floor2floor.ToString(), levels, corridorWidth.ToString(), apartmentDepth.ToString() };
            data.AddRange(apartmentNumbers.Select(x => x.ToString()).ToList());

            return string.Join(",", data.ToArray()); ;
        }

        /// <summary>
        /// Returns the statistics of this floor layout for a given level
        /// </summary>
        /// <param name="levelIndex">The index of the level</param>
        /// <returns>String</returns>
        public string GetFloorStats(int levelIndex)
        {

            string levels = this.levels[levelIndex].ToString();
            //"GEA", "GIA", "NIA", "GrossToNet", "ApartmentsArea", "FacadeArea", "FacadePerimeter", "WallToFloor", "NumberOfPeople", "Floor2Floor", "Levels", "CorridorWidth", "ApartmentDepth" };
            var NIA = Math.Round(this.NIA);
            var GIA = Math.Round(this.GIA);
            var facadeArea = Mathf.Round(this.facadeArea);
            var GEA = Math.Round(this.GEA);
            List<string> data = new List<string>() { GEA.ToString(), GIA.ToString(), NIA.ToString(), Math.Round((NIA / GIA) * 100, 2).ToString(), Mathf.Round(apartmentsArea).ToString(), facadeArea.ToString(), Mathf.Round(facadePerimeter).ToString(), Math.Round((facadeArea / GEA) * 100, 2).ToString(), numberOfPeople.ToString(), floor2floor.ToString(), levels, corridorWidth.ToString(), apartmentDepth.ToString() };
            data.AddRange(apartmentNumbers.Select(x => x.ToString()).ToList());

            return string.Join(",", data.ToArray()); ;
        }

        /// <summary>
        /// Returns the Facade area of a single level of this floor layout
        /// </summary>
        /// <returns>Float</returns>
        public float GetFacadeArea()
        {
            return GetFacadePerimeter() * floor2floor;
        }

        /// <summary>
        /// Returns the facade perimeter of a single level of this floor layout
        /// </summary>
        /// <returns></returns>
        public float GetFacadePerimeter()
        {
            if (exteriorPolygon != null)
            {
                return exteriorPolygon.Length();
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Returns the Floor Layout Save State
        /// </summary>
        /// <returns>FLoor Layout Save State</returns>
        public virtual FloorLayoutState GetLayoutState()
        {
            UpdateLayoutState();
            return saveState;
        }

        /// <summary>
        /// Updates the Floor Layout Save State
        /// </summary>
        public virtual void UpdateLayoutState()
        {
            saveState = new FloorLayoutState(building.index, id, false, centreLine.isCopy, apartmentDepth, corridorWidth, false, null);
            List<float> verts = new List<float>();
            for (int i = 0; i < centreLine.Count; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    verts.Add(centreLine[i].currentPosition[j]);
                }
            }
            saveState.centreLine = verts.ToArray();
            bool isCustom = (centreLine as PolygonSegment) != null;
            saveState.isCustom = isCustom;
            if (isCustom)
            {
                var segment = (centreLine as PolygonSegment);
                saveState.customParams = segment.curveLengths;
            }
            saveState.apartmentStates = new List<ApartmentState>();
            for (int i = 0; i < apartments.Count; i++)
            {
                saveState.apartmentStates.Add(apartments[i].GetApartmentState());
            }
            saveState.minApartmentWidth = building.prevMinimumWidth;
        }

        /// <summary>
        /// Starts the update of the building
        /// </summary>
        /// <param name="sender"></param>
        public void UpdateBuilding(Polygon sender)
        {
            if (!startEditing)
            {
                startEditing = true;
            }
            lastEditTime = DateTime.Now;
            this.sender = sender;
        }

        /// <summary>
        /// ADds an apartment to the floor layout
        /// </summary>
        /// <param name="sender">The apartment to be added</param>
        public void AddApartment(ApartmentUnity sender)
        {
            if (apartments != null)
            {
                apartments.Add(sender);
                sender.ProceduralFloor = this;
            }
        }

        /// <summary>
        /// Updates the floor to floor height and levels information of the floor layout
        /// </summary>
        /// <param name="f2f">Floor to floor height</param>
        /// <param name="levels">The levels as a list</param>
        public virtual void UpdateLevels(float f2f, int[] levels)
        {
            yVal = building.buildingElement.GetYForLevel(levels[0], id);
            this.levels = levels;
            floor2floor = f2f;
            if (hasGeometry)
            {
                transform.position = new Vector3(transform.position.x, yVal, transform.position.z);
                centreLine.transform.position = new Vector3(centreLine.transform.position.x, yVal, centreLine.transform.position.z);
                for (int i = 0; i < centreLine.Count; i++)
                {
                    centreLine[i].UpdatePosition(new Vector3(centreLine[i].currentPosition.x, yVal, centreLine[i].currentPosition.z));
                    if (centreLine[i].cpui != null)
                    {
                        centreLine[i].cpui.UpdateInitDiffs();
                    }
                }
                if (apartments != null)
                {
                    for (int i = 0; i < apartments.Count; i++)
                    {
                        apartments[i].editableMesh.UpdateVerticesPositions();
                    }
                }
                this.levels = levels;
                floor2floor = f2f;
                SetPreviewMode(building.buildingManager.previewMode);
                Offset();
                GenerateExteriorPolygon();
                UpdateFloorOutline(building.buildingManager.previewMode);
                building.OnFloorHeightChanged();
            }
        }

        /// <summary>
        /// Updates the plant-room for this floor
        /// </summary>
        public void UpdatePlantRoom()
        {
            GeneratePlantRoom();
            UpdateFloorOutline(building.buildingManager.previewMode);
        }

        /// <summary>
        /// Called when a vertex has been destroyed
        /// </summary>
        /// <param name="sender">The vertex that has been destroyed</param>
        public void OnVertexDestroyed(GeometryVertex sender)
        {
            var m_sender = sender as MeshVertex;
            if (m_sender != null)
            {
                floorVertices.Remove(m_sender);
                for (int i = 0; i < floorVertices.Count; i++)
                {
                    if (floorVertices[i] != null)
                    {
                        floorVertices[i].SetGroup(floorVertices);
                    }
                }
            }
        }

        /// <summary>
        /// Called when the apartments of the floor layout have changed
        /// </summary>
        public void OnApartmentsChanged()
        {
            for (int i = 0; i < apartments.Count; i++)
            {
                apartments[i].editableMesh.UpdateVertices();
            }
            for (int i = 0; i < floorVertices.Count; i++)
            {
                if (floorVertices[i] != null)
                {
                    floorVertices[i].SetGroup(floorVertices);
                }
            }
        }

        /// <summary>
        /// Called when the apartments of the floor layout have changed (in a Coroutine)
        /// </summary>
        /// <returns>IEnumerator</returns>
        public IEnumerator OnApartmentsChangedCoroutine()
        {
            yield return waitFrame;

            for (int i = 0; i < apartments.Count; i++)
            {
                apartments[i].editableMesh.UpdateVertices();
            }
            for (int i = 0; i < floorVertices.Count; i++)
            {
                floorVertices[i].SetGroup(floorVertices);
            }
        }

        /// <summary>
        /// Sends an overall update request to the procedural building manager
        /// </summary>
        public void RequestOverallUpdate()
        {
            building.RequestOverallUpdate();
        }


        /// <summary>
        /// Populates the apartments of the floor layout with the internal layout
        /// </summary>
        /// <param name="show">Whether the internal layouts should be visible</param>
        /// <param name="repRootParent">The parent object for the internal layouts' representation</param>
        /// <param name="keepMesh">Whether the apartments should keep their external meshes</param>
        public virtual void PopulateApartments(bool show, Transform repRootParent, bool keepMesh = false)
        {
            if (transform.GetChild(0).gameObject.activeSelf)
            {
                exteriorPolygon.gameObject.SetActive(!show);
                GameObject thisFloorRepParent = new GameObject("Floor Rep_" + gameObject.name);
                thisFloorRepParent.transform.SetParent(repRootParent);
                for (int i = 0; i < apartments.Count; i++)
                {
                    if (apartments[i].ApartmentType != "SpecialApt")
                    {
                        if (show)
                        {
                            if (apartments[i].Representation == null)
                            {
                                apartments[i].PopulateInternalLayout(proceduralApartmentLayout, apartmentDepth, thisFloorRepParent.transform, keepMesh);
                            }
                            else
                            {
                                apartments[i].ToggleRepresentation(show);
                            }
                        }
                        else
                        {
                            apartments[i].ToggleRepresentation(show);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sets the manufacturing system
        /// </summary>
        /// <param name="manufacturingSystem">The new manufacturing system</param>
        public void SetManufacturingSystem(ManufacturingSystem manufacturingSystem)
        {
            if (apartments != null)
            {
                for (int i = 0; i < apartments.Count; i++)
                {
                    if (apartments[i].ApartmentType != "SpecialApt")
                    {
                        if (apartments[i].Representation != null)
                        {
                            apartments[i].SetManufacturingSystem(manufacturingSystem);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Returns the statistics regarding the Vertical Modules of the floor layout
        /// </summary>
        /// <returns>Dictionary</returns>
        public Dictionary<string, int> GetVerticalModulesNumbers()
        {
            Dictionary<string, int> modulesNumbers = new Dictionary<string, int>();
            for (int i = 0; i < apartments.Count; i++)
            {
                if (apartments[i].ApartmentType != "SpecialApt")
                {
                    if (apartments[i].Representation != null)
                    {
                        var modules = apartments[i].GetVerticalModulesNumbers();
                        if (modules != null)
                        {
                            foreach (var item in modules)
                            {
                                if (modulesNumbers.ContainsKey(item.Key))
                                {
                                    modulesNumbers[item.Key] += item.Value;
                                }
                                else
                                {
                                    modulesNumbers.Add(item.Key, item.Value);
                                }
                            }
                        }
                    }
                }
            }
            return modulesNumbers;
        }

        /// <summary>
        /// Populates the apartments of the floor layout with the internal layout
        /// </summary>
        /// <param name="show">Whether the internal layouts should be visible</param>
        public void PopulateApartments(bool show)
        {
            exteriorPolygon.gameObject.SetActive(!show);
            for (int i = 0; i < apartments.Count; i++)
            {
                if (apartments[i].ApartmentType != "SpecialApt")
                {
                    apartments[i].editableMesh.gameObject.SetActive(!show);
                    apartments[i].editableMesh.meshExtrusion.transform.localScale = Vector3.one;
                }
            }
        }

        /// <summary>
        /// Generates the geometries of this floor layout
        /// </summary>
        /// <param name="maxWidth">The maximum apartment width</param>
        /// <returns>IEnumerator</returns>
        public IEnumerator GenerateGeometries(float maxWidth)
        {
            ResetLayout();
            if (!percentages.ContainsKey("Commercial") && !percentages.ContainsKey("Other"))
            {
                Offset();
                GenerateExteriorPolygon();
                yield return waitFrame;
                if (projectedCores)
                {
                    GetCoresPositions(maxWidth);
                }
                else
                {
                    GetCoresPositions(coresRadius, corridorWidth, maxWidth);
                }
                yield return waitFrame;
                GetCorridors();
                yield return waitFrame;
                GetApartments();
                yield return waitFrame;
                SetPreviewMode(building.buildingManager.previewMode);
            }
            else
            {
                OffsetCommercial();
                GenerateExteriorPolygon();
                yield return waitFrame;
                GenerateCommercialLayout(coresRadius, corridorWidth, maxWidth);
            }

            yield return waitFrame;
            if (levels.Length == 1 && levels[0] == 0)
            {
                GeneratePlantRoom();
                CheckCollisions();
            }
            hasGeometry = true;


        }

        /// <summary>
        /// Generates the geometries of this floor layout
        /// </summary>
        /// <param name="masterFloor">The base (ground floor) of the building</param>
        /// <param name="maxWidth">The maximum apartment width</param>
        /// <returns></returns>
        public IEnumerator GenerateGeometries(ProceduralFloor masterFloor, float maxWidth)
        {
            this.masterFloor = masterFloor;
            projectedCores = true;

            ResetLayout();
            if (!percentages.ContainsKey("Commercial") && !percentages.ContainsKey("Other"))
            {
                Offset();
                GenerateExteriorPolygon();
                yield return waitFrame;
                if (projectedCores)
                {
                    GetCoresPositions(maxWidth);
                }
                else
                {
                    GetCoresPositions(coresRadius, corridorWidth, maxWidth);
                }
                GetCorridors();
                yield return waitFrame;
                GetApartments();
                yield return waitFrame;
                SetPreviewMode(building.buildingManager.previewMode);
            }
            else
            {
                OffsetCommercial();
                GenerateExteriorPolygon();
                yield return waitFrame;
                GenerateCommercialLayout(coresRadius, corridorWidth, maxWidth);
            }
            yield return waitFrame;
            if (levels.Length == 1 && levels[0] == 0)
            {
                GeneratePlantRoom();
                CheckCollisions();
            }
            hasGeometry = true;
        }

        /// <summary>
        /// Loads the geometries of this floor layout
        /// </summary>
        /// <param name="loadState">The load data for the layout</param>
        /// <param name="masterFloor">The base (ground floor) of the building</param>
        public virtual void LoadGeometries(FloorLayoutState loadState, ProceduralFloor masterFloor)
        {
            if (masterFloor != null)
            {
                this.masterFloor = masterFloor;
            }
            Offset();
            GenerateExteriorPolygon();
            ResetLayout();
            if (!percentages.ContainsKey("Commercial") && !percentages.ContainsKey("Other"))
            {
                if (projectedCores)
                {
                    GetCoresPositions(building.prevMinimumWidth);
                }
                else
                {
                    GetCoresPositions(coresRadius, corridorWidth, building.prevMinimumWidth);
                }
                GetCorridors();
                LoadApartments(loadState);
                SetPreviewMode(building.buildingManager.previewMode);
            }
            else
            {
                LoadCommercialLayout(coresRadius, corridorWidth, loadState);
            }
            hasGeometry = true;
        }

        /// <summary>
        /// Generates the exterior polygon of the floor layout (outline of the external wall)
        /// </summary>
        public virtual void GenerateExteriorPolygon()
        {
            if (exteriorPolygon != null)
            {
                Destroy(exteriorPolygon.gameObject);
                exteriorPolygon = null;
            }

            float offsetDist = apartmentDepth + (Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] + Standards.TaggedObject.ConstructionFeatures["CorridorWall"]) + corridorWidth / 2;
            Vector3[] side1;
            Vector3[] side2;
            exteriorPolygon = Instantiate(polygonPrefab, transform).GetComponent<Polygon>();
            exteriorPolygon.gameObject.name = "ExteriorEnvelopePolygon";
            exteriorPolygon.GetComponent<MeshRenderer>().enabled = false;
            exteriorPolygon.transform.localPosition = Vector3.zero;
            if (typology == LinearTypology.Single)
            {
                side1 = centreLine.Offset(offsetDist);
                side2 = centreLine.Offset(-offsetDist);
            }
            else
            {
                if (coreAllignment == CoreAllignment.Left)
                {
                    side1 = centreLine.Offset(offsetDist);
                    side2 = centreLine.Offset(-(Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] + corridorWidth / 2));
                }
                else /*(coreAllignment == CoreAllignment.Right)*/
                {
                    side1 = centreLine.Offset((Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] + corridorWidth / 2));
                    side2 = centreLine.Offset(-offsetDist);
                }
            }

            side1[0] = side1[0] + (side1[0] - side1[1]).normalized * Standards.TaggedObject.ConstructionFeatures["ExteriorWall"];
            side2[0] = side2[0] + (side2[0] - side2[1]).normalized * Standards.TaggedObject.ConstructionFeatures["ExteriorWall"];
            side1[side1.Length - 1] = side1[side1.Length - 1] + (side1[side1.Length - 1] - side1[side1.Length - 2]).normalized * Standards.TaggedObject.ConstructionFeatures["ExteriorWall"];
            side2[side2.Length - 1] = side2[side2.Length - 1] + (side2[side2.Length - 1] - side2[side2.Length - 2]).normalized * Standards.TaggedObject.ConstructionFeatures["ExteriorWall"];

            side2 = side2.Reverse().ToArray();

            List<PolygonVertex> verts = new List<PolygonVertex>();

            for (int i = 0; i < side1.Length; i++)
            {
                PolygonVertex vert = Instantiate(polygonVertexPrefab, exteriorPolygon.transform).GetComponent<PolygonVertex>();
                vert.Initialize(i);
                vert.UpdatePosition(side1[i]);
                verts.Add(vert);
            }

            for (int i = 0; i < side2.Length; i++)
            {
                PolygonVertex vert = Instantiate(polygonVertexPrefab, exteriorPolygon.transform).GetComponent<PolygonVertex>();
                vert.Initialize(i);
                vert.UpdatePosition(side2[i]);
                verts.Add(vert);
            }

            exteriorPolygon.Initialize(verts, true, true, false);
            Extrusion outterEnvelope = Instantiate(extrusionPrefab, exteriorPolygon.transform).GetComponent<Extrusion>();
            if (building.buildingManager.previewMode == PreviewMode.Buildings)
            {
                outterEnvelope.totalHeight = floor2floor * levels.Length;
            }
            else if (building.buildingManager.previewMode == PreviewMode.Floors)
            {
                outterEnvelope.totalHeight = floor2floor;
            }
            outterEnvelope.Initialize(exteriorPolygon);
            exteriorPolygon.extrusion = outterEnvelope;
            Material material;
            if (percentages.ContainsKey("Commercial") || percentages.ContainsKey("Other"))
            {
                material = Resources.Load("Materials/Commercial") as Material;
            }
            else
            {
                material = Resources.Load("Materials/Envelope") as Material;
            }
            exteriorPolygon.SetOriginalMaterial(material);
        }

        /// <summary>
        /// Offset the centre line based on the apartment depth
        /// </summary>
        public void Offset()
        {
            if (offsets != null)
            {
                for (int i = 0; i < offsets.Count; i++)
                {
                    for (int j = 0; j < offsets[i].Count; j++)
                    {
                        Destroy(offsets[i][j].gameObject);
                    }
                }
            }

            offsets = new List<List<Polygon>>();

            Vector3[] firstSidePoints;
            Vector3[] secondSidePoints;
            List<Polygon> firstSide = new List<Polygon>();
            List<Polygon> secondSide = new List<Polygon>();
            float offsetDist = apartmentDepth + (Standards.TaggedObject.ConstructionFeatures["CorridorWall"]) + corridorWidth / 2;

            switch (typology)
            {
                case LinearTypology.Single:
                    firstSidePoints = centreLine.OffsetAsLineSegments(offsetDist);

                    for (int i = 0; i < firstSidePoints.Length; i += 2)
                    {

                        Polygon m_polygon = Instantiate(polygonPrefab, transform).GetComponent<Polygon>();
                        m_polygon.gameObject.name = "first_" + i;
                        PolygonVertex startPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                        startPoint.Initialize(i);
                        startPoint.UpdatePosition(firstSidePoints[i]);

                        PolygonVertex endPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                        endPoint.Initialize(i + 1);
                        endPoint.UpdatePosition(firstSidePoints[i + 1]);
                        m_polygon.Initialize(new List<PolygonVertex>() { startPoint, endPoint }, false);

                        firstSide.Add(m_polygon);
                    }
                    offsets.Add(firstSide);

                    secondSidePoints = centreLine.OffsetAsLineSegments(-offsetDist);

                    for (int i = 0; i < secondSidePoints.Length; i += 2)
                    {
                        Polygon m_polygon = Instantiate(polygonPrefab, transform).GetComponent<Polygon>();
                        m_polygon.gameObject.name = "second_" + i;
                        PolygonVertex startPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                        startPoint.Initialize(i);
                        startPoint.UpdatePosition(secondSidePoints[i]);

                        PolygonVertex endPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                        endPoint.Initialize(i + 1);
                        endPoint.UpdatePosition(secondSidePoints[i + 1]);

                        m_polygon.Initialize(new List<PolygonVertex>() { startPoint, endPoint }, false);

                        secondSide.Add(m_polygon);
                    }
                    offsets.Add(secondSide);
                    break;
                case LinearTypology.DeckAccess:
                    if (coreAllignment == CoreAllignment.Left)
                    {
                        firstSidePoints = centreLine.OffsetAsLineSegments(offsetDist);

                        for (int i = 0; i < firstSidePoints.Length; i += 2)
                        {

                            Polygon m_polygon = Instantiate(polygonPrefab, transform).GetComponent<Polygon>();
                            m_polygon.gameObject.name = "first_" + i;
                            PolygonVertex startPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                            startPoint.Initialize(i);
                            startPoint.UpdatePosition(firstSidePoints[i]);

                            PolygonVertex endPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                            endPoint.Initialize(i + 1);
                            endPoint.UpdatePosition(firstSidePoints[i + 1]);
                            m_polygon.Initialize(new List<PolygonVertex>() { startPoint, endPoint }, false);

                            firstSide.Add(m_polygon);
                        }
                        offsets.Add(firstSide);

                        secondSidePoints = centreLine.OffsetAsLineSegments(-(corridorWidth / 2));

                        for (int i = 0; i < secondSidePoints.Length; i += 2)
                        {
                            Polygon m_polygon = Instantiate(polygonPrefab, transform).GetComponent<Polygon>();
                            m_polygon.gameObject.name = "second_" + i;
                            PolygonVertex startPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                            startPoint.Initialize(i);
                            startPoint.UpdatePosition(secondSidePoints[i]);

                            PolygonVertex endPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                            endPoint.Initialize(i + 1);
                            endPoint.UpdatePosition(secondSidePoints[i + 1]);

                            m_polygon.Initialize(new List<PolygonVertex>() { startPoint, endPoint }, false);

                            secondSide.Add(m_polygon);
                        }
                        offsets.Add(secondSide);
                    }
                    else if (coreAllignment == CoreAllignment.Right)
                    {
                        firstSidePoints = centreLine.OffsetAsLineSegments(corridorWidth / 2);

                        for (int i = 0; i < firstSidePoints.Length; i += 2)
                        {

                            Polygon m_polygon = Instantiate(polygonPrefab, transform).GetComponent<Polygon>();
                            m_polygon.gameObject.name = "first_" + i;
                            PolygonVertex startPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                            startPoint.Initialize(i);
                            startPoint.UpdatePosition(firstSidePoints[i]);

                            PolygonVertex endPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                            endPoint.Initialize(i + 1);
                            endPoint.UpdatePosition(firstSidePoints[i + 1]);
                            m_polygon.Initialize(new List<PolygonVertex>() { startPoint, endPoint }, false);

                            firstSide.Add(m_polygon);
                        }
                        offsets.Add(firstSide);

                        secondSidePoints = centreLine.OffsetAsLineSegments(-offsetDist);

                        for (int i = 0; i < secondSidePoints.Length; i += 2)
                        {
                            Polygon m_polygon = Instantiate(polygonPrefab, transform).GetComponent<Polygon>();
                            m_polygon.gameObject.name = "second_" + i;
                            PolygonVertex startPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                            startPoint.Initialize(i);
                            startPoint.UpdatePosition(secondSidePoints[i]);

                            PolygonVertex endPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                            endPoint.Initialize(i + 1);
                            endPoint.UpdatePosition(secondSidePoints[i + 1]);

                            m_polygon.Initialize(new List<PolygonVertex>() { startPoint, endPoint }, false);

                            secondSide.Add(m_polygon);
                        }
                        offsets.Add(secondSide);
                    }
                    break;
            }

            CheckIntersections(offsets);
        }

        /// <summary>
        /// Offset the centre line based on the apartment depth (if Commercial use is set)
        /// </summary>
        public void OffsetCommercial()
        {
            if (offsets != null)
            {
                for (int i = 0; i < offsets.Count; i++)
                {
                    for (int j = 0; j < offsets[i].Count; j++)
                    {
                        Destroy(offsets[i][j].gameObject);
                    }
                }
            }
            float offsetDist = apartmentDepth + (Standards.TaggedObject.ConstructionFeatures["CorridorWall"]) + corridorWidth / 2;
            offsets = new List<List<Polygon>>();
            if (typology == LinearTypology.Single)
            {
                var firstSidePoints = centreLine.OffsetAsLineSegments(offsetDist);
                List<Polygon> firstSide = new List<Polygon>();

                for (int i = 0; i < firstSidePoints.Length; i += 2)
                {

                    Polygon m_polygon = Instantiate(polygonPrefab, transform).GetComponent<Polygon>();
                    m_polygon.gameObject.name = "first_" + i;
                    PolygonVertex startPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                    startPoint.Initialize(i);
                    startPoint.UpdatePosition(firstSidePoints[i]);

                    PolygonVertex endPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                    endPoint.Initialize(i + 1);
                    endPoint.UpdatePosition(firstSidePoints[i + 1]);
                    m_polygon.Initialize(new List<PolygonVertex>() { startPoint, endPoint }, false);

                    firstSide.Add(m_polygon);
                }
                offsets.Add(firstSide);

                var secondSidePoints = centreLine.OffsetAsLineSegments(-offsetDist);
                List<Polygon> secondSide = new List<Polygon>();

                for (int i = 0; i < secondSidePoints.Length; i += 2)
                {
                    Polygon m_polygon = Instantiate(polygonPrefab, transform).GetComponent<Polygon>();
                    m_polygon.gameObject.name = "second_" + i;
                    PolygonVertex startPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                    startPoint.Initialize(i);
                    startPoint.UpdatePosition(secondSidePoints[i]);

                    PolygonVertex endPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                    endPoint.Initialize(i + 1);
                    endPoint.UpdatePosition(secondSidePoints[i + 1]);

                    m_polygon.Initialize(new List<PolygonVertex>() { startPoint, endPoint }, false);

                    secondSide.Add(m_polygon);
                }
                offsets.Add(secondSide);
            }
            else if (typology == LinearTypology.DeckAccess)
            {
                if (coreAllignment == CoreAllignment.Left)
                {
                    var firstSidePoints = centreLine.OffsetAsLineSegments(offsetDist);
                    List<Polygon> firstSide = new List<Polygon>();

                    for (int i = 0; i < firstSidePoints.Length; i += 2)
                    {

                        Polygon m_polygon = Instantiate(polygonPrefab, transform).GetComponent<Polygon>();
                        m_polygon.gameObject.name = "first_" + i;
                        PolygonVertex startPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                        startPoint.Initialize(i);
                        startPoint.UpdatePosition(firstSidePoints[i]);

                        PolygonVertex endPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                        endPoint.Initialize(i + 1);
                        endPoint.UpdatePosition(firstSidePoints[i + 1]);
                        m_polygon.Initialize(new List<PolygonVertex>() { startPoint, endPoint }, false);

                        firstSide.Add(m_polygon);
                    }
                    offsets.Add(firstSide);

                    var secondSidePoints = centreLine.OffsetAsLineSegments(-(corridorWidth / 2));
                    List<Polygon> secondSide = new List<Polygon>();

                    for (int i = 0; i < secondSidePoints.Length; i += 2)
                    {
                        Polygon m_polygon = Instantiate(polygonPrefab, transform).GetComponent<Polygon>();
                        m_polygon.gameObject.name = "second_" + i;
                        PolygonVertex startPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                        startPoint.Initialize(i);
                        startPoint.UpdatePosition(secondSidePoints[i]);

                        PolygonVertex endPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                        endPoint.Initialize(i + 1);
                        endPoint.UpdatePosition(secondSidePoints[i + 1]);

                        m_polygon.Initialize(new List<PolygonVertex>() { startPoint, endPoint }, false);

                        secondSide.Add(m_polygon);
                    }
                    offsets.Add(secondSide);
                }
                else if (coreAllignment == CoreAllignment.Right)
                {
                    var firstSidePoints = centreLine.OffsetAsLineSegments(corridorWidth / 2);
                    List<Polygon> firstSide = new List<Polygon>();

                    for (int i = 0; i < firstSidePoints.Length; i += 2)
                    {

                        Polygon m_polygon = Instantiate(polygonPrefab, transform).GetComponent<Polygon>();
                        m_polygon.gameObject.name = "first_" + i;
                        PolygonVertex startPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                        startPoint.Initialize(i);
                        startPoint.UpdatePosition(firstSidePoints[i]);

                        PolygonVertex endPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                        endPoint.Initialize(i + 1);
                        endPoint.UpdatePosition(firstSidePoints[i + 1]);
                        m_polygon.Initialize(new List<PolygonVertex>() { startPoint, endPoint }, false);

                        firstSide.Add(m_polygon);
                    }
                    offsets.Add(firstSide);

                    var secondSidePoints = centreLine.OffsetAsLineSegments(-offsetDist);
                    List<Polygon> secondSide = new List<Polygon>();

                    for (int i = 0; i < secondSidePoints.Length; i += 2)
                    {
                        Polygon m_polygon = Instantiate(polygonPrefab, transform).GetComponent<Polygon>();
                        m_polygon.gameObject.name = "second_" + i;
                        PolygonVertex startPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                        startPoint.Initialize(i);
                        startPoint.UpdatePosition(secondSidePoints[i]);

                        PolygonVertex endPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                        endPoint.Initialize(i + 1);
                        endPoint.UpdatePosition(secondSidePoints[i + 1]);

                        m_polygon.Initialize(new List<PolygonVertex>() { startPoint, endPoint }, false);

                        secondSide.Add(m_polygon);
                    }
                    offsets.Add(secondSide);
                }
            }

            //UpdateLineDrawing();
            CheckIntersections(offsets);
        }

        /// <summary>
        /// Load the floor layout if Commercial use is set
        /// </summary>
        /// <param name="coresRadius">The radius between the cores</param>
        /// <param name="corridorWidth">The width of the corridor</param>
        /// <param name="loadState">The load data</param>
        public void LoadCommercialLayout(float coresRadius, float corridorWidth, FloorLayoutState loadState)
        {
            Material commercialMaterial = Resources.Load("Materials/Commercial") as Material;
            if (coresParent != null)
            {
                Destroy(coresParent);
            }
            coresParent = new GameObject("Cores");
            coresParent.transform.SetParent(transform);
            coresParent.transform.localPosition = Vector3.zero;
            floorVertices = new List<GeometryVertex>();

            if (commercialPolygons != null)
            {
                for (int i = 0; i < commercialPolygons.Count; i++)
                {
                    Destroy(commercialPolygons[i].gameObject);
                }
            }

            commercialPolygons = new List<Polygon>();

            if (commercialParent == null)
            {
                commercialParent = new GameObject("Comm/Other").transform;
                commercialParent.transform.SetParent(transform);
                commercialParent.transform.localPosition = Vector3.zero;
            }

            GetCoresPositions(coresRadius, corridorWidth, building.prevMinimumWidth);

            for (int i = 0; i < offsets[0].Count; i++)
            {
                Polygon m_polygon = Instantiate(polygonPrefab, commercialParent).GetComponent<Polygon>();
                m_polygon.gameObject.name = "Comm/Other_" + i;
                List<PolygonVertex> verts = new List<PolygonVertex>() { offsets[0][i][0], offsets[0][i][1], offsets[1][i][1], offsets[1][i][0] };
                m_polygon.Initialize(verts, true, true);
                Extrusion m_extrusion = Instantiate(extrusionPrefab, m_polygon.transform).GetComponent<Extrusion>();
                m_extrusion.totalHeight = floor2floor * levels.Length;
                m_extrusion.Initialize(m_polygon);
                m_polygon.SetOriginalMaterial(commercialMaterial);
                commercialPolygons.Add(m_polygon);
            }
        }

        /// <summary>
        /// Generates the geometries of the floor layout if Commercial use is set
        /// </summary>
        /// <param name="coresRadius">The radius between the cores</param>
        /// <param name="corridorWidth">The width of the corridor</param>
        /// <param name="maxWidth">The maximum apartment width</param>
        public void GenerateCommercialLayout(float coresRadius, float corridorWidth, float maxWidth)
        {
            Material commercialMaterial = Resources.Load("Materials/Commercial") as Material;
            if (coresParent != null)
            {
                Destroy(coresParent);
            }
            coresParent = new GameObject("Cores");
            coresParent.transform.SetParent(transform);
            coresParent.transform.localPosition = Vector3.zero;
            floorVertices = new List<GeometryVertex>();

            if (commercialPolygons != null)
            {
                for (int i = 0; i < commercialPolygons.Count; i++)
                {
                    Destroy(commercialPolygons[i].gameObject);
                }
            }

            commercialPolygons = new List<Polygon>();

            if (commercialParent == null)
            {
                commercialParent = new GameObject("Comm/Other").transform;
                commercialParent.transform.SetParent(transform);
                commercialParent.transform.localPosition = Vector3.zero;
            }

            if (projectedCores)
            {
                GetCoresPositions(maxWidth);
            }
            else
            {
                GetCoresPositions(coresRadius, corridorWidth, maxWidth);
            }

            for (int i = 0; i < offsets[0].Count; i++)
            {
                Polygon m_polygon = Instantiate(polygonPrefab, commercialParent).GetComponent<Polygon>();
                m_polygon.gameObject.name = "Comm/Other_" + i;
                List<PolygonVertex> verts = new List<PolygonVertex>() { offsets[0][i][0], offsets[0][i][1], offsets[1][i][1], offsets[1][i][0] };
                m_polygon.Initialize(verts, true, true);
                Extrusion m_extrusion = Instantiate(extrusionPrefab, m_polygon.transform).GetComponent<Extrusion>();
                m_extrusion.totalHeight = floor2floor * levels.Length;
                m_extrusion.Initialize(m_polygon);
                m_polygon.SetOriginalMaterial(commercialMaterial);
                commercialPolygons.Add(m_polygon);
            }
        }

        /// <summary>
        /// Generates the plant room for this floor layout
        /// </summary>
        public void GeneratePlantRoom()
        {
            GeneratePlantRoom(building.GetBuildingPlantArea());//buildingManager.GetBuildingPlantArea(int.Parse(key.Split(',')[0])));
        }

        /// <summary>
        /// Generates the plant room for this floor layout
        /// </summary>
        /// <param name="area">The required area for the plant room</param>
        public virtual void GeneratePlantRoom(float area)
        {
            if (plantRoom != null)
            {
                Destroy(plantRoom.gameObject);
            }
            Material plantRoomMaterial = Resources.Load("Materials/PlantRoom") as Material;

            float requiredLength;
            float offsetDist = apartmentDepth + Standards.TaggedObject.ConstructionFeatures["CorridorWall"] + corridorWidth / 2;
            if (typology == LinearTypology.Single)
            {
                requiredLength = area / (2 * offsetDist);
            }
            else
            {
                requiredLength = area / offsetDist;
            }

            if (requiredLength > centreLine.Length())
            {
                requiredLength = centreLine.Length();
                plantRoomColor = Color.red;
                if (!Notifications.TaggedObject.activeNotifications.Contains("PlantRoom"))
                {
                    Notifications.TaggedObject.activeNotifications.Add("PlantRoom");
                    Notifications.TaggedObject.UpdateNotifications();
                }
            }
            else
            {
                plantRoomColor = Color.black;
                if (Notifications.TaggedObject.activeNotifications.Contains("PlantRoom"))
                {
                    Notifications.TaggedObject.activeNotifications.Remove("PlantRoom");
                    Notifications.TaggedObject.UpdateNotifications();
                }
            }

            var length = centreLine.Length() - requiredLength;

            var verts = centreLine.Split(length);

            Polygon temp_Centre = Instantiate(polygonPrefab).GetComponent<Polygon>();
            List<PolygonVertex> temp_Verts = new List<PolygonVertex>();
            for (int j = 0; j < verts[1].Length; j++)
            {
                var vert = Instantiate(polygonVertexPrefab).GetComponent<PolygonVertex>();
                vert.Initialize(j);
                vert.UpdatePosition(verts[1][j]);
                temp_Verts.Add(vert);
            }
            temp_Centre.Initialize(temp_Verts, false, false, false);

            List<PolygonVertex> roomVertices = new List<PolygonVertex>();

            Vector3[] points1;
            Vector3[] points2;
            if (typology == LinearTypology.Single)
            {
                points1 = temp_Centre.Offset(offsetDist);
                points2 = temp_Centre.Offset(-offsetDist);
            }
            else
            {
                if (coreAllignment == CoreAllignment.Left)
                {
                    points1 = temp_Centre.Offset(offsetDist);
                    points2 = temp_Centre.Offset(-(corridorWidth / 2));
                }
                else /*(coreAllignment == CoreAllignment.Right)*/
                {
                    points1 = temp_Centre.Offset((corridorWidth / 2));
                    points2 = temp_Centre.Offset(-offsetDist);
                }
            }
            points2 = points2.Reverse().ToArray();

            Polygon roomPerimeter = Instantiate(polygonPrefab, transform).GetComponent<Polygon>();
            roomPerimeter.GetComponent<MeshRenderer>().enabled = false;
            roomPerimeter.gameObject.name = "PlantRoom";

            for (int i = 0; i < points1.Length; i++)
            {
                var vert = Instantiate(polygonVertexPrefab, roomPerimeter.transform).GetComponent<PolygonVertex>();
                vert.Initialize(i);
                vert.UpdatePosition(points1[i]);
                roomVertices.Add(vert);
            }

            for (int i = 0; i < points2.Length; i++)
            {
                var vert = Instantiate(polygonVertexPrefab, roomPerimeter.transform).GetComponent<PolygonVertex>();
                vert.Initialize(i);
                vert.UpdatePosition(points2[i]);
                roomVertices.Add(vert);
            }
            roomPerimeter.Initialize(roomVertices, true, true, false);

            Extrusion roomExtrusion = Instantiate(extrusionPrefab, roomPerimeter.transform).GetComponent<Extrusion>();
            roomExtrusion.totalHeight = floor2floor;
            roomExtrusion.GetComponent<MeshRenderer>().enabled = false;
            roomExtrusion.Initialize(roomPerimeter);
            roomPerimeter.SetOriginalMaterial(plantRoomMaterial);
            plantRoom = roomPerimeter;
            Destroy(temp_Centre.gameObject);
        }

        /// <summary>
        /// Check whether ther are collisions between apartments and the plant room
        /// </summary>
        public void CheckCollisions()
        {
            for (int i = 0; i < apartments.Count; i++)
            {
                if (plantRoom.IsIncluded(apartments[i].editableMesh.Centre + new Vector3(0, 0.5f, 0)))
                {
                    Destroy(apartments[i].gameObject);
                    continue;
                }
                for (int j = 0; j < apartments[i].editableMesh.Count; j++)
                {
                    if (plantRoom.IsIncluded(apartments[i].editableMesh[j].currentPosition + (apartments[i].editableMesh.Centre - apartments[i].editableMesh[j].currentPosition).normalized * 0.5f))
                    {
                        Destroy(apartments[i].gameObject);
                        continue;
                    }
                }
            }
        }

        /// <summary>
        /// Revaluates the neighbours of each apartment in the layout
        /// </summary>
        public void ReEvaluateLayoutNeighbours()
        {
            for (int i = 0; i < apartments.Count; i++)
            {
                if (apartments[i].ApartmentType == "SpecialApt" && !apartments[i].IsCorner)
                {
                    apartments[i].SetCloseOnes(apartments);
                }
            }
            floorVertices.RemoveAll(item => item == null);
            for (int i = 0; i < floorVertices.Count; i++)
            {
                floorVertices[i].SetGroup(floorVertices);
            }
        }

        /// <summary>
        /// Sets the preview mode
        /// </summary>
        /// <param name="previewMode">The new preview mode</param>
        public void SetPreviewMode(PreviewMode previewMode)
        {
            if (hasGeometry)
            {
                switch (previewMode)
                {
                    case PreviewMode.Floors:
                        if (commercialPolygons != null)
                        {
                            for (int i = 0; i < commercialPolygons.Count; i++)
                            {
                                commercialPolygons[i].capped = false;
                                commercialPolygons[i].SetExtrusionHeight(floor2floor);
                            }
                        }
                        for (int i = 0; i < apartments.Count; i++)
                        {
                            apartments[i].editableMesh.capped = false;
                            apartments[i].editableMesh.SetExtrusionHeight(floor2floor);
                            apartments[i].editableMesh.SetTypeMaterialAsMain();
                            apartments[i].editableMesh.TypeHighlight(false);
                        }
                        for (int i = 0; i < coresParent.transform.childCount; i++)
                        {
                            coresParent.transform.GetChild(i).localPosition = new Vector3(coresParent.transform.GetChild(i).localPosition.x, floor2floor / 2.0f, coresParent.transform.GetChild(i).localPosition.z);
                            coresParent.transform.GetChild(i).localScale = new Vector3(coresParent.transform.GetChild(i).localScale.x, floor2floor, coresParent.transform.GetChild(i).localScale.z);
                        }
                        if (exteriorPolygon != null)
                        {
                            exteriorPolygon.SetExtrusionHeight(floor2floor);
                        }
                        break;
                    case PreviewMode.Buildings:
                        if (commercialPolygons != null)
                        {
                            for (int i = 0; i < commercialPolygons.Count; i++)
                            {
                                commercialPolygons[i].capped = true;
                                commercialPolygons[i].SetExtrusionHeight(floor2floor * levels.Length);
                            }
                        }
                        for (int i = 0; i < apartments.Count; i++)
                        {
                            apartments[i].editableMesh.capped = true;
                            apartments[i].editableMesh.SetExtrusionHeight(floor2floor * levels.Length);
                            apartments[i].editableMesh.originalMaterial = originalMaterial;
                            apartments[i].editableMesh.TypeHighlight(false);
                        }
                        for (int i = 0; i < coresParent.transform.childCount; i++)
                        {
                            coresParent.transform.GetChild(i).localPosition = new Vector3(coresParent.transform.GetChild(i).localPosition.x, (floor2floor / 2.0f) * levels.Length, coresParent.transform.GetChild(i).localPosition.z);
                            coresParent.transform.GetChild(i).localScale = new Vector3(coresParent.transform.GetChild(i).localScale.x, floor2floor * levels.Length, coresParent.transform.GetChild(i).localScale.z);
                        }

                        if (exteriorPolygon != null)
                        {
                            exteriorPolygon.SetExtrusionHeight(floor2floor * levels.Length);
                        }
                        break;
                }

                UpdateFloorOutline(previewMode);
            }
        }

        /// <summary>
        /// Returns the GIA of a single level of the floor layout
        /// </summary>
        /// <returns>Float</returns>
        public virtual float GetGIA()
        {
            if (centreLine != null)
            {
                float side1;
                float side2;
                if (typology == LinearTypology.Single)
                {
                    side1 = side2 = apartmentDepth + (Standards.TaggedObject.ConstructionFeatures["CorridorWall"]) + corridorWidth / 2;
                }
                else
                {
                    side1 = apartmentDepth + (Standards.TaggedObject.ConstructionFeatures["CorridorWall"]) + corridorWidth / 2;
                    side2 = corridorWidth / 2;
                }
                return centreLine.Length() * (side1 + side2);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Sets the levels of this floor
        /// </summary>
        /// <param name="levels">The new levels</param>
        public virtual void SetLevels(int[] levels)
        {
            this.levels = levels;
        }

        /// <summary>
        /// Creates corner apartments from a list of vertices
        /// </summary>
        /// <param name="type">The type of the apartment</param>
        /// <param name="verts">The array with the vectors of the vertices</param>
        /// <param name="i">The index of the apartment</param>
        /// <param name="isExterior">Whether it belongs to an external corner</param>
        public void CreateCornerApartment(string type, Vector3[] verts, int i, bool isExterior)
        {
            int index = aptParent.transform.childCount;
            i = index;
            GameObject apartment = Instantiate(apartmentPrefab, aptParent.transform);
            apartment.name = "Apartment_" + i + "_" + type;
            apartment.transform.position = verts.Last();
            apartment.transform.LookAt(verts[0]);

            ApartmentUnity aptU = apartment.GetComponent<ApartmentUnity>();
            aptU.hasExtrusion = true;
            aptU.extrusionMoveable = false;
            aptU.extrusionEditEdges = true;
            aptU.extrusionCapped = false;
            //Apartment apt = new Apartment(id, 0, type);
            aptU.Initialize(id, type);
            AddApartment(aptU);

            aptU.IsCorner = true;
            aptU.isExterior = isExterior;
            //aptU.buildingManager = buildingManager;

            GameObject meshObject = new GameObject("MeshObject");
            meshObject.transform.SetParent(apartment.transform);
            meshObject.transform.localPosition = Vector3.zero;
            meshObject.transform.localEulerAngles = Vector3.zero;
            meshObject.layer = 8;
            var filter = meshObject.AddComponent<MeshFilter>();
            var renderer = meshObject.AddComponent<MeshRenderer>();
            var collider = meshObject.AddComponent<MeshCollider>();

            Mesh mesh = new Mesh();
            Vector3[] meshVerts = new Vector3[verts.Length];
            for (int j = 0; j < meshVerts.Length; j++)
            {
                meshVerts[j] = meshObject.transform.InverseTransformPoint(verts[j]);
                meshVerts[j] = new Vector3(meshVerts[j].x, 0, meshVerts[j].z);
            }
            mesh.vertices = meshVerts;
            if (meshVerts.Length == 3)
            {
                mesh.triangles = new int[] { 0, 2, 1 };
            }
            else
            {
                mesh.triangles = new int[] { 0, 2, 1, 0, 3, 2 };
            }
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();

            filter.sharedMesh = mesh;
            collider.sharedMesh = mesh;
            renderer.material = originalMaterial;

            var editMesh = meshObject.AddComponent<EditableMesh>();
            editMesh.capped = false;
            editMesh.editEdges = true;
            editMesh.moveable = false;
            editMesh.originalMaterial = originalMaterial;
            editMesh.typeMaterial = new Material(typeMaterial);
            editMesh.SetTypeColor(Standards.TaggedObject.ApartmentTypesColours[type]);
            editMesh.Initialize(editMesh.GetComponent<MeshFilter>().sharedMesh, editMesh.transform);
            var temp_verts = editMesh.vertices.Select(x => (GeometryVertex)x).ToList();
            floorVertices.AddRange(temp_verts);
            aptU.SetEditableMesh(editMesh);
            aptU.editableMesh.TypeHighlight(false);
            editMesh.meshExtrusion.GetComponent<MeshCollider>().convex = true;
        }

        /// <summary>
        /// Toggles the children of this floor on or off
        /// </summary>
        /// <param name="show">On or Off</param>
        public void ToggleChildren(bool show)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(show);
            }
        }

        /// <summary>
        /// Creates an apartment based on a list of vertices
        /// </summary>
        /// <param name="type">The type of the apartment</param>
        /// <param name="verts">The list of vectors of the vertices</param>
        /// <param name="i">The index of the apartment</param>
        /// <param name="corridorIndex">The index of the corridor the apartment belongs</param>
        /// <param name="hasCorridors">Whether the modules of the apartment should include the corridor</param>
        /// <param name="name">The name of the gameObject of the apartment</param>
        /// <param name="_tris">The list of triangles for the mesh</param>
        /// <param name="flipped">Whether the apartment is flipped or not</param>
        /// <returns>Apartment Unity Component</returns>
        public ApartmentUnity CreateApartment(string type, Vector3[] verts, int i, int corridorIndex, bool hasCorridors = false, string name = "", int[] _tris = null, bool flipped = false)
        {
            int index = aptParent.transform.childCount;
            i = index;
            GameObject apartment = Instantiate(apartmentPrefab, aptParent.transform);
            if (string.IsNullOrEmpty(name))
            {
                apartment.name = "Apartment_" + i + "_" + type + name;
            }
            else
            {
                apartment.name = name;
            }
            apartment.transform.position = verts[3];
            apartment.transform.LookAt(verts[2]);

            ApartmentUnity aptU = apartment.GetComponent<ApartmentUnity>();
            aptU.HasCorridor = false;//hasCorridors;
            aptU.hasExtrusion = true;
            aptU.extrusionMoveable = false;
            aptU.extrusionEditEdges = true;
            aptU.extrusionCapped = false;
            aptU.isExterior = true;
            aptU.flipped = flipped;
            //Apartment apt = new Apartment(id, 0, type);
            aptU.Initialize(id, type);

            if (apartment.name.Contains("_Core"))
            {
                aptU.isCoreResidual = true;
            }

            AddApartment(aptU);

            //aptU.buildingManager = buildingManager;
            aptU.activeCorridorIndex = corridorIndex;

            GameObject meshObject = new GameObject("MeshObject");
            meshObject.transform.SetParent(apartment.transform);
            meshObject.transform.localPosition = Vector3.zero;
            meshObject.transform.localEulerAngles = Vector3.zero;
            meshObject.layer = 8;
            var filter = meshObject.AddComponent<MeshFilter>();
            var renderer = meshObject.AddComponent<MeshRenderer>();
            var collider = meshObject.AddComponent<MeshCollider>();

            Mesh mesh = new Mesh();
            Vector3[] meshVerts = new Vector3[verts.Length];
            Vector2[] uvs = new Vector2[verts.Length];
            for (int j = 0; j < meshVerts.Length; j++)
            {
                meshVerts[j] = meshObject.transform.InverseTransformPoint(verts[j]);
                uvs[j] = new Vector2(meshVerts[j].x, meshVerts[j].z);
            }
            mesh.vertices = meshVerts;

            if (_tris == null)
            {
                mesh.triangles = new int[] { 0, 2, 1, 0, 3, 2 };
            }
            else
            {
                mesh.triangles = _tris;
            }
            mesh.uv = uvs;
            mesh.uv2 = uvs;
            mesh.uv3 = uvs;
            mesh.uv4 = uvs;
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();

            filter.sharedMesh = mesh;
            collider.sharedMesh = mesh;
            renderer.material = originalMaterial;

            var editMesh = meshObject.AddComponent<EditableMesh>();
            editMesh.capped = false;
            editMesh.editEdges = true;
            editMesh.moveable = false;
            editMesh.originalMaterial = originalMaterial;
            editMesh.typeMaterial = new Material(typeMaterial);
            editMesh.SetTypeColor(Standards.TaggedObject.ApartmentTypesColours[type]);
            editMesh.Initialize(editMesh.GetComponent<MeshFilter>().sharedMesh, editMesh.transform);
            var temp_verts = editMesh.vertices.Select(x => (GeometryVertex)x).ToList();
            floorVertices.AddRange(temp_verts);
            aptU.SetEditableMesh(editMesh);
            aptU.editableMesh.TypeHighlight(false);
            editMesh.meshExtrusion.GetComponent<MeshCollider>().convex = true;
            return aptU;
        }

        /// <summary>
        /// Highlights the apartments of the floor layout which are of a given type
        /// </summary>
        /// <param name="type">The type of the apartments</param>
        /// <param name="show">Toggle the highlight</param>
        public List<ApartmentUnity> ColorApartmentsOfType(string type, bool show)
        {
            if (apartments != null)
            {
                List<ApartmentUnity> apts = new List<ApartmentUnity>();
                for (int i = 0; i < apartments.Count; i++)
                {
                    if (apartments[i].ApartmentType == type)
                    {
                        apartments[i].HighlightType(show);
                        apts.Add(apartments[i]);
                    }
                }
                return apts;
            }
            else return new List<ApartmentUnity>();
        }

        /// <summary>
        /// Returns information about the floor layout
        /// </summary>
        /// <returns>String Array</returns>
        public override string[] GetInfoText()
        {
            string title = string.Format("{0} Info:\r\n", gameObject.name);

            string info = "";
            info += string.Format("Floor id: {0}\r\n", id);
            info += string.Format("Floor total area: {0}\r\n", Math.Round(GEA));
            info += string.Format("Apartments area: {0}\r\n", Math.Round(apartmentsArea));
            info += string.Format("Number of People: {0}\r\n", numberOfPeople);
            info += "Levels: \r\n";
            string levelLine = "";
            for (int i = 0; i < levels.Length; i++)
            {
                levelLine += (levels[i] + ",");
            }
            info += (levelLine + "\r\n");

            return new string[2] { title, info };
        }

        /// <summary>
        /// Updates the list of unresolvable apartments
        /// </summary>
        public void UpdateUnresolvables()
        {
            for (int i = 0; i < apartments.Count; i++)
            {
                apartments[i].unresolvableOnLeft = false;
                apartments[i].unresolvableOnRight = false;
                apartments[i].CheckNeighbours();
            }

            for (int i = 0; i < unresolvables.Count; i++)
            {
                Vector3 centre = unresolvables[i].editableMesh.meshExtrusion.GetComponent<MeshRenderer>().bounds.center;
                Ray r1 = new Ray(centre, unresolvables[i].transform.right);
                Ray r2 = new Ray(centre, -unresolvables[i].transform.right);
                RaycastHit hit;
                if (Physics.Raycast(r1, out hit, unresolvables[i].Width * 1.5f))
                {
                    unresolvables[i].resRight = hit.collider.transform.parent.parent.gameObject;
                    hit.collider.transform.parent.parent.GetComponent<ApartmentUnity>().unresolvableOnLeft = true;
                    hit.collider.transform.parent.parent.GetComponent<ApartmentUnity>().editableMesh.constrainUnresolvedL = true;
                }
                if (Physics.Raycast(r2, out hit, unresolvables[i].Width * 1.5f))
                {
                    unresolvables[i].resLeft = hit.collider.transform.parent.parent.gameObject;
                    hit.collider.transform.parent.parent.GetComponent<ApartmentUnity>().unresolvableOnRight = true;
                    hit.collider.transform.parent.parent.GetComponent<ApartmentUnity>().editableMesh.constrainUnresolvedR = true;
                }
            }
        }
        #endregion

        #region Private Methods

        private bool ProjectedInside(Vector3 point, Vector3[] line, out Vector3 projection)
        {
            projection = BrydenWoodUtils.ProjectOnCurve(line[0], line[1], point);
            float len1 = Vector3.Distance(projection, line[0]);
            float len2 = Vector3.Distance(projection, line[1]);
            float length = Vector3.Distance(line[0], line[1]);
            return (len1 - length < 0.01f) && (len2 - length < 0.01f);
        }

        private int FindClosestPoint(Vector3[] points, Vector3 point)
        {
            int index = -1;
            float minDist = float.MaxValue;

            for (int i = 0; i < points.Length; i++)
            {
                if (Vector3.Distance(points[i], point) < minDist)
                {
                    index = i;
                    minDist = Vector3.Distance(points[i], point);
                }
            }
            return index;
        }

        private List<Vector3> ModifyCorridorCentre(Polygon side, List<Vector3[]> coreCentres, float coreLength, float coreWidth, out List<Line> actives, Polygon exterior, bool createApts = true)
        {
            List<Vector3> newPoints = new List<Vector3>();
            actives = new List<Line>();
            newPoints.Add(side[0].currentPosition);
            for (int i = 0; i < coreCentres.Count + 1; i++)
            {
                if (i < coreCentres.Count)
                {
                    Vector3 projection = BrydenWoodUtils.ProjectOnCurve(side[0].currentPosition, side[1].currentPosition, coreCentres[i][0]);
                    Vector3 normal = (projection - coreCentres[i][0]).normalized;//Vector3.Cross(side[1].currentPosition- side[0].currentPosition, Vector3.up).normalized;
                    float dist1 = (coreLength + corridorWidth);
                    float dist2 = (coreWidth + corridorWidth / 2);

                    Vector3 point0 = projection + (side[0].currentPosition - projection).normalized * dist1 + normal * dist2;
                    Vector3 point1 = projection + (side[1].currentPosition - projection).normalized * dist1 + normal * dist2;

                    newPoints.Add(projection + (side[0].currentPosition - projection).normalized * dist1);
                    newPoints.Add(newPoints.Last() + normal * dist2);
                    newPoints.Add(projection + (side[1].currentPosition - projection).normalized * dist1 + normal * dist2);
                    newPoints.Add(projection + (side[1].currentPosition - projection).normalized * dist1);

                    if (createApts)
                    {
                        //Create extra apartment With special Type
                        Vector3[] aptPoints = new Vector3[4];
                        aptPoints[0] = point0;
                        aptPoints[1] = BrydenWoodUtils.ProjectOnCurve(exterior[0].currentPosition, exterior[1].currentPosition, point0);
                        aptPoints[2] = BrydenWoodUtils.ProjectOnCurve(exterior[0].currentPosition, exterior[1].currentPosition, point1);
                        aptPoints[3] = point1;

                        if (Polygon.IsClockWise(aptPoints.ToList()))
                        {
                            aptPoints = aptPoints.Reverse().ToArray();
                        }
                        var apt = CreateApartment("SpecialApt", aptPoints, i, -1, false, "_Core");
                        apt.isUnresolvable = true;
                        unresolvables.Add(apt);
                    }

                }

                if (i == 0)
                {
                    Vector3 projection = BrydenWoodUtils.ProjectOnCurve(side[0].currentPosition, side[1].currentPosition, coreCentres[i][0]);
                    float dist1 = (coreLength + corridorWidth);
                    actives.Add(new Line() { start = side[0].currentPosition, end = projection + (side[0].currentPosition - projection).normalized * dist1 });
                }
                else if (i == coreCentres.Count)
                {
                    Vector3 projection = BrydenWoodUtils.ProjectOnCurve(side[0].currentPosition, side[1].currentPosition, coreCentres[i - 1][0]);
                    float dist1 = (coreLength + corridorWidth);
                    actives.Add(new Line() { start = projection + (side[1].currentPosition - projection).normalized * dist1, end = side[1].currentPosition });
                }
                else
                {
                    float dist1 = (coreLength + corridorWidth);
                    Vector3 projection = BrydenWoodUtils.ProjectOnCurve(side[0].currentPosition, side[1].currentPosition, coreCentres[i - 1][0]);
                    Vector3 projectionNext = BrydenWoodUtils.ProjectOnCurve(side[0].currentPosition, side[1].currentPosition, coreCentres[i][0]);
                    actives.Add(new Line() { start = projection + (projectionNext - projection).normalized * dist1, end = projectionNext + (projection - projectionNext).normalized * dist1 });
                }
            }
            newPoints.Add(side[1].currentPosition);
            return newPoints;
        }

        private List<Vector3> ModifyCorridorSide(Polygon side, List<Vector3[]> coreCentres, float coreLength, float coreWidth, out List<Line> actives, Polygon exterior)
        {
            List<Vector3> newPoints = new List<Vector3>();
            actives = new List<Line>();
            newPoints.Add(side[0].currentPosition);
            for (int i = 0; i < coreCentres.Count + 1; i++)
            {
                if (i < coreCentres.Count)
                {
                    Vector3 projection = BrydenWoodUtils.ProjectOnCurve(side[0].currentPosition, side[1].currentPosition, coreCentres[i][0]);
                    Vector3 normal = (projection - coreCentres[i][0]).normalized;
                    float dist1 = (coreLength/* + corridorWidth*/);
                    float dist2 = (apartmentDepth + Standards.TaggedObject.ConstructionFeatures["CorridorWall"]);

                    Vector3 point0 = projection + (side[0].currentPosition - projection).normalized * dist1 + normal * dist2;
                    Vector3 point1 = projection + (side[1].currentPosition - projection).normalized * dist1 + normal * dist2;

                    newPoints.Add(projection + (side[0].currentPosition - projection).normalized * dist1);
                    newPoints.Add(newPoints.Last() + normal * dist2);
                    newPoints.Add(projection + (side[1].currentPosition - projection).normalized * dist1 + normal * dist2);
                    newPoints.Add(projection + (side[1].currentPosition - projection).normalized * dist1);

                }

                if (i == 0)
                {
                    Vector3 projection = BrydenWoodUtils.ProjectOnCurve(side[0].currentPosition, side[1].currentPosition, coreCentres[i][0]);
                    float dist1 = (coreLength /*+ corridorWidth*/);
                    actives.Add(new Line() { start = side[0].currentPosition, end = projection + (side[0].currentPosition - projection).normalized * dist1 });
                }
                else if (i == coreCentres.Count)
                {
                    Vector3 projection = BrydenWoodUtils.ProjectOnCurve(side[0].currentPosition, side[1].currentPosition, coreCentres[i - 1][0]);
                    float dist1 = (coreLength /*+ corridorWidth*/);
                    actives.Add(new Line() { start = projection + (side[1].currentPosition - projection).normalized * dist1, end = side[1].currentPosition });
                }
                else
                {
                    float dist1 = (coreLength /*+ corridorWidth*/);
                    Vector3 projection = BrydenWoodUtils.ProjectOnCurve(side[0].currentPosition, side[1].currentPosition, coreCentres[i - 1][0]);
                    Vector3 projectionNext = BrydenWoodUtils.ProjectOnCurve(side[0].currentPosition, side[1].currentPosition, coreCentres[i][0]);
                    actives.Add(new Line() { start = projection + (projectionNext - projection).normalized * dist1, end = projectionNext + (projection - projectionNext).normalized * dist1 });
                }
            }
            newPoints.Add(side[1].currentPosition);
            return newPoints;
        }

        private List<Vector3> SplitCorridorPerCores(Polygon side, List<Vector3[]> coreCentres, out List<Line> actives)
        {
            List<Vector3> newPoints = new List<Vector3>();
            actives = new List<Line>();

            newPoints.Add(side[0].currentPosition);

            for (int i = 0; i < coreCentres.Count; i++)
            {
                float projParam;
                newPoints.Add(side.PointOnCurve(coreCentres[i][0], out projParam));
            }

            newPoints.Add(side.vertices.Last().currentPosition);

            for (int i = 0; i < newPoints.Count - 1; i++)
            {
                actives.Add(new Line(newPoints[i], newPoints[i + 1], Color.white));
            }

            return newPoints;
        }

        private void GetCorridorOffsets()
        {
            if (corridorsPolygons != null)
            {
                for (int i = 0; i < corridorsPolygons.Count; i++)
                {
                    for (int j = 0; j < corridorsPolygons[i].Count; j++)
                    {
                        if (corridorsPolygons[i][j] != null)
                            Destroy(corridorsPolygons[i][j].gameObject);
                    }
                }
            }

            corridorsPolygons = new List<List<Polygon>>();

            var firstSidePoints = centreLine.OffsetAsLineSegments(corridorWidth / 2.0f);
            List<Polygon> firstSide = new List<Polygon>();

            for (int i = 0; i < firstSidePoints.Length; i += 2)
            {
                Polygon m_polygon = Instantiate(polygonPrefab, transform).GetComponent<Polygon>();
                m_polygon.gameObject.name = "first_" + i;
                PolygonVertex startPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                startPoint.Initialize(i);
                startPoint.UpdatePosition(firstSidePoints[i]);

                PolygonVertex endPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                endPoint.Initialize(i + 1);
                endPoint.UpdatePosition(firstSidePoints[i + 1]);
                m_polygon.Initialize(new List<PolygonVertex>() { startPoint, endPoint }, false);

                firstSide.Add(m_polygon);
            }

            var secondSidePoints = centreLine.OffsetAsLineSegments(-corridorWidth / 2.0f);
            List<Polygon> secondSide = new List<Polygon>();

            for (int i = 0; i < secondSidePoints.Length; i += 2)
            {
                Polygon m_polygon = Instantiate(polygonPrefab, transform).GetComponent<Polygon>();
                m_polygon.gameObject.name = "second_" + i;
                PolygonVertex startPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                startPoint.Initialize(i);
                startPoint.UpdatePosition(secondSidePoints[i]);

                PolygonVertex endPoint = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                endPoint.Initialize(i + 1);
                endPoint.UpdatePosition(secondSidePoints[i + 1]);

                m_polygon.Initialize(new List<PolygonVertex>() { startPoint, endPoint }, false);

                secondSide.Add(m_polygon);
            }

            corridorsPolygons.Add(firstSide);
            corridorsPolygons.Add(secondSide);

            CheckIntersections(corridorsPolygons);
        }

        private List<List<KeyValuePair<string, Vector3[]>>> GetApartments(Dictionary<string, float> widths, Dictionary<string, int> aptNumbers, List<Line>[] activeCors, List<Polygon>[] tempOffsets, ref List<bool> hasCorridors)
        {
            Dictionary<string, int> aptNumbersPlaced = new Dictionary<string, int>();

            foreach (var item in aptNumbers)
            {
                aptNumbersPlaced.Add(item.Key, 0);
            }

            List<List<KeyValuePair<string, Vector3[]>>> vertices = new List<List<KeyValuePair<string, Vector3[]>>>();

            string currentAptType = aptNumbers.Keys.ToList()[0];


            if (typology == LinearTypology.Single)
            {
                for (int j = 0; j < activeCors[0].Count; j++)
                {
                    ApartmentsFromCorridor(vertices, tempOffsets[0][j], activeCors[0][j], widths, aptNumbersPlaced, aptNumbers, ref currentAptType);
                    hasCorridors.Add(true);
                }
                for (int j = 0; j < activeCors[1].Count; j++)
                {
                    ApartmentsFromCorridor(vertices, tempOffsets[1][j], activeCors[1][j].Reverse(), widths, aptNumbersPlaced, aptNumbers, ref currentAptType);
                    hasCorridors.Add(false);
                }
            }
            else if (typology == LinearTypology.DeckAccess)
            {
                if (coreAllignment == CoreAllignment.Left)
                {
                    for (int j = 0; j < activeCors[0].Count; j++)
                    {
                        ApartmentsFromCorridor(vertices, tempOffsets[0][j], activeCors[0][j], widths, aptNumbersPlaced, aptNumbers, ref currentAptType);
                        hasCorridors.Add(true);
                    }
                }
                else if (coreAllignment == CoreAllignment.Right)
                {
                    for (int j = 0; j < activeCors[1].Count; j++)
                    {
                        ApartmentsFromCorridor(vertices, tempOffsets[1][j], activeCors[1][j].Reverse(), widths, aptNumbersPlaced, aptNumbers, ref currentAptType);
                        hasCorridors.Add(true);
                    }
                }
            }


            return vertices;
        }

        private Dictionary<string, int> EvaluateAptsNumbers(Dictionary<string, float> widths, Dictionary<string, int> aptNumbers, List<Line>[] activeCors, List<Polygon>[] tempOffsets)
        {
            Dictionary<string, int> aptNumbersPlaced = new Dictionary<string, int>();

            foreach (var item in aptNumbers)
            {
                aptNumbersPlaced.Add(item.Key, 0);
            }
            //aptNumbersPlaced.Add("SpecialApt", 0);

            List<List<KeyValuePair<string, Vector3[]>>> vertices = new List<List<KeyValuePair<string, Vector3[]>>>();

            string currentAptType = aptNumbers.Keys.ToList()[0];


            if (typology == LinearTypology.Single)
            {
                for (int j = 0; j < activeCors[0].Count; j++)
                {
                    AptNumsFromCorridor(tempOffsets[0][j], activeCors[0][j], widths, aptNumbersPlaced, aptNumbers, ref currentAptType);
                }
                for (int j = 0; j < activeCors[1].Count; j++)
                {
                    AptNumsFromCorridor(tempOffsets[1][j], activeCors[1][j].Reverse(), widths, aptNumbersPlaced, aptNumbers, ref currentAptType);
                }
            }
            else if (typology == LinearTypology.DeckAccess)
            {
                if (coreAllignment == CoreAllignment.Left)
                {
                    for (int j = 0; j < activeCors[0].Count; j++)
                    {
                        AptNumsFromCorridor(tempOffsets[0][j], activeCors[0][j], widths, aptNumbersPlaced, aptNumbers, ref currentAptType);
                    }
                }
                else if (coreAllignment == CoreAllignment.Right)
                {
                    for (int j = 0; j < activeCors[1].Count; j++)
                    {
                        AptNumsFromCorridor(tempOffsets[1][j], activeCors[1][j].Reverse(), widths, aptNumbersPlaced, aptNumbers, ref currentAptType);
                    }
                }
            }

            return aptNumbersPlaced;
        }

        private void ApartmentsFromCorridor(List<List<KeyValuePair<string, Vector3[]>>> vertices, Polygon m_offset1, Line m_corridor1, Dictionary<string, float> widths, Dictionary<string, int> aptsPlaced, Dictionary<string, int> aptsNums, ref string currentType)
        {
            float minWidth = widths[currentType];
            Vector3 origin = m_corridor1.start;
            float length = Vector3.Distance(origin, m_corridor1.end);
            int counter = 0;
            List<KeyValuePair<string, Vector3[]>> vrts = new List<KeyValuePair<string, Vector3[]>>();
            length = (float)Math.Round(length, 2);
            minWidth = (float)Math.Round(minWidth, 2);

            while (length >= minWidth)
            {
                if (length < minWidth * 0.05f && length > 0)
                {
                    minWidth = length;
                }
                Vector3[] aptVerts = new Vector3[4];
                aptVerts[0] = origin;
                aptVerts[1] = BrydenWoodUtils.ProjectOnCurve(m_offset1[0].currentPosition, m_offset1[1].currentPosition, aptVerts[0]);
                aptVerts[2] = aptVerts[1] + (m_corridor1.end - m_corridor1.start).normalized * minWidth;
                aptVerts[3] = origin + (m_corridor1.end - m_corridor1.start).normalized * minWidth;
                if (Polygon.IsClockWise(aptVerts.ToList()))
                {
                    aptVerts = aptVerts.Reverse().ToArray();
                }
                vrts.Add(new KeyValuePair<string, Vector3[]>(currentType, aptVerts));

                origin += (m_corridor1.end - m_corridor1.start).normalized * minWidth;
                length = (float)Math.Round(Vector3.Distance(origin, m_corridor1.end), 2);



                if (length < minWidth && length > 0)
                {
                    {
                        aptVerts = new Vector3[4];
                        aptVerts[0] = origin;
                        aptVerts[1] = BrydenWoodUtils.ProjectOnCurve(m_offset1[0].currentPosition, m_offset1[1].currentPosition, aptVerts[0]);
                        aptVerts[2] = aptVerts[1] + (m_corridor1.end - m_corridor1.start).normalized * length;
                        aptVerts[3] = origin + (m_corridor1.end - m_corridor1.start).normalized * length;
                        if (Polygon.IsClockWise(aptVerts.ToList()))
                        {
                            aptVerts = aptVerts.Reverse().ToArray();
                        }
                        string newType = "SpecialApt";
                        foreach (var item in widths)
                        {
                            if (length > item.Value)
                            {
                                newType = item.Key;
                            }
                        }
                        vrts.Add(new KeyValuePair<string, Vector3[]>(newType, aptVerts));
                        length = 0;
                        if (newType != "SpecialApt")
                        {
                            aptsPlaced[newType]++;
                        }
                    }
                }

                aptsPlaced[currentType]++;

                if (aptsPlaced[currentType] >= aptsNums[currentType])
                {
                    var types = aptsPlaced.Keys.ToList();
                    var nextIndex = types.IndexOf(currentType) + 1;
                    if (nextIndex < types.Count)
                    {
                        currentType = types[nextIndex];
                        minWidth = widths[currentType];
                    }
                }
                counter++;
            }

            if (counter == 0 || (length < minWidth && length > 0))
            {
                Vector3[] aptVerts = new Vector3[4];
                aptVerts = new Vector3[4];
                aptVerts[0] = origin;
                aptVerts[1] = BrydenWoodUtils.ProjectOnCurve(m_offset1[0].currentPosition, m_offset1[1].currentPosition, aptVerts[0]);
                aptVerts[2] = aptVerts[1] + (m_corridor1.end - m_corridor1.start).normalized * length;
                aptVerts[3] = origin + (m_corridor1.end - m_corridor1.start).normalized * length;
                if (Polygon.IsClockWise(aptVerts.ToList()))
                {
                    aptVerts = aptVerts.Reverse().ToArray();
                }
                string newType = "SpecialApt";
                foreach (var item in widths)
                {
                    if (length > item.Value)
                    {
                        newType = item.Key;
                    }
                }
                vrts.Add(new KeyValuePair<string, Vector3[]>(newType, aptVerts));
                length = 0;
                if (newType != "SpecialApt")
                {
                    aptsPlaced[newType]++;
                }
            }

            vertices.Add(vrts);
        }

        private void AptNumsFromCorridor(Polygon m_offset1, Line m_corridor1, Dictionary<string, float> widths, Dictionary<string, int> aptsPlaced, Dictionary<string, int> aptsNums, ref string currentType)
        {
            float minWidth = widths[currentType];
            Vector3 origin = m_corridor1.start;
            float length = Vector3.Distance(origin, m_corridor1.end);
            int counter = 0;
            List<KeyValuePair<string, Vector3[]>> vrts = new List<KeyValuePair<string, Vector3[]>>();
            length = (float)Math.Round(length, 2);
            minWidth = (float)Math.Round(minWidth, 2);

            while (length >= minWidth)
            {
                if (length < minWidth * 0.05f && length > 0)
                {
                    minWidth = length;
                }
                Vector3[] aptVerts = new Vector3[4];
                aptVerts[0] = origin;
                aptVerts[1] = BrydenWoodUtils.ProjectOnCurve(m_offset1[0].currentPosition, m_offset1[1].currentPosition, aptVerts[0]);
                aptVerts[2] = aptVerts[1] + (m_corridor1.end - m_corridor1.start).normalized * minWidth;
                aptVerts[3] = origin + (m_corridor1.end - m_corridor1.start).normalized * minWidth;
                if (Polygon.IsClockWise(aptVerts.ToList()))
                {
                    aptVerts = aptVerts.Reverse().ToArray();
                }
                vrts.Add(new KeyValuePair<string, Vector3[]>(currentType, aptVerts));

                origin += (m_corridor1.end - m_corridor1.start).normalized * minWidth;
                length = (float)Math.Round(Vector3.Distance(origin, m_corridor1.end), 2);

                aptsPlaced[currentType]++;


                if (length < minWidth && length > 0)
                {
                    {
                        aptVerts = new Vector3[4];
                        aptVerts[0] = origin;
                        aptVerts[1] = BrydenWoodUtils.ProjectOnCurve(m_offset1[0].currentPosition, m_offset1[1].currentPosition, aptVerts[0]);
                        aptVerts[2] = aptVerts[1] + (m_corridor1.end - m_corridor1.start).normalized * length;
                        aptVerts[3] = origin + (m_corridor1.end - m_corridor1.start).normalized * length;
                        if (Polygon.IsClockWise(aptVerts.ToList()))
                        {
                            aptVerts = aptVerts.Reverse().ToArray();
                        }
                        string newType = "SpecialApt";
                        foreach (var item in widths)
                        {
                            if (length > item.Value)
                            {
                                newType = item.Key;
                            }
                        }
                        vrts.Add(new KeyValuePair<string, Vector3[]>(newType, aptVerts));
                        length = 0;
                        if (newType != "SpecialApt"/*aptsPlaced.ContainsKey(newType)*/)
                        {
                            aptsPlaced[newType]++;
                        }
                        //else
                        //{
                        //    aptsPlaced.Add(newType, 1);
                        //}
                    }
                }

                if (aptsPlaced[currentType] >= aptsNums[currentType])
                {
                    var types = aptsPlaced.Keys.ToList();
                    var nextIndex = types.IndexOf(currentType) + 1;
                    if (nextIndex < types.Count)
                    {
                        currentType = types[nextIndex];
                        minWidth = widths[currentType];
                    }
                }
                counter++;
            }

            if (counter == 0 || (length < minWidth && length > 0))
            {
                Vector3[] aptVerts = new Vector3[4];
                aptVerts = new Vector3[4];
                aptVerts[0] = origin;
                aptVerts[1] = BrydenWoodUtils.ProjectOnCurve(m_offset1[0].currentPosition, m_offset1[1].currentPosition, aptVerts[0]);
                aptVerts[2] = aptVerts[1] + (m_corridor1.end - m_corridor1.start).normalized * length;
                aptVerts[3] = origin + (m_corridor1.end - m_corridor1.start).normalized * length;
                if (Polygon.IsClockWise(aptVerts.ToList()))
                {
                    aptVerts = aptVerts.Reverse().ToArray();
                }
                string newType = "SpecialApt";
                foreach (var item in widths)
                {
                    if (length > item.Value)
                    {
                        newType = item.Key;
                    }
                }
                vrts.Add(new KeyValuePair<string, Vector3[]>(newType, aptVerts));
                length = 0;
                if (newType != "SpecialApt"/*aptsPlaced.ContainsKey(newType)*/)
                {
                    aptsPlaced[newType]++;
                }
                //else
                //{
                //    aptsPlaced.Add(newType, 1);
                //}
            }
        }

        private void GetCornerPoints()
        {
            //For the outter corners
            for (int i = 0; i < offsets.Count; i++)
            {
                for (int j = 0; j < offsets[i].Count; j++)
                {

                    var line = new Vector3[] { centreLine[j].currentPosition, centreLine[j + 1].currentPosition };
                    Vector3 proj;

                    if (!ProjectedInside(offsets[i][j][0].currentPosition, line, out proj))
                    {
                        var point2Project = line[FindClosestPoint(line, offsets[i][j][0].currentPosition)];

                        Vector3 projection = BrydenWoodUtils.ProjectOnCurve(offsets[i][j][0].currentPosition, offsets[i][j][1].currentPosition, point2Project);
                    }

                    if (!ProjectedInside(offsets[i][j][1].currentPosition, line, out proj))
                    {
                        var point2Project = line[FindClosestPoint(line, offsets[i][j][1].currentPosition)];

                        Vector3 projection = BrydenWoodUtils.ProjectOnCurve(offsets[i][j][0].currentPosition, offsets[i][j][1].currentPosition, point2Project);
                    }
                }
            }

            //For the inner corners
            for (int i = 0; i < offsets.Count; i++)
            {
                for (int j = 0; j < offsets[i].Count; j++)
                {

                    var line = new Vector3[] { offsets[i][j][0].currentPosition, offsets[i][j][1].currentPosition };
                    Vector3 proj;

                    if (!ProjectedInside(centreLine[j].currentPosition, line, out proj))
                    {
                        var point2Project = line[FindClosestPoint(line, centreLine[j].currentPosition)];

                        Vector3 projection = BrydenWoodUtils.ProjectOnCurve(centreLine[j].currentPosition, centreLine[j + 1].currentPosition, point2Project) + (centreLine[j + 1].currentPosition - centreLine[j].currentPosition).normalized * (corridorWidth + coreLength * 0.5f);

                        float par;
                        var vec = centreLine.PointOnCurve(projection, out par);
                        cornersParams.Add(par);
                    }

                    if (!ProjectedInside(centreLine[j + 1].currentPosition, line, out proj))
                    {
                        var point2Project = line[FindClosestPoint(line, centreLine[j + 1].currentPosition)];

                        Vector3 projection = BrydenWoodUtils.ProjectOnCurve(centreLine[j].currentPosition, centreLine[j + 1].currentPosition, point2Project) + (centreLine[j].currentPosition - centreLine[j + 1].currentPosition).normalized * (corridorWidth + coreLength * 0.5f);

                        float par;
                        var vec = centreLine.PointOnCurve(projection, out par);
                        cornersParams.Add(par);
                    }
                }
            }
        }

        private void GetOutterCorners()
        {
            for (int i = 0; i < offsets.Count; i++)
            {
                for (int j = 0; j < offsets[i].Count; j++)
                {
                    var line = new Vector3[] { corridorsPolygons[i][j][0].currentPosition, corridorsPolygons[i][j][1].currentPosition };
                    Vector3 proj;

                    if (!ProjectedInside(offsets[i][j][0].currentPosition, line, out proj))
                    {
                        var point2Project = line[FindClosestPoint(line, offsets[i][j][0].currentPosition)];

                        Vector3 projection = BrydenWoodUtils.ProjectOnCurve(offsets[i][j][0].currentPosition, offsets[i][j][1].currentPosition, point2Project);

                        var corners = new Vector3[3] { projection, point2Project, offsets[i][j][0].currentPosition };

                        if (Polygon.IsClockWise(corners.ToList()))
                        {
                            corners = corners.Reverse().ToArray();
                        }
                        cornerAptPoints.Add(corners);
                        isExteriorCorner.Add(true);
                    }

                    if (!ProjectedInside(offsets[i][j][1].currentPosition, line, out proj))
                    {
                        var point2Project = line[FindClosestPoint(line, offsets[i][j][1].currentPosition)];

                        Vector3 projection = BrydenWoodUtils.ProjectOnCurve(offsets[i][j][0].currentPosition, offsets[i][j][1].currentPosition, point2Project);

                        var corners = new Vector3[3] { projection, offsets[i][j][1].currentPosition, point2Project };

                        if (Polygon.IsClockWise(corners.ToList()))
                        {
                            corners = corners.Reverse().ToArray();
                        }
                        cornerAptPoints.Add(corners);
                        isExteriorCorner.Add(true);
                    }
                }
            }
        }

        private void GetActiveCorridors(bool createApts = true)
        {
            floorActiveCorridorLength = 0;

            for (int i = 0; i < activeCorridorLengths.Length; i++)
            {
                if (activeCorridorLengths[i] != null)
                {
                    for (int j = 0; j < activeCorridorLengths[i].Count; j++)
                    {
                        for (int k = 0; k < activeCorridorLengths[i][j].Count; k++)
                        {
                            Vector3 proj;
                            Vector3[] line = new Vector3[] { offsets[i][j][0].currentPosition, offsets[i][j][1].currentPosition };

                            if (!ProjectedInside(activeCorridorLengths[i][j][k].start, line, out proj))
                            {
                                var point2Project = line[FindClosestPoint(line, activeCorridorLengths[i][j][k].start)];

                                Vector3 projection = BrydenWoodUtils.ProjectOnCurve(activeCorridorLengths[i][j][k].start, activeCorridorLengths[i][j][k].end, point2Project);

                                var corners = new Vector3[3] { projection, point2Project, activeCorridorLengths[i][j][k].start };

                                if (Polygon.IsClockWise(corners.ToList()))
                                {
                                    corners = corners.Reverse().ToArray();
                                }
                                if (createApts)
                                {
                                    cornerAptPoints.Add(corners);
                                    isExteriorCorner.Add(false);
                                }
                                activeCorridorLengths[i][j][k] = new Line() { start = projection, end = activeCorridorLengths[i][j][k].end };
                            }

                            if (!ProjectedInside(activeCorridorLengths[i][j][k].end, line, out proj))
                            {
                                var point2Project = line[FindClosestPoint(line, activeCorridorLengths[i][j][k].end)];

                                Vector3 projection = BrydenWoodUtils.ProjectOnCurve(activeCorridorLengths[i][j][k].start, activeCorridorLengths[i][j][k].end, point2Project);

                                var corners = new Vector3[3] { projection, point2Project, activeCorridorLengths[i][j][k].end };

                                if (Polygon.IsClockWise(corners.ToList()))
                                {
                                    corners = corners.Reverse().ToArray();
                                }
                                if (createApts)
                                {
                                    cornerAptPoints.Add(corners);
                                    isExteriorCorner.Add(false);
                                }
                                activeCorridorLengths[i][j][k] = new Line() { start = activeCorridorLengths[i][j][k].start, end = projection };
                            }


                            floorActiveCorridorLength += Vector3.Distance(activeCorridorLengths[i][j][k].start, activeCorridorLengths[i][j][k].end);

                        }
                    }
                }
            }
        }
        private List<Line>[] ReorderActiveCorridorsEnds2CentreOpen(out List<Polygon>[] orderedOffsets)
        {

            List<Line>[] orderedCorridors = new List<Line>[2];
            orderedOffsets = new List<Polygon>[2];

            List<List<Line>> temp_cors = new List<List<Line>>();
            List<List<Polygon>> tempOffsets = new List<List<Polygon>>();

            temp_cors.Add(new List<Line>());
            temp_cors.Add(new List<Line>());
            tempOffsets.Add(new List<Polygon>());
            tempOffsets.Add(new List<Polygon>());

            for (int k = 0; k < activeCorridorLengths.Length; k++)
            {
                if (activeCorridorLengths[k] != null)
                {
                    for (int i = 0; i < activeCorridorLengths[k].Count; i++)
                    {
                        for (int j = 0; j < activeCorridorLengths[k][i].Count; j++)
                        {
                            temp_cors[k].Add(activeCorridorLengths[k][i][j]);
                            tempOffsets[k].Add(offsets[k][i]);
                        }
                    }
                }
            }
            //for (int i = 0; i < activeCorridorLengths[1].Count; i++)
            //{
            //    for (int j = 0; j < activeCorridorLengths[1][i].Count; j++)
            //    {
            //        temp_cors[1].Add(activeCorridorLengths[1][i][j]);
            //        tempOffsets[1].Add(offsets[1][i]);
            //    }
            //}

            List<int> m_inds = new List<int>();
            if (temp_cors[0] != null && temp_cors[0].Count > 0)
            {
                for (int i = 0; i < temp_cors[0].Count; i++)
                {
                    if (!m_inds.Contains(i))
                    {
                        m_inds.Add(i);
                    }
                    if (!m_inds.Contains(temp_cors[0].Count - 1 - i))
                    {
                        m_inds.Add(temp_cors[0].Count - 1 - i);
                    }
                }
                orderedCorridors[0] = (m_inds.Select(x => temp_cors[0][x]).ToList());
                orderedOffsets[0] = (m_inds.Select(x => tempOffsets[0][x]).ToList());
            }

            if (temp_cors[1] != null && temp_cors[1].Count > 0)
            {
                for (int i = 0; i < temp_cors[1].Count; i++)
                {
                    if (!m_inds.Contains(i))
                    {
                        m_inds.Add(i);
                    }
                    if (!m_inds.Contains(temp_cors[1].Count - 1 - i))
                    {
                        m_inds.Add(temp_cors[1].Count - 1 - i);
                    }
                }
                orderedCorridors[1] = (m_inds.Select(x => temp_cors[1][x]).ToList());
                orderedOffsets[1] = (m_inds.Select(x => tempOffsets[1][x]).ToList());
            }

            return orderedCorridors;
        }

        private List<List<Line>> ReorderActiveCorridorsEnds2CentreClosed(out List<List<Polygon>> orderedOffsets)
        {
            orderedOffsets = new List<List<Polygon>>();
            List<List<Line>> orderedCorridors = new List<List<Line>>();
            orderedOffsets.Add(new List<Polygon>());
            orderedOffsets.Add(new List<Polygon>());
            orderedCorridors.Add(new List<Line>());
            orderedCorridors.Add(new List<Line>());

            List<int>[] split = new List<int>[2] { new List<int>(), new List<int>() };

            for (int i = 0; i < activeCorridorLengths[0].Count; i++)
            {
                if (i < activeCorridorLengths[0].Count / 2.0f)
                {
                    split[0].Add(i);
                }
                else
                {
                    split[1].Add(i);
                }
            }

            List<int>[] temp_index = new List<int>[2] { new List<int>(), new List<int>() };
            int splitIndex = -1;

            for (int i = 0; i < split.Length; i++)
            {
                List<List<Line>> temp_cors = new List<List<Line>>();
                List<List<Polygon>> tempOffsets = new List<List<Polygon>>();

                temp_cors.Add(new List<Line>());
                temp_cors.Add(new List<Line>());
                tempOffsets.Add(new List<Polygon>());
                tempOffsets.Add(new List<Polygon>());

                for (int j = 0; j < split[i].Count; j++)
                {
                    for (int k = 0; k < activeCorridorLengths[0][split[i][j]].Count; k++)
                    {
                        temp_cors[0].Add(activeCorridorLengths[0][split[i][j]][k]);
                        temp_cors[1].Add(activeCorridorLengths[1][split[i][j]][k]);
                        tempOffsets[0].Add(offsets[0][split[i][j]]);
                        tempOffsets[1].Add(offsets[1][split[i][j]]);
                    }
                }

                List<int> m_inds = new List<int>();

                for (int j = 0; j < temp_cors[0].Count; j++)
                {
                    if (!m_inds.Contains(j))
                    {
                        m_inds.Add(j);
                    }
                    if (!m_inds.Contains(temp_cors[0].Count - 1 - j))
                    {
                        m_inds.Add(temp_cors[0].Count - 1 - j);
                    }
                }

                orderedCorridors[0].AddRange(m_inds.Select(x => temp_cors[0][x]).ToList());
                orderedCorridors[1].AddRange(m_inds.Select(x => temp_cors[1][x]).ToList());
                orderedOffsets[0].AddRange(m_inds.Select(x => tempOffsets[0][x]).ToList());
                orderedOffsets[1].AddRange(m_inds.Select(x => tempOffsets[1][x]).ToList());
                if (splitIndex == -1)
                {
                    splitIndex = orderedCorridors[0].Count;
                }
            }

            List<Line>[] newCors = new List<Line>[2] { new List<Line>(), new List<Line>() };
            List<Polygon>[] newOffs = new List<Polygon>[2] { new List<Polygon>(), new List<Polygon>() };

            for (int i = 0; i < orderedCorridors[0].Count && orderedCorridors[0].Count >= newCors[0].Count; i++)
            {
                if (i < splitIndex && !newCors[0].Contains(orderedCorridors[0][i]))
                {
                    newCors[0].Add(orderedCorridors[0][i]);
                    newCors[1].Add(orderedCorridors[1][i]);
                    newOffs[0].Add(orderedOffsets[0][i]);
                    newOffs[1].Add(orderedOffsets[1][i]);
                }

                if ((i + splitIndex) < orderedCorridors[0].Count && !newCors[0].Contains(orderedCorridors[0][i + splitIndex]))
                {
                    newCors[0].Add(orderedCorridors[0][i + splitIndex]);
                    newCors[1].Add(orderedCorridors[1][i + splitIndex]);
                    newOffs[0].Add(orderedOffsets[0][i + splitIndex]);
                    newOffs[1].Add(orderedOffsets[1][i + splitIndex]);
                }
            }

            orderedOffsets = newOffs.ToList();

            return newCors.ToList();
        }

        private void GetCorridors()
        {
            GetCorridorOffsets();
            GetOutterCorners();

            List<List<Line>> activeSide1 = new List<List<Line>>();
            List<List<Line>> activeSide2 = new List<List<Line>>();

            for (int i = 0; i < cores.Count; i++)
            {
                if (cores[i] != null && cores[i].Count > 0)
                {
                    List<Line> s1;
                    List<Line> s2;
                    List<Vector3> side1 = new List<Vector3>();
                    List<Vector3> side2 = new List<Vector3>();
                    switch (typology)
                    {
                        case LinearTypology.Single:
                            switch (coreAllignment)
                            {
                                case CoreAllignment.Centre:
                                    side1 = ModifyCorridorCentre(corridorsPolygons[0][i], cores[i], coreLength / 2.0f, coreWidth / 2.0f, out s1, offsets[0][i]);
                                    activeSide1.Add(s1);
                                    side2 = ModifyCorridorCentre(corridorsPolygons[1][i], cores[i], coreLength / 2.0f, coreWidth / 2.0f, out s2, offsets[1][i]);
                                    activeSide2.Add(s2);
                                    break;
                                case CoreAllignment.Left:
                                    side1 = ModifyCorridorSide(corridorsPolygons[0][i], cores[i], coreLength / 2.0f, coreWidth / 2.0f, out s1, offsets[0][i]);
                                    activeSide1.Add(s1);
                                    side2 = SplitCorridorPerCores(corridorsPolygons[1][i], cores[i], out s2);
                                    activeSide2.Add(s2);
                                    break;
                                case CoreAllignment.Right:
                                    side1 = SplitCorridorPerCores(corridorsPolygons[0][i], cores[i], out s1);
                                    activeSide1.Add(s1);
                                    side2 = ModifyCorridorSide(corridorsPolygons[1][i], cores[i], coreLength / 2.0f, coreWidth / 2.0f, out s2, offsets[1][i]);
                                    activeSide2.Add(s2);
                                    break;
                            }
                            break;
                        case LinearTypology.DeckAccess:
                            if (coreAllignment == CoreAllignment.Left)
                            {
                                side1 = ModifyCorridorSide(corridorsPolygons[0][i], cores[i], coreLength / 2.0f, coreWidth / 2.0f, out s1, offsets[0][i]);
                                activeSide1.Add(s1);
                                side2 = SplitCorridorPerCores(corridorsPolygons[1][i], cores[i], out s2);
                                activeSide2.Add(s2);
                            }
                            else if (coreAllignment == CoreAllignment.Right)
                            {
                                side1 = SplitCorridorPerCores(corridorsPolygons[0][i], cores[i], out s1);
                                activeSide1.Add(s1);
                                side2 = ModifyCorridorSide(corridorsPolygons[1][i], cores[i], coreLength / 2.0f, coreWidth / 2.0f, out s2, offsets[0][i]);
                                activeSide2.Add(s2);
                            }
                            break;
                    }

                    side2.Reverse();
                    side1.AddRange(side2);

                    //side1 = BrydenWoodUtils.RemoveCollinearPoints(side1);

                    Polygon m_polygon = Instantiate(polygonPrefab, corridors.transform).GetComponent<Polygon>();
                    m_polygon.gameObject.name = "Corridor" + i;
                    List<PolygonVertex> vertices = new List<PolygonVertex>();
                    for (int j = 0; j < side1.Count; j++)
                    {
                        PolygonVertex vertex = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                        vertex.gameObject.name = "Corridor" + i + "_vertex" + j;
                        vertex.Initialize(j);
                        vertex.UpdatePosition(side1[j]);
                        vertices.Add(vertex);
                    }

                    m_polygon.Initialize(vertices, true, true);
                }
                else
                {
                    List<Line> s1 = new List<Line>() { new Line() { start = corridorsPolygons[0][i][0].currentPosition, end = corridorsPolygons[0][i][1].currentPosition } };
                    activeSide1.Add(s1);
                    List<Line> s2 = new List<Line>() { new Line() { start = corridorsPolygons[1][i][0].currentPosition, end = corridorsPolygons[1][i][1].currentPosition } };
                    activeSide2.Add(s2);
                    List<Vector3> side1 = new List<Vector3>() { s1[0].start, s1[0].end, s2[0].end, s2[0].start };

                    Polygon m_polygon = Instantiate(polygonPrefab, corridors.transform).GetComponent<Polygon>();
                    m_polygon.gameObject.name = "Corridor" + i;
                    List<PolygonVertex> vertices = new List<PolygonVertex>();
                    for (int j = 0; j < side1.Count; j++)
                    {
                        PolygonVertex vertex = Instantiate(polygonVertexPrefab, m_polygon.transform).GetComponent<PolygonVertex>();
                        vertex.gameObject.name = "Corridor" + i + "_vertex" + j;
                        vertex.Initialize(j);
                        vertex.UpdatePosition(side1[j]);
                        vertices.Add(vertex);
                    }

                    m_polygon.Initialize(vertices, true, true);

                }
            }
            if (typology == LinearTypology.Single)
            {
                switch (coreAllignment)
                {
                    case CoreAllignment.Centre:
                        activeCorridorLengths[0] = activeSide1;
                        activeCorridorLengths[1] = (activeSide2);
                        GetActiveCorridors();
                        break;
                    case CoreAllignment.Left:
                        activeCorridorLengths[0] = (activeSide1);
                        activeCorridorLengths[1] = (activeSide2);
                        GetActiveCorridors();
                        break;
                    case CoreAllignment.Right:
                        activeCorridorLengths[0] = (activeSide1);
                        activeCorridorLengths[1] = (activeSide2);
                        GetActiveCorridors();
                        break;
                }
            }
            else if (typology == LinearTypology.DeckAccess)
            {
                if (coreAllignment == CoreAllignment.Left)
                {
                    activeCorridorLengths[0] = (activeSide1);
                    //activeCorridorLengths.Add(activeSide2);
                    GetActiveCorridors();
                }
                else if (coreAllignment == CoreAllignment.Right)
                {
                    //activeCorridorLengths.Add(activeSide1);
                    activeCorridorLengths[1] = (activeSide2);
                    GetActiveCorridors();
                }
            }
        }

        /// <summary>
        /// Loads apartments from the save state
        /// </summary>
        /// <param name="loadState">The save state of the floor layout</param>
        protected virtual void LoadApartments(FloorLayoutState loadState)
        {
            List<ApartmentUnity> apts = new List<ApartmentUnity>();
            for (int i = 0; i < loadState.apartmentStates.Count; i++)
            {
                var aptState = loadState.apartmentStates[i];
                List<Vector3> temp_points = new List<Vector3>();
                for (int j = 0; j < aptState.vertices.Length; j += 3)
                {
                    temp_points.Add(new Vector3(aptState.vertices[j], aptState.vertices[j + 1], aptState.vertices[j + 2]));
                }

                if (aptState.isCorner)
                {
                    CreateCornerApartment("SpecialApt", temp_points.ToArray(), aptState.id, aptState.isExterior);
                }
                else
                {
                    apts.Add(CreateApartment(aptState.type, temp_points.ToArray(), aptState.id, aptState.corridorIndex, aptState.hasCorridor, "", aptState.triangles, aptState.flipped));
                }
            }

            for (int i = 0; i < apts.Count; i++)
            {
                if (apts[i].ApartmentType == "SpecialApt" && !apts[i].IsCorner)
                {
                    apts[i].SetCloseOnes(apts);
                }
                apts[i].CheckNeighbours();
            }

            for (int i = 0; i < floorVertices.Count; i++)
            {
                floorVertices[i].SetGroup(floorVertices);
            }
        }

        private void GetApartments()
        {
            List<Polygon>[] temp_Offsets = new List<Polygon>[2];
            List<Line>[] tempactive = new List<Line>[2];
            if (centreLine.closed)
            {
                //tempactive = ReorderActiveCorridorsEnds2CentreClosed(out temp_Offsets);
            }
            else
            {
                tempactive = ReorderActiveCorridorsEnds2CentreOpen(out temp_Offsets);
            }


            var temp_percentages = percentages.OrderBy(x => Standards.TaggedObject.DesiredAreas[x.Key]).ToList();
            temp_percentages.Reverse();
            Dictionary<string, float> widths = new Dictionary<string, float>();
            Dictionary<string, float> availableLengths = new Dictionary<string, float>();
            Dictionary<string, int> aptNumbers = new Dictionary<string, int>();
            foreach (var item in temp_percentages)
            {
                double aptEnvelopeWidth = (Standards.TaggedObject.DesiredAreas[item.Key] / apartmentDepth) + Standards.TaggedObject.ConstructionFeatures["PartyWall"];
                widths.Add(item.Key, (float)(aptEnvelopeWidth));
                availableLengths.Add(item.Key, floorActiveCorridorLength * (item.Value / 100.0f));
                aptNumbers.Add(item.Key, Mathf.FloorToInt(availableLengths[item.Key] / widths[item.Key]));
            }

            List<bool> hasCorridors = new List<bool>();
            List<List<KeyValuePair<string, Vector3[]>>> verts = GetApartments(widths, aptNumbers, tempactive, temp_Offsets, ref hasCorridors);

            int counter = 0;
            for (int i = 0; i < verts.Count; i++)
            {
                List<ApartmentUnity> apts = new List<ApartmentUnity>();
                for (int j = 0; j < verts[i].Count; j++)
                {
                    apts.Add(CreateApartment(verts[i][j].Key, verts[i][j].Value, counter, i, hasCorridors[i]));
                    counter++;
                }

                for (int j = 0; j < apts.Count; j++)
                {
                    if (apts[j].ApartmentType == "SpecialApt")
                    {
                        apts[j].SetCloseOnes(apts);
                    }
                }
            }

            for (int i = 0; i < cornerAptPoints.Count; i++)
            {
                CreateCornerApartment("SpecialApt", cornerAptPoints[i], i, isExteriorCorner[i]);
            }

            for (int i = 0; i < floorVertices.Count; i++)
            {
                floorVertices[i].SetGroup(floorVertices);
            }

            for (int i = 0; i < unresolvables.Count; i++)
            {
                Vector3 centre = unresolvables[i].editableMesh.meshExtrusion.GetComponent<MeshRenderer>().bounds.center;
                Ray r1 = new Ray(centre, unresolvables[i].transform.right);
                Ray r2 = new Ray(centre, -unresolvables[i].transform.right);
                RaycastHit hit;
                if (Physics.Raycast(r1, out hit, unresolvables[i].Width * 1.5f))
                {
                    unresolvables[i].resRight = hit.collider.transform.parent.parent.gameObject;
                    hit.collider.transform.parent.parent.GetComponent<ApartmentUnity>().unresolvableOnLeft = true;
                    hit.collider.transform.parent.parent.GetComponent<ApartmentUnity>().editableMesh.constrainUnresolvedL = true;
                }
                if (Physics.Raycast(r2, out hit, unresolvables[i].Width * 1.5f))
                {
                    unresolvables[i].resLeft = hit.collider.transform.parent.parent.gameObject;
                    hit.collider.transform.parent.parent.GetComponent<ApartmentUnity>().unresolvableOnRight = true;
                    hit.collider.transform.parent.parent.GetComponent<ApartmentUnity>().editableMesh.constrainUnresolvedR = true;
                }
            }

            for (int i = 0; i < apartments.Count; i++)
            {
                apartments[i].CheckNeighbours();
            }

            apartments.TrimExcess();
        }

        private void ResetLayout()
        {
            if (coresParent != null)
            {
                Destroy(coresParent);
            }
            coresParent = new GameObject("Cores");
            coresParent.transform.SetParent(transform);
            coresParent.transform.localPosition = Vector3.zero;

            if (corridors != null)
            {
                Destroy(corridors);
            }
            corridors = new GameObject("Corridors");
            corridors.transform.SetParent(transform);
            corridors.transform.localPosition = Vector3.zero;
            if (aptParent != null)
            {
                Destroy(aptParent);
            }
            aptParent = new GameObject("AptParent");
            aptParent.transform.SetParent(transform);
            aptParent.transform.localPosition = Vector3.zero;
            if (apartments == null)
                apartments = new List<ApartmentUnity>();
            else apartments.Clear();

            if (floorVertices == null)
                floorVertices = new List<GeometryVertex>();
            else floorVertices.Clear();

            if (cornerAptPoints == null)
                cornerAptPoints = new List<Vector3[]>();
            else cornerAptPoints.Clear();

            if (isExteriorCorner == null)
                isExteriorCorner = new List<bool>();
            else isExteriorCorner.Clear();

            if (cornersParams == null)
                cornersParams = new List<float>();
            else cornersParams.Clear();

            corridors.transform.localPosition = Vector3.zero;
            activeCorridorLengths = new List<List<Line>>[2];

            if (unresolvables == null)
                unresolvables = new List<ApartmentUnity>();
            else unresolvables.Clear();

            drawBuildingLines.RemoveBuildingLines(key);
        }
        private void GetCoresPositions(float maxWidth)
        {

            GetCornerPoints();
            cornersParams.Sort();
            coresParams = new List<float>();

            for (int i = 0; i < masterFloor.coresPositions.Count; i++)
            {
                Vector3 projection = new Vector3(masterFloor.coresPositions[i].x, centreLine[0].currentPosition.y, masterFloor.coresPositions[i].z);
                float par = 0.0f;
                float distance = float.MaxValue;
                centreLine.PointOnCurve(projection, out par, out distance);
                if (distance < 0.2f)
                {
                    coresParams.Add(par);
                }
            }
            var vec1 = centreLine.PointOnCurve(coresParams[0]);
            var vec2 = centreLine.PointOnCurve(coresParams[coresParams.Count - 1]);
            var distanceStart = Vector3.Distance(centreLine[0].currentPosition, vec1);
            var distanceEnd = Vector3.Distance(centreLine[centreLine.Count - 1].currentPosition, vec2);
            Debug.DrawLine(centreLine[0].currentPosition, vec1, Color.green, 1000, false);
            Debug.DrawLine(centreLine[centreLine.Count - 1].currentPosition, vec2, Color.green, 1000, false);
            if (Math.Round(distanceEnd, 1) > Math.Round(maxWidth + coreLength / 2.0f + corridorWidth, 1) || Math.Round(distanceStart, 1) > Math.Round(maxWidth + coreLength / 2.0f + corridorWidth, 1))
            {
                if (!Notifications.TaggedObject.activeNotifications.Contains("DistanceFromCores"))
                {
                    Notifications.TaggedObject.activeNotifications.Add("DistanceFromCores");
                }
            }
            else
            {
                if (Notifications.TaggedObject.activeNotifications.Contains("DistanceFromCores"))
                {
                    Notifications.TaggedObject.activeNotifications.Remove("DistanceFromCores");
                }
            }
            Notifications.TaggedObject.UpdateNotifications();

            var groupCoresPars = GroupCoresPerSegment(coresParams);

            cores = new List<List<Vector3[]>>();

            for (int i = 0; i < groupCoresPars.Count; i++)
            {
                cores.Add(new List<Vector3[]>());
                for (int j = 0; j < groupCoresPars[i].Count; j++)
                {
                    Vector3 tangent;
                    var position = centreLine.PointOnCurve(groupCoresPars[i][j], out tangent);
                    cores.Last().Add(new Vector3[] { position, tangent });

                    GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    obj.name = "Core_" + i + "_";
                    obj.AddComponent<BoxCollider>();
                    obj.transform.SetParent(coresParent.transform);
                    obj.transform.position = position;
                    obj.transform.LookAt(obj.transform.position + tangent * 1);
                    obj.transform.localScale = new Vector3(coreWidth, floor2floor, coreLength);

                    switch (coreAllignment)
                    {
                        case CoreAllignment.Centre:
                            obj.transform.position += new Vector3(0, floor2floor / 2.0f, 0);
                            break;
                        case CoreAllignment.Left:
                            obj.transform.position += (new Vector3(0, floor2floor / 2.0f, 0) + Vector3.Cross(tangent, Vector3.up).normalized * (apartmentDepth + corridorWidth / 2.0f - coreWidth * 0.5f + Standards.TaggedObject.ConstructionFeatures["CorridorWall"]));
                            break;
                        case CoreAllignment.Right:
                            obj.transform.position += (new Vector3(0, floor2floor / 2.0f, 0) + Vector3.Cross(tangent, -Vector3.up).normalized * (apartmentDepth - coreWidth * 0.5f + corridorWidth / 2.0f + Standards.TaggedObject.ConstructionFeatures["CorridorWall"]));
                            break;
                    }
                }
            }
        }

        private void GetCoresPositions(float coresRadius, float corridorWidth, float maxWidth)
        {

            GetCornerPoints();
            cornersParams.Sort();

            coresParams = GetCorePositionParams(apartmentDepth, coresRadius, coreLength / 2.0f, maxWidth);

            var groupCoresPars = GroupCoresPerSegment(coresParams);

            cores = new List<List<Vector3[]>>();

            for (int i = 0; i < groupCoresPars.Count; i++)
            {
                cores.Add(new List<Vector3[]>());
                for (int j = 0; j < groupCoresPars[i].Count; j++)
                {
                    Vector3 tangent;
                    var position = centreLine.PointOnCurve(groupCoresPars[i][j], out tangent);
                    cores.Last().Add(new Vector3[] { position, tangent });

                    GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    obj.name = "Core_" + i + "_";
                    obj.AddComponent<BoxCollider>();
                    obj.transform.SetParent(coresParent.transform);
                    obj.transform.position = position;
                    obj.transform.LookAt(obj.transform.position + tangent * 1);
                    obj.transform.localScale = new Vector3(coreWidth, floor2floor, coreLength);

                    switch (coreAllignment)
                    {
                        case CoreAllignment.Centre:
                            obj.transform.position += new Vector3(0, floor2floor / 2.0f, 0);
                            break;
                        case CoreAllignment.Left:
                            obj.transform.position += (new Vector3(0, floor2floor / 2.0f, 0) + Vector3.Cross(tangent, Vector3.up).normalized * (apartmentDepth + corridorWidth / 2.0f - coreWidth * 0.5f + Standards.TaggedObject.ConstructionFeatures["CorridorWall"]));
                            break;
                        case CoreAllignment.Right:
                            obj.transform.position += (new Vector3(0, floor2floor / 2.0f, 0) + Vector3.Cross(tangent, -Vector3.up).normalized * (apartmentDepth - coreWidth * 0.5f + corridorWidth / 2.0f + Standards.TaggedObject.ConstructionFeatures["CorridorWall"]));
                            break;
                    }

                }
            }
        }

        private List<List<float>> GroupCoresPerSegment(List<float> pars)
        {
            List<List<float>> parPerSegment = new List<List<float>>();
            float parStep = 1.0f / (centreLine.Count - 1);

            for (int i = 0; i < centreLine.Count - 1; i++)
            {
                parPerSegment.Add(new List<float>());
                for (int j = 0; j < pars.Count; j++)
                {
                    if (pars[j] >= parStep * i && pars[j] <= parStep * (i + 1))
                    {
                        parPerSegment.Last().Add(pars[j]);
                    }
                }
            }

            return parPerSegment;
        }

        private List<float> GetCorePositionParams(float depth, float coresStep, float halfCore, float maxWidth)
        {
            List<float> coreParams = new List<float>();
            List<float> coreLengths = new List<float>();
            List<float> cornerLengths = new List<float>();

            if (coreAllignment == CoreAllignment.Centre)
            {
                maxWidth += (halfCore + corridorWidth);
            }
            else
            {
                maxWidth += halfCore;
            }

            Polygon polyline = centreLine;

            if (polyline.Length() >= coresStep)
            {

                for (int i = 0; i < cornersParams.Count; i++)
                {
                    cornerLengths.Add(polyline.ParameterToLength(cornersParams[i]));
                }

                float activeLength = polyline.Length() - 2.0f * (maxWidth);
                int divNum = Mathf.CeilToInt(activeLength / coresStep);
                float temp_Step = activeLength / divNum;
                float currentLength = maxWidth;
                float remainderLength = activeLength;
                coreLengths.Add(maxWidth);

                while (currentLength < activeLength)
                {
                    currentLength += temp_Step;
                    for (int i = 0; i < cornerLengths.Count; i += 2)
                    {
                        if (currentLength >= cornerLengths[i] && currentLength <= cornerLengths[i + 1])
                        {
                            int deltaLength = (int)System.Math.Round((currentLength - cornerLengths[i]) / (cornerLengths[i + 1] - cornerLengths[i]));

                            if (deltaLength == 1)
                            {
                                if (cornerLengths[i + 1] - currentLength < coresStep)
                                {
                                    currentLength = cornerLengths[i + 1];
                                }
                                else
                                {
                                    currentLength = cornerLengths[i];
                                }
                            }
                            else if (deltaLength == 0)
                            {
                                currentLength = cornerLengths[i];
                            }
                        }
                    }

                    if (currentLength <= activeLength)
                    {
                        coreLengths.Add(currentLength);
                        remainderLength = polyline.Length() - currentLength - maxWidth;
                        divNum--;
                        temp_Step = remainderLength / divNum;
                    }
                }

                coreLengths.Add(polyline.Length() - maxWidth);
                for (int i = 0; i < coreLengths.Count; i++)
                {
                    coreParams.Add(polyline.LengthToParameter(coreLengths[i]));
                }
            }
            else
            {
                coreParams.Add(0.5f);
            }

            return coreParams;
        }

        private IEnumerator DelayedUpdate(Polygon sender)
        {
            if (offsets != null)
            {
                bool isActive = transform.GetChild(0).gameObject.activeSelf;
                apartments = new List<ApartmentUnity>();
                yield return StartCoroutine(building.buildingManager.PopulateInterior(false));
                yield return StartCoroutine(GenerateGeometries(building.prevMinimumWidth));
                var custom = sender as PolygonSegment;
                if (custom != null)
                {
                    if (masterFloor != null)
                    {
                        masterFloor.GeneratePlantRoom();
                        masterFloor.UpdateFloorOutline(building.buildingManager.previewMode);
                    }
                }
                UpdateFloorOutline(building.buildingManager.previewMode);
                building.RequestOverallUpdate();
                building.buildingManager.ClearUndoActions();
                if (!isActive)
                {
                    ToggleChildren(false);
                }
            }
        }
        private List<int> GetApartmentNumbers()
        {
            Dictionary<string, int> numbers = new Dictionary<string, int>();

            foreach (var item in Standards.TaggedObject.ApartmentTypesMinimumSizes)
            {
                if (!numbers.ContainsKey(item.Key))
                {
                    numbers.Add(item.Key, 0);
                }
            }

            if (apartments != null)
            {
                for (int i = 0; i < apartments.Count; i++)
                {
                    if (numbers.ContainsKey(apartments[i].ApartmentType))
                    {
                        numbers[apartments[i].ApartmentType]++;
                    }
                    else
                    {
                        numbers.Add(apartments[i].ApartmentType, 1);
                    }
                }
            }

            return numbers.Values.ToList();
        }

        private Dictionary<string, float[]> GetApartmentStats()
        {
            Dictionary<string, float[]> stats = new Dictionary<string, float[]>();

            foreach (var item in Standards.TaggedObject.ApartmentTypesMinimumSizes)
            {
                if (!stats.ContainsKey(item.Key))
                {
                    stats.Add(item.Key, new float[2] { 0, 0 });
                }
            }

            if (apartments != null)
            {
                for (int i = 0; i < apartments.Count; i++)
                {
                    if (stats.ContainsKey(apartments[i].ApartmentType))
                    {
                        if (apartments[i].ApartmentType != "SpecialApt")
                        {
                            stats[apartments[i].ApartmentType][0]++;
                            stats[apartments[i].ApartmentType][1] += apartments[i].NIA;
                        }
                        else
                        {
                            stats[apartments[i].ApartmentType][0]++;
                            stats[apartments[i].ApartmentType][1] += apartments[i].Area;
                        }
                    }
                    else
                    {
                        if (apartments[i].ApartmentType != "SpecialApt")
                            stats.Add(apartments[i].ApartmentType, new float[] { 1, apartments[i].NIA });
                        else
                            stats.Add(apartments[i].ApartmentType, new float[] { 1, apartments[i].Area });
                    }
                }
            }

            return stats;
        }

        private void CheckDistances(float coresRadius, float corridorWidth, List<Vector3[]> coreCentres, int i, int next)
        {
            Vector3 start = centreLine[i].currentPosition;
            Vector3 end = centreLine[next].currentPosition;
            Vector3 dir = end - start;
            float length = Vector3.Distance(end, start);
            List<Vector3[]> myCores = new List<Vector3[]>();
            List<Vector3[]> myCorridors = new List<Vector3[]>();

            if (length >= minimumCorridorLengthForCore && length <= minimumCorridorLengthForCore * 1.5f)
            {
                myCores.Add(new Vector3[2] { start + (end - start) / 2.0f, dir });
            }
            else if (length > minimumCorridorLengthForCore * 1.5f && length < minimumCorridorLengthForCore + 2 * coresRadius)
            {
                start += dir.normalized * coreCornerOffset;
                end -= dir.normalized * coreCornerOffset;
                myCores.Add(new Vector3[2] { start, dir });
                //                myCores.Add(new Vector3[2] { start + (end-start)/2.0f, dir });
                myCores.Add(new Vector3[2] { end, dir });
            }
            else if (length >= minimumCorridorLengthForCore + 2 * coresRadius)
            {
                start += dir.normalized * coreCornerOffset;
                end -= dir.normalized * coreCornerOffset;
                myCores.Add(new Vector3[2] { start, dir });
                length = Vector3.Distance(start, end);
                var num = Mathf.FloorToInt(length / coresRadius);
                var step = length / num;
                if (num > 0)
                {
                    for (int j = 1; j < num; j++)
                    {
                        myCores.Add(new Vector3[2] { start + dir.normalized * (step * j), dir });
                    }
                }
                myCores.Add(new Vector3[2] { end, dir });
            }

            coreCentres.AddRange(myCores);

        }

        protected virtual float GetNIA()
        {
            if (hasGeometry)
            {
                if (!percentages.ContainsKey("Commercial") && !percentages.ContainsKey("Other"))
                {
                    nia = 0.0f;

                    if (apartments != null)
                    {
                        for (int i = 0; i < apartments.Count; i++)
                        {
                            nia += apartments[i].NIA;
                        }
                    }
                    nia = Mathf.Round(nia);
                    return nia;
                }
                else
                {
                    return 0;
                }
            }
            else return 0;
        }

        protected virtual float GetTotalFloorArea()
        {
            if (hasGeometry)
            {
                if (exteriorPolygon != null)
                {
                    return exteriorPolygon.Area;
                }
                else
                {
                    return 0;
                }
            }
            else return 0;
        }

        private void GetActiveCorridorsOnSide(bool createApts = true)
        {
            floorActiveCorridorLength = 0;

            for (int i = 0; i < activeCorridorLengths.Length; i++)
            {
                if (activeCorridorLengths[i] != null)
                {
                    for (int j = 0; j < activeCorridorLengths[i].Count; j++)
                    {
                        for (int k = 0; k < activeCorridorLengths[i][j].Count; k++)
                        {
                            Vector3 proj;
                            Vector3[] line = new Vector3[] { offsets[i][j][0].currentPosition, offsets[i][j][1].currentPosition };

                            if (!ProjectedInside(activeCorridorLengths[i][j][k].start, line, out proj))
                            {
                                var point2Project = line[FindClosestPoint(line, activeCorridorLengths[i][j][k].start)];

                                Vector3 projection = BrydenWoodUtils.ProjectOnCurve(activeCorridorLengths[i][j][k].start, activeCorridorLengths[i][j][k].end, point2Project);

                                var corners = new Vector3[3] { projection, point2Project, activeCorridorLengths[i][j][k].start };

                                if (Polygon.IsClockWise(corners.ToList()))
                                {
                                    corners = corners.Reverse().ToArray();
                                }
                                if (createApts)
                                {
                                    cornerAptPoints.Add(corners);
                                    isExteriorCorner.Add(false);
                                }
                                activeCorridorLengths[i][j][k] = new Line() { start = projection, end = activeCorridorLengths[i][j][k].end };
                            }

                            if (!ProjectedInside(activeCorridorLengths[i][j][k].end, line, out proj))
                            {
                                var point2Project = line[FindClosestPoint(line, activeCorridorLengths[i][j][k].end)];

                                Vector3 projection = BrydenWoodUtils.ProjectOnCurve(activeCorridorLengths[i][j][k].start, activeCorridorLengths[i][j][k].end, point2Project);

                                var corners = new Vector3[3] { projection, point2Project, activeCorridorLengths[i][j][k].end };

                                if (Polygon.IsClockWise(corners.ToList()))
                                {
                                    corners = corners.Reverse().ToArray();
                                }
                                if (createApts)
                                {
                                    cornerAptPoints.Add(corners);
                                    isExteriorCorner.Add(false);
                                }
                                activeCorridorLengths[i][j][k] = new Line() { start = activeCorridorLengths[i][j][k].start, end = projection };
                            }


                            floorActiveCorridorLength += Vector3.Distance(activeCorridorLengths[i][j][k].start, activeCorridorLengths[i][j][k].end);

                        }
                    }
                }
            }
        }

        private void CheckIntersections(List<List<Polygon>> offsets)
        {
            for (int i = 0; i < offsets.Count; i++)
            {
                if (centreLine.closed)
                {
                    for (int j = 0; j < offsets[i].Count; j++)
                    {
                        Vector3 intersectionPoint;
                        offsets[i][j].Intersects(offsets[i][(j + 1) % offsets[i].Count], out intersectionPoint, true);
                    }
                }
                else
                {
                    for (int j = 0; j < offsets[i].Count - 1; j++)
                    {
                        Vector3 intersectionPoint;
                        offsets[i][j].Intersects(offsets[i][j + 1], out intersectionPoint, true);
                    }
                }
            }
            //UpdateLineDrawing();
        }
        #endregion
    }
}
