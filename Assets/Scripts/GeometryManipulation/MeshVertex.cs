﻿using BrydenWoodUnity.DesignData;
using BrydenWoodUnity.UIElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BrydenWoodUnity.GeometryManipulation
{

    /// <summary>
    /// A MonoBehaviour component for a Mesh Vertex (inherits from Geometry Vertex)
    /// </summary>
    public class MeshVertex : GeometryVertex
    {

        public EditableMesh editableMesh { get; set; }

        /// <summary>
        /// Sets the groups of the mesh vertices
        /// </summary>
        /// <param name="vertices">The vertices from which the group will be generated</param>
        public override void SetGroup(List<GeometryVertex> vertices)
        {
            group = new List<GeometryVertex>();
            groupDiffs = new List<Vector3>();
            for (int i = 0; i < vertices.Count; i++)
            {
                if (vertices[i] != this && vertices[i]!=null)
                {
                    if (Vector3.Distance(currentPosition, vertices[i].currentPosition) < 0.1f)
                    {
                        var vert = vertices[i] as MeshVertex;
                        if (vert != null)
                        {
                            group.Add(vert);
                            groupDiffs.Add(vert.currentPosition - currentPosition);
                        }
                    }
                }
            }
        }
    }
}