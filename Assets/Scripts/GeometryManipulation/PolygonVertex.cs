﻿using BrydenWoodUnity.UIElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BrydenWoodUnity.GeometryManipulation
{

    /// <summary>
    /// A MonoBehaviour component for a Polygon Vertex (inherits from Geometry Vertex)
    /// </summary>
    public class PolygonVertex : GeometryVertex
    {
        #region Public Fields and Properties
        public List<Polygon> polygons { get; set; }
        public Polygon polygon { get; set; }
        #endregion

        #region Private Fields and Properties
        private List<int> polygonVerts;
        #endregion

        #region Public Methods
        /// <summary>
        /// Initializes the Instance
        /// </summary>
        /// <param name="index">The index of the polygon vertex</param>
        public override void Initialize(int index)
        {
            base.Initialize(index);
            polygons = new List<Polygon>();
            polygonVerts = new List<int>();
        }

        /// <summary>
        /// Sets the polygon to which the vertex belongs
        /// </summary>
        /// <param name="polygon">The polygon to which it belongs</param>
        public void SetPolygon(Polygon polygon)
        {
            this.polygon = polygon;
            for (int i = 0; i < polygon.Count; i++)
            {
                polygonVerts.Add(polygon[i].index);
            }
        }

        /// <summary>
        /// Sets the local position of the polygon vertex
        /// </summary>
        /// <param name="localPos">The new local position</param>
        public void SetLocalPosition(Vector3 localPos)
        {
            transform.localPosition = localPos;
            currentLocalPosition = localPos;
            currentPosition = transform.position;
            if (cpui != null)
            {
                cpui.UpdatePositions(transform);
            }
            OnMoved(this);

        }

        /// <summary>
        /// Adds a polygon to the polygons the vertex belongs
        /// </summary>
        /// <param name="polygon">The polygon to be added</param>
        public void AddPolygon(Polygon polygon)
        {
            polygons.Add(polygon);
            for (int i = 0; i < polygon.Count; i++)
            {
                polygonVerts.Add(polygon[i].index);
            }
        }
        #endregion

    }
}
