﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrydenWoodUnity;
using BrydenWoodUnity.DesignData;
using ChartAndGraph;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component to control the Chart graph for the apartment types
    /// </summary>
    public class ApartmentTypesChart : DesignDataChart
    {
        #region Public Fields and Properties
        public ProceduralBuildingManager buildingManager;
        #endregion

        #region Private Fields and Properties
        private string selectedType;
        private DateTime lastExit;
        private bool exited;
        #endregion

        #region Events

        /// <summary>
        /// Called when an apartment type has been selected
        /// </summary>
        [Header("Events:")]
        public OnTypeSelectedEvent myTypeSelected;
        /// <summary>
        /// Called when an apartment type has been deselected
        /// </summary>
        public OnTypeSelectedEvent myTypeDeselect;
        #endregion

        #region MonoBehaviour Methods

        // Use this for initialization
        void Awake()
        {
            Initialize();
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject(-1))
            {
                selected = false;
                OnNonHovered();
            }
        }
        #endregion


        #region Public Methods
        /// <summary>
        /// Called when a type is hover over
        /// </summary>
        /// <param name="eventArgs">The arguments of the hover event</param>
        public override void OnHoverOver(PieChart.PieEventArgs eventArgs)
        {
            if (designData != null)
            {
                if (!selected)
                {
                    //var floorUnity = (ProceduralFloor)designData;
                    selectedType = eventArgs.Category;
                    buildingManager.ShowSelectedApartmentType(selectedType, "");
                    //floorUnity.ColorApartmentsOfType(selectedType, true);
                }
            }
        }

        /// <summary>
        /// Called when no type is hovered
        /// </summary>
        public override void OnNonHovered()
        {
            if (designData != null)
            {
                if (!selected)
                {
                    if (myTypeDeselect!=null)
                    {
                        myTypeDeselect.Invoke(selectedType, new List<ApartmentUnity>());
                        buildingManager.ShowSelectedApartmentType("", selectedType);
                    }
                }
            }
        }

        /// <summary>
        /// Called when a type is clicked
        /// </summary>
        /// <param name="eventArgs">The arguments of the click event</param>
        public override void OnClicked(PieChart.PieEventArgs eventArgs)
        {
            List<ApartmentUnity> apts = new List<ApartmentUnity>();
            string prevType = "";
            if (eventArgs.Category != selectedType)
            {
                prevType = selectedType;
            }
            apts = buildingManager.ShowSelectedApartmentType(eventArgs.Category, prevType);
            selectedType = eventArgs.Category;

            selected = true;

            if (myTypeSelected!=null)
            {
                myTypeSelected.Invoke(selectedType, apts);
            }
        }

        /// <summary>
        /// Sets the chart values
        /// </summary>
        /// <param name="designData">The Design data object from which to get the values</param>
        /// <param name="original">Whether the values are the originally set brief</param>
        public override void SetChartValues(BaseDesignData designData, bool original = false)
        {
            Initialize();
            title.text = "Apartment Types:";
            this.designData = designData;

            var floorUnity = designData as ProceduralFloor;

            chart.DataSource.StartBatch();
            Dictionary<string, int> types = new Dictionary<string, int>();
            if (!original)
            {
                for (int i = 0; i < floorUnity.apartments.Count; i++)
                {
                    if (types.ContainsKey(floorUnity.apartments[i].ApartmentType))
                    {
                        types[floorUnity.apartments[i].ApartmentType]++;
                    }
                    else
                    {
                        types.Add(floorUnity.apartments[i].ApartmentType, 1);
                    }
                }
            }

            foreach (var item in types)
            {
                Color typeColor;
                if (Standards.TaggedObject.ApartmentTypesColours.ContainsKey(item.Key))
                {
                    typeColor = Standards.TaggedObject.ApartmentTypesColours[item.Key];
                }
                else
                {
                    typeColor = Color.white;
                }
                Color m_color = typeColor;
                Material typeMat = new Material(chartMaterial);
                typeMat.color = m_color;

                chart.DataSource.AddCategory(item.Key, typeMat);
                chart.DataSource.GetMaterial(item.Key).Normal = typeMat;
                chart.DataSource.GetMaterial(item.Key).Hover = m_color;
                chart.DataSource.GetMaterial(item.Key).Selected = m_color;
                chart.DataSource.SetValue(item.Key, item.Value);
            }

            chart.DataSource.EndBatch();
        }

        /// <summary>
        /// Sets the chart values
        /// </summary>
        /// <param name="areas">Areas per apartment type</param>
        /// <param name="title">The title for the info to be displayed</param>
        public void SetChartValues(Dictionary<string, float> areas, string title)
        {
            Initialize();
            this.title.text = title;//"Apartment Types Areas(m\xB2):";

            chart.DataSource.StartBatch();

            foreach (var item in areas)
            {
                Color typeColor;
                if (Standards.TaggedObject.ApartmentTypesColours.ContainsKey(item.Key))
                {
                    typeColor = Standards.TaggedObject.ApartmentTypesColours[item.Key];
                }
                else
                {
                    typeColor = Color.white;
                }
                Color m_color = typeColor;
                Material typeMat = new Material(chartMaterial);
                typeMat.color = m_color;

                chart.DataSource.AddCategory(item.Key, typeMat);
                chart.DataSource.GetMaterial(item.Key).Normal = typeMat;
                chart.DataSource.GetMaterial(item.Key).Hover = m_color;
                chart.DataSource.GetMaterial(item.Key).Selected = m_color;
                chart.DataSource.SetValue(item.Key, item.Value);
            }

            chart.DataSource.EndBatch();
        }
        #endregion
    }

    /// <summary>
    /// A Unity Event class for when an apartment type is selected
    /// </summary>
    [System.Serializable]
    public class OnTypeSelectedEvent : UnityEvent<string, List<ApartmentUnity>>
    {

    }
}
