﻿using BrydenWoodUnity.DesignData;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for managing the building elements
    /// </summary>
    public class BuildingPanel : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Scene References:")]
        public Transform buildingElementHolder;
        public GameObject buildingElementPrefab;
        public FloorPanel floorPanel;
        public FloorPanel towerFloorPanel;
        public BuildingElement currentElement;
        public Button addBuildingButton;
        public Dropdown coreAllignmentDropdown;
        public Dropdown typologyDropdown;
        public InputField buildingNameInputField;
        public InputField apartmentDepthField;
        public InputField corridorWidthField;
        public DesignInputTabs designInputTabs;
        public GameObject linearBuildingUI;
        public GameObject towerBuildingUI;
        public Button placeButton;
        public Button rotateButton;
        public int currentElementIndex;
        public bool loading;
        public object waitFrame;

        public int floorIndex { get { return floorPanel.currentElementIndex; } }
        #endregion

        #region MonoBehaviour Methods
        private void OnDestroy()
        {
            RemoveEvents();
        }

        // Use this for initialization
        void Start()
        {
            AddEvents();
            waitFrame = new WaitForEndOfFrame();
        }
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when a building element is deleted
        /// </summary>
        /// <param name="sender">The deleted building element</param>
        public void OnBuildingDeleted(BuildingElement sender)
        {
            StartCoroutine(OnBuildingElementDeleted(sender));
        }

        /// <summary>
        /// Called when a floor element is deleted
        /// </summary>
        /// <param name="floorIndex">The index of the deleted floor element</param>
        public void OnFloorDeleted(int floorIndex)
        {
            currentElement.OnFloorDeleted(floorIndex);
        }

        /// <summary>
        /// Called when the brief of a floor element has changed
        /// </summary>
        /// <param name="floorIndex">The index of the floor element</param>
        /// <param name="percentages">The new brief percentages</param>
        public void OnBriefChanged(int floorIndex, Dictionary<string, float> percentages)
        {
            if (floorIndex < currentElement.buildingData.percentages.Count)
            {
                currentElement.buildingData.percentages[floorIndex] = percentages;
            }
            else
            {
                currentElement.buildingData.percentages.Add(percentages);
            }
            designInputTabs.OnBriefElementChanged(floorIndex, percentages);
        }

        /// <summary>
        /// Called from the Unity UI to add a linear building element to the list
        /// </summary>
        public void AddLinearBuildingElement()
        {
            if (currentElement != null)
            {
                currentElement.Deselect();
            }

            linearBuildingUI.SetActive(true);
            towerBuildingUI.SetActive(false);
            buildingNameInputField.SetValue("");
            floorPanel.ClearPrevious();
            GameObject obj = Instantiate(buildingElementPrefab, buildingElementHolder);
            obj.transform.SetSiblingIndex(buildingElementHolder.childCount - 2);
            obj.name = "BuildingElement_" + (buildingElementHolder.childCount - 1);
            currentElement = obj.GetComponent<BuildingElement>();
            currentElement.buildingName = (buildingElementHolder.childCount - 1).ToString();

            float aptDepth = 7.2f;
            float crdrWidth = 1.2f;
            float.TryParse(apartmentDepthField.text, out aptDepth);
            float.TryParse(corridorWidthField.text, out crdrWidth);

            currentElement.Initialize(aptDepth, crdrWidth, BuildingType.Linear);
            currentElementIndex = buildingElementHolder.childCount - 2;

            if (coreAllignmentDropdown.options.Count == 2)
            {
                coreAllignmentDropdown.options.Insert(0, new Dropdown.OptionData("Centre"));
                coreAllignmentDropdown.SetValue(0);
                coreAllignmentDropdown.RefreshShownValue();
            }

            coreAllignmentDropdown.SetValue(0);
            typologyDropdown.SetValue(0);
            coreAllignmentDropdown.onValueChanged.RemoveAllListeners();
            typologyDropdown.onValueChanged.RemoveAllListeners();
            buildingNameInputField.onValueChanged.RemoveAllListeners();
            coreAllignmentDropdown.onValueChanged.AddListener(currentElement.OnCoreAllignmentChanged);
            typologyDropdown.onValueChanged.AddListener(currentElement.OnAspectChanged);
            buildingNameInputField.onValueChanged.AddListener(currentElement.OnBuildingNameChanged);

            currentElement.typologyDropdown = typologyDropdown;
            currentElement.coreAlignmentDropdown = coreAllignmentDropdown;
            currentElement.buildingNameInputField = buildingNameInputField;
            currentElement.corridorWidthField = corridorWidthField;
            currentElement.SetProceduralBuilding(designInputTabs.OnBuildingAdded(currentElement));
            if (!loading)
            {
                AddGroundFloor();
                AddSecondFloor();
            }
            obj.GetComponent<Toggle>().SetValue(true);
        }

        /// <summary>
        /// Called from the Unity UI to add a tower building element to the list
        /// </summary>
        public void AddTowerBuilding()
        {
            if (currentElement != null)
            {
                currentElement.Deselect();
            }
            //placeButton.interactable = true;
            //placeButton.transform.GetChild(0).GetComponent<Text>().text = "Place";
            //rotateButton.interactable = false;
            linearBuildingUI.SetActive(false);
            towerBuildingUI.SetActive(true);
            buildingNameInputField.SetValue("");
            towerFloorPanel.ClearPrevious();
            GameObject obj = Instantiate(buildingElementPrefab, buildingElementHolder);
            obj.transform.SetSiblingIndex(buildingElementHolder.childCount - 2);
            obj.name = "BuildingElement_" + (buildingElementHolder.childCount - 1);
            currentElement = obj.GetComponent<BuildingElement>();
            currentElement.buildingName = (buildingElementHolder.childCount - 1).ToString();

            float aptDepth = 7.2f;
            float crdrWidth = 1.2f;
            float.TryParse(apartmentDepthField.text, out aptDepth);
            float.TryParse(corridorWidthField.text, out crdrWidth);

            currentElement.Initialize(aptDepth, crdrWidth, BuildingType.Tower);
            currentElementIndex = buildingElementHolder.childCount - 2;

            buildingNameInputField.onValueChanged.RemoveAllListeners();
            buildingNameInputField.onValueChanged.AddListener(currentElement.OnBuildingNameChanged);

            currentElement.SetProceduralBuilding(designInputTabs.OnBuildingAdded(currentElement));
            if (!loading)
            {
                AddGroundFloor();
            }
            obj.GetComponent<Toggle>().SetValue(true);
        }

        /// <summary>
        /// Called from the Unity UI to cancel the addition of a building element
        /// </summary>
        public void CancelBuildingAddition()
        {

        }

        /// <summary>
        /// Called when the values of a floor element have changed
        /// </summary>
        /// <param name="floorIndex">The index of the floor element</param>
        /// <param name="levels">The new levels of the floor element</param>
        /// <param name="f2f">The new floor to floor height of the floor element</param>
        /// <param name="aptTypes">The new list of apartment types of the floor element</param>
        public void UpdateFloorValues(int floorIndex, List<int> levels, float f2f, string[] aptTypes = null)
        {
            currentElement.UpdateFloorValues(floorIndex, levels, f2f, aptTypes);
        }

        /// <summary>
        /// Adds a building element with set values to the list
        /// </summary>
        /// <param name="building">The building data</param>
        public void AddBuildingElement(BuildingData building)
        {
            if (currentElement != null)
            {
                currentElement.GetComponent<Toggle>().isOn = false;
            }
            GameObject obj = Instantiate(buildingElementPrefab, buildingElementHolder);
            obj.name = "BuildingElement_" + (buildingElementHolder.childCount - 1);
            obj.transform.SetSiblingIndex(buildingElementHolder.childCount - 2);
            currentElementIndex = buildingElementHolder.childCount - 2;
            currentElement = obj.GetComponent<BuildingElement>();
            currentElement.Initialize();
            currentElement.typologyDropdown = typologyDropdown;
            currentElement.coreAlignmentDropdown = coreAllignmentDropdown;
            currentElement.buildingNameInputField = buildingNameInputField;
            currentElement.corridorWidthField = corridorWidthField;

            coreAllignmentDropdown.onValueChanged.RemoveAllListeners();
            typologyDropdown.onValueChanged.RemoveAllListeners();
            buildingNameInputField.onValueChanged.RemoveAllListeners();
            coreAllignmentDropdown.onValueChanged.AddListener(currentElement.OnCoreAllignmentChanged);
            typologyDropdown.onValueChanged.AddListener(currentElement.OnAspectChanged);
            buildingNameInputField.onValueChanged.AddListener(currentElement.OnBuildingNameChanged);



            currentElement.SetProceduralBuilding(designInputTabs.OnBuildingAdded(currentElement));
            currentElement.SetBuildingData(building);

            for (int i = 0; i < building.levels.Count; i++)
            {
                floorPanel.AddFloorElement(building.levels[i], building.floorHeights[i], currentElement.proceduralBuilding.AddFloor(i, "LoadedFloor"));
            }

            obj.GetComponent<Toggle>().group = GetComponent<ToggleGroup>();
            obj.GetComponent<Toggle>().isOn = true;
        }

        /// <summary>
        /// Loads a tower building element
        /// </summary>
        /// <param name="building">The data of the building to be loaded</param>
        public void LoadTowerBuildingElement(BuildingData building)
        {
            if (currentElement != null)
            {
                currentElement.Deselect();
            }
            towerBuildingUI.SetActive(true);
            linearBuildingUI.SetActive(false);
            GameObject obj = Instantiate(buildingElementPrefab, buildingElementHolder);
            obj.name = "BuildingElement_" + (buildingElementHolder.childCount - 1);
            obj.transform.SetSiblingIndex(buildingElementHolder.childCount - 2);
            currentElementIndex = buildingElementHolder.childCount - 2;
            currentElement = obj.GetComponent<BuildingElement>();
            currentElement.Initialize();

            apartmentDepthField.SetValue(building.apartmentDepth.ToString("0.00"));
            currentElement.buildingNameInputField = buildingNameInputField;
            buildingNameInputField.onValueChanged.RemoveAllListeners();
            buildingNameInputField.onValueChanged.AddListener(currentElement.OnBuildingNameChanged);

            currentElement.SetProceduralBuilding(designInputTabs.OnBuildingAdded(currentElement));
            currentElement.SetBuildingData(building);

            towerFloorPanel.ClearPrevious();

            for (int i = 0; i < building.levels.Count; i++)
            {
                towerFloorPanel.LoadTowerFloorElement(building.levels[i], building.floorHeights[i], building.percentages[i], currentElement.proceduralBuilding.LoadTowerFloor(i, "LoadedFloor", building.percentages[i], building.floorHeights[i], building.levels[i].ToArray(), building.aptTypesTower[i]));
            }
        }

        /// <summary>
        /// Loads a linear building element
        /// </summary>
        /// <param name="building">The data of the building to be loaded</param>
        public void LoadLinearBuildingElement(BuildingData building)
        {
            if (currentElement != null)
            {
                currentElement.Deselect();
            }
            towerBuildingUI.SetActive(false);
            linearBuildingUI.SetActive(true);
            GameObject obj = Instantiate(buildingElementPrefab, buildingElementHolder);
            obj.name = "BuildingElement_" + (buildingElementHolder.childCount - 1);
            obj.transform.SetSiblingIndex(buildingElementHolder.childCount - 2);
            currentElementIndex = buildingElementHolder.childCount - 2;
            currentElement = obj.GetComponent<BuildingElement>();
            currentElement.Initialize();

            apartmentDepthField.SetValue(building.apartmentDepth.ToString("0.00"));
            corridorWidthField.SetValue(building.corridorWidth.ToString("0.00"));

            currentElement.typologyDropdown = typologyDropdown;
            currentElement.coreAlignmentDropdown = coreAllignmentDropdown;
            currentElement.buildingNameInputField = buildingNameInputField;
            currentElement.corridorWidthField = corridorWidthField;

            coreAllignmentDropdown.onValueChanged.RemoveAllListeners();
            typologyDropdown.onValueChanged.RemoveAllListeners();
            buildingNameInputField.onValueChanged.RemoveAllListeners();
            coreAllignmentDropdown.onValueChanged.AddListener(currentElement.OnCoreAllignmentChanged);
            typologyDropdown.onValueChanged.AddListener(currentElement.OnAspectChanged);
            buildingNameInputField.onValueChanged.AddListener(currentElement.OnBuildingNameChanged);

            currentElement.SetProceduralBuilding(designInputTabs.OnBuildingAdded(currentElement));
            currentElement.SetBuildingData(building);

            floorPanel.ClearPrevious();

            for (int i = 0; i < building.levels.Count; i++)
            {
                floorPanel.LoadFloorElement(building.levels[i], building.floorHeights[i], building.percentages[i], currentElement.proceduralBuilding.LoadFloor(i, "LoadedFloor", building.percentages[i], building.floorHeights[i], building.levels[i].ToArray()));
            }
        }

        /// <summary>
        /// Called when a building element is deleted
        /// </summary>
        /// <param name="sender">The current list of building elements</param>
        public IEnumerator OnBuildingElementDeleted(BuildingElement sender)
        {
            int deleteIndex = -1;
            for (int i = 0; i < buildingElementHolder.childCount - 1; i++)
            {
                if (buildingElementHolder.GetChild(i).GetComponent<BuildingElement>() == sender)
                {
                    Destroy(buildingElementHolder.GetChild(i).gameObject);
                    deleteIndex = i;
                }
            }
            yield return waitFrame;
            try
            {
                if (currentElementIndex > deleteIndex)
                {
                    buildingElementHolder.GetChild(currentElementIndex - 1).GetComponent<Toggle>().SetValue(true);
                    buildingElementHolder.GetChild(currentElementIndex - 1).GetComponent<BuildingElement>().Selected(true);
                }
                else if (currentElementIndex == deleteIndex)
                {
                    if (deleteIndex == buildingElementHolder.childCount - 1)
                    {
                        buildingElementHolder.GetChild(currentElementIndex - 1).GetComponent<Toggle>().isOn = true;
                    }
                    else
                    {
                        buildingElementHolder.GetChild(deleteIndex).GetComponent<Toggle>().isOn = true;
                    }
                }
            }
            catch (Exception e)
            {
                UnityEngine.Debug.Log(e);
            }
            designInputTabs.OnBuildingRemoved(deleteIndex);
        }

        /// <summary>
        /// Returns the data of all the buildings in the project
        /// </summary>
        /// <returns>List of Building Data</returns>
        public List<BuildingData> GetBuildingData()
        {
            List<BuildingData> data = new List<BuildingData>();
            foreach (Transform item in buildingElementHolder)
            {
                var build = item.GetComponent<BuildingElement>();
                if (build != null)
                {
                    data.Add(item.GetComponent<BuildingElement>().buildingData);
                }
            }
            return data;
        }

        /// <summary>
        /// Sets the selected building element based on index
        /// </summary>
        /// <param name="index">The index of the building element to be selected</param>
        public void SetSelectedBuildingElement(int index)
        {
            OnBuildingElementSelected(buildingElementHolder.GetChild(index).GetComponent<BuildingElement>());
        }

        /// <summary>
        /// Called when a floor element is added to the current building
        /// </summary>
        /// <param name="element">The floor element to be added</param>
        /// <param name="floorIndex">The index of the floor element</param>
        /// <returns>Procedural Floor</returns>
        public ProceduralFloor OnFloorAdded(FloorElement element, int floorIndex)
        {
            return currentElement.OnFloorAdded(element, floorIndex);
        }

        /// <summary>
        /// Called from Unity UI when the apartment depth changes
        /// </summary>
        /// <param name="value">The new apartment depth</param>
        public void OnApartmentDepthChanged(string value)
        {
            float depth = 7.2f;
            if (float.TryParse(value, out depth))
            {
                currentElement.buildingData.apartmentDepth = depth;
            }
        }

        /// <summary>
        /// Called from Unity UI when the corridor width changes
        /// </summary>
        /// <param name="value">The new corridor width</param>
        public void OnCorridorWidthChanged(string value)
        {
            float width = 1.2f;
            {
                if (float.TryParse(value, out width))
                {
                    currentElement.buildingData.corridorWidth = width;
                }
            }
        }

        /// <summary>
        /// Called when a building element is selected
        /// </summary>
        /// <param name="sender">The current list of building elements</param>
        public void OnBuildingElementSelected(BuildingElement sender)
        {
            if (currentElement != null && currentElement != sender)
            {
                currentElement.Deselect();
            }
            currentElement = sender;

            if (sender.buildingType == BuildingType.Linear)
            {
                linearBuildingUI.gameObject.SetActive(true);
                towerBuildingUI.gameObject.SetActive(false);
                if (sender.typology != 0)
                {
                    if (coreAllignmentDropdown.options.Count == 3)
                    {
                        coreAllignmentDropdown.options.RemoveAt(0);
                    }
                    coreAllignmentDropdown.SetValue(currentElement.coreAllignment - 1);
                }
                else if (sender.typology == 0)
                {
                    if (coreAllignmentDropdown.options.Count == 2)
                    {
                        coreAllignmentDropdown.options.Insert(0, new Dropdown.OptionData("Centre"));
                    }
                    coreAllignmentDropdown.SetValue(currentElement.coreAllignment);
                }
                coreAllignmentDropdown.RefreshShownValue();

                typologyDropdown.SetValue(currentElement.typology);
                typologyDropdown.RefreshShownValue();



                coreAllignmentDropdown.onValueChanged.RemoveAllListeners();
                typologyDropdown.onValueChanged.RemoveAllListeners();
                coreAllignmentDropdown.onValueChanged.AddListener(currentElement.OnCoreAllignmentChanged);
                typologyDropdown.onValueChanged.AddListener(currentElement.OnAspectChanged);

                floorPanel.ClearPrevious();
                if (sender.proceduralBuilding.floors != null)
                {
                    for (int i = 0; i < sender.proceduralBuilding.floors.Count; i++)
                    {
                        floorPanel.AddFloorElement(sender.proceduralBuilding.floors[i].levels.ToList(), sender.proceduralBuilding.floors[i].floor2floor, sender.proceduralBuilding.floors[i], i == sender.proceduralBuilding.floors.Count - 1, false);
                    }
                }
            }
            else
            {
                linearBuildingUI.gameObject.SetActive(false);
                towerBuildingUI.gameObject.SetActive(true);
                placeButton.gameObject.SetActive(!sender.proceduralBuilding.Placed);
                towerFloorPanel.ClearPrevious();
                if (sender.proceduralBuilding.floors != null)
                {
                    for (int i = 0; i < sender.proceduralBuilding.floors.Count; i++)
                    {
                        var tower = sender.proceduralBuilding.floors[i] as ProceduralTower;
                        if (tower != null)
                        {
                            towerFloorPanel.AddFloorElement(sender.proceduralBuilding.floors[i].levels.ToList(), sender.proceduralBuilding.floors[i].floor2floor, tower, i == sender.proceduralBuilding.floors.Count - 1, false);
                        }
                    }
                }
            }
            buildingNameInputField.onValueChanged.RemoveAllListeners();
            buildingNameInputField.onValueChanged.AddListener(currentElement.OnBuildingNameChanged);
            apartmentDepthField.SetValue(currentElement.buildingData.apartmentDepth.ToString("0.00"));
            buildingNameInputField.SetValue(currentElement.buildingData.name);
            for (int i = 0; i < buildingElementHolder.childCount - 1; i++)
            {
                if (buildingElementHolder.GetChild(i).GetComponent<BuildingElement>() == sender)
                {
                    currentElementIndex = i;
                }
            }
        }
        #endregion

        #region Private Methods
        private void AddGroundFloor()
        {
            currentElement.buildingData.levels.Add(new List<int>() { 0 });
            currentElement.buildingData.percentages.Add(new Dictionary<string, float>() { { "Commercial", 100 } });
            currentElement.buildingData.floorHeights.Add(4.5f);
            currentElement.buildingData.aptTypesTower.Add(new string[] { });
            if (currentElement.buildingType == BuildingType.Linear)
                floorPanel.AddGroundFloor(currentElement.proceduralBuilding.AddFloor(0, "GroundFloor", new int[] { 0 }, 4.5f, new Dictionary<string, float>() { { "Commercial", 100 } }));
            else
                towerFloorPanel.AddGroundFloor(currentElement.proceduralBuilding.AddTowerFloor(0, "GroundFloor", new int[] { 0 }, 4.5f, null));
        }
        private void AddSecondFloor()
        {
            if (currentElement.buildingType == BuildingType.Linear)
                floorPanel.AddSecondFloor();
            else
                towerFloorPanel.AddSecondFloor();
        }
        private void AddEvents()
        {
            BuildingElement.selected += OnBuildingElementSelected;
            BuildingElement.deleted += OnBuildingDeleted;
        }
        private void RemoveEvents()
        {
            BuildingElement.selected -= OnBuildingElementSelected;
            BuildingElement.deleted -= OnBuildingDeleted;
        }
        #endregion
    }
}
