﻿using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using BrydenWoodUnity.DesignData;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for managing all the required inputs for the design process
    /// </summary>
    public class DesignInputTabs : MonoBehaviour
    {
        #region Public Fields and Properties
        public GameObject geometryPanel;
        public BuildingPanel buildingPanel;
        public GameObject firstSeparator;
        public FloorPanel floorPanel;
        public FloorPanel towerFloorPanel;
        public GameObject secondSeparator;
        public GameObject thirdSeparator;
        public BriefPanel briefPanel;
        public RectTransform panelTransform;
        public RectTransform contentTransform;
        public Button customPolygonButton;
        public Button generateButton;
        public Button placeButton;
        public GlobalTooltip generateButtonTooltip;
        public GlobalTooltip placeButtonTooltip;
        public Text instructionText;
        public GameObject coreDropdown;
        public GameObject typologyDropdown;
        public ProceduralBuildingManager buildingManager;
        public InputField offsetDistance;
        public InputField corridorWidthField;

        public float[] heights;

        public int buildingIndex { get { return buildingPanel.currentElementIndex; } }//; set; }
        public int floorIndex { get; set; }

        public List<BuildingData> buildings { get; set; }
        public BuildingData currentBuilding
        {
            get
            {
                try
                {
                    return buildingPanel.currentElement.buildingData;
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                    return null;
                }
            }
        }
        public bool hasSelected { get; set; }
        #endregion

        #region Private Fields and Properties
        private bool loading { get; set; }
        private WaitForEndOfFrame waitFrame { get; set; }
        private StringBuilder stringBuilder;
        #endregion

        #region Events
        /// <summary>
        /// Used when a building is created
        /// </summary>
        public delegate void OnCreateBuilding();
        /// <summary>
        /// Used when the design input for a floor layout has changed
        /// </summary>
        /// <param name="buildingIndex">The index of the building</param>
        /// <param name="floorIndex">The index of the floor</param>
        /// <param name="valuseChanged">Whether the values have changed or not</param>
        public delegate void OnSetFloor(int buildingIndex, int floorIndex, bool valuseChanged = false);
        /// <summary>
        /// Triggered when a floor layout has been deleted
        /// </summary>
        public event OnSetFloor floorDeleted;
        /// <summary>
        /// Triggered when a floor layout has been selected
        /// </summary>
        public event OnSetFloor floorSet;
        /// <summary>
        /// Triggered when a floor layout has been created
        /// </summary>
        public event OnSetFloor floorCreated;
        /// <summary>
        /// Triggered when the brief has been loaded
        /// </summary>
        public event OnCreateBuilding briefLoaded;
        #endregion

        #region MonoBehaviour Methods
        private void OnDestroy()
        {
            foreach (Delegate eh in briefLoaded.GetInvocationList())
            {
                briefLoaded -= (OnCreateBuilding)eh;
            }

            //foreach (Delegate de in floorCreated.GetInvocationList())
            //{
            //    floorCreated -= (OnSetFloor)de;
            //}
        }

        // Use this for initialization
        void Awake()
        {
            buildings = new List<BuildingData>();
            hasSelected = false;
            instructionText.gameObject.SetActive(true);
            geometryPanel.SetActive(false);
            firstSeparator.SetActive(false);
            floorPanel.gameObject.SetActive(false);
            secondSeparator.gameObject.SetActive(false);
            thirdSeparator.SetActive(false);
            briefPanel.gameObject.SetActive(false);
            customPolygonButton.interactable = false;
            coreDropdown.SetActive(false);
            typologyDropdown.SetActive(false);
            panelTransform.sizeDelta = new Vector3(panelTransform.sizeDelta.x, heights[0]);
            contentTransform.sizeDelta = new Vector3(contentTransform.sizeDelta.x, heights[0]);
            waitFrame = new WaitForEndOfFrame();
        }



        // Update is called once per frame
        void Update()
        {

            if (currentBuilding != null)
            {
                if (currentBuilding.percentages.Count > floorIndex)
                {
                    hasSelected = currentBuilding.percentages[floorIndex].Count != 0;
                }
                else
                {
                    hasSelected = false;
                }
                if (currentBuilding.levels.Count > 1)
                {
                    customPolygonButton.interactable = true;
                }
                else
                {
                    customPolygonButton.interactable = false;
                }
            }

        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Sets a brief state to be loaded
        /// </summary>
        /// <param name="briefState">The new brief state</param>
        /// <returns>IEnumerator</returns>
        public IEnumerator SetBriefState(BriefState briefState)
        {
            hasSelected = false;
            instructionText.gameObject.SetActive(false);
            geometryPanel.SetActive(true);
            firstSeparator.SetActive(true);
            floorPanel.gameObject.SetActive(true);
            secondSeparator.gameObject.SetActive(true);
            thirdSeparator.SetActive(true);
            briefPanel.gameObject.SetActive(true);
            customPolygonButton.interactable = true;
            coreDropdown.SetActive(true);
            typologyDropdown.SetActive(true);
            panelTransform.sizeDelta = new Vector3(panelTransform.sizeDelta.x, heights[2]);
            contentTransform.sizeDelta = new Vector3(contentTransform.sizeDelta.x, heights[2]);

            loading = true;
            for (int i = 0; i < briefState.buildings.Count; i++)
            {
                if (briefState.buildings[i].buildingType == 0)
                {
                    buildingPanel.LoadLinearBuildingElement(briefState.buildings[i]);
                }
                else if (briefState.buildings[i].buildingType == 1)
                {
                    buildingPanel.LoadTowerBuildingElement(briefState.buildings[i]);
                }
                yield return waitFrame;
            }

            CheckForStacking();

            if (briefLoaded != null)
            {
                loading = false;
                briefLoaded();
            }
        }

        /// <summary>
        /// Returns a brief state to be saved, based on the created buildings
        /// </summary>
        /// <returns>Brief State for saving</returns>
        public BriefState SerializeBuildings()
        {
            BriefState briefState = new BriefState();
            briefState.buildings = buildingPanel.GetBuildingData();
            return briefState;
        }

        /// <summary>
        /// Called when a building is added
        /// </summary>
        /// <param name="loaded">Whether the building was loaded or not</param>
        public void OnBuildingAdded(bool loaded = true)
        {
            currentBuilding.name = buildingPanel.currentElement.buildingData.name;
            buildingManager.coreAllignment = (CoreAllignment)currentBuilding.coreAllignment;
            buildingManager.typology = (LinearTypology)currentBuilding.linearTypology;
            buildings.Add(currentBuilding);
            if (buildings.Count > 0)
            {
                geometryPanel.SetActive(true);
                instructionText.gameObject.SetActive(false);
                coreDropdown.SetActive(true);
                typologyDropdown.SetActive(true);
                firstSeparator.SetActive(true);
                floorPanel.gameObject.SetActive(true);
                secondSeparator.gameObject.SetActive(true);
                thirdSeparator.SetActive(true);
                briefPanel.gameObject.SetActive(true);
                panelTransform.sizeDelta = new Vector3(panelTransform.sizeDelta.x, heights[2]);
                contentTransform.sizeDelta = new Vector3(contentTransform.sizeDelta.x, heights[2]);
            }
            floorIndex = 1;
            if (!loading)
            {
                StartCoroutine(AddGroundFloor());
            }
        }

        public ProceduralBuilding OnBuildingAdded(BuildingElement buildingElement)
        {
            var procBuilding = buildingManager.CreateNewBuilding(buildingElement.buildingType);
            buildings.Add(buildingElement.buildingData);
            if (buildings.Count > 0)
            {
                geometryPanel.SetActive(true);
                instructionText.gameObject.SetActive(false);
                coreDropdown.SetActive(true);
                typologyDropdown.SetActive(true);
                firstSeparator.SetActive(true);
                floorPanel.gameObject.SetActive(true);
                secondSeparator.gameObject.SetActive(true);
                thirdSeparator.SetActive(true);
                briefPanel.gameObject.SetActive(true);
                panelTransform.sizeDelta = new Vector3(panelTransform.sizeDelta.x, heights[2]);
                contentTransform.sizeDelta = new Vector3(contentTransform.sizeDelta.x, heights[2]);
            }
            return procBuilding;
        }


        /// <summary>
        /// Called when a building is removed
        /// </summary>
        /// <param name="index">The index of the building</param>
        /// <param name="loaded">Whether the building was loaded or not</param>
        public void OnBuildingRemoved(int index, bool loaded = false)
        {
            buildingManager.DeleteBuilding(index);

            buildings.RemoveAt(index);
            if (buildings.Count == 0)
            {
                hasSelected = false;
                instructionText.gameObject.SetActive(true);
                geometryPanel.SetActive(false);
                firstSeparator.SetActive(false);
                floorPanel.gameObject.SetActive(false);
                secondSeparator.gameObject.SetActive(false);
                thirdSeparator.SetActive(false);
                briefPanel.gameObject.SetActive(false);
                customPolygonButton.interactable = false;
                coreDropdown.SetActive(false);
                typologyDropdown.SetActive(false);
                panelTransform.sizeDelta = new Vector3(panelTransform.sizeDelta.x, heights[0]);
                contentTransform.sizeDelta = new Vector3(contentTransform.sizeDelta.x, heights[0]);
            }
            floorPanel.ClearPrevious();
        }

        /// <summary>
        /// Called when a brief element is changed
        /// </summary>
        /// <param name="sender">The brief panel whose element changed</param>
        public void OnBriefElementChanged(int floorIndex, Dictionary<string, float> percentages)
        {
            CheckForStacking();
        }

        public void CheckToPlace()
        {
            if (currentBuilding != null && currentBuilding.buildingType == (int)BuildingType.Tower)
            {
                bool a = currentBuilding.floorHeights != null && currentBuilding.floorHeights.Count > 0 && currentBuilding.floorHeights[floorIndex] != -1;
                bool b = currentBuilding.levels != null && currentBuilding.levels.Count > 0 && currentBuilding.levels[floorIndex] != null && currentBuilding.levels[floorIndex].Count > 0;
                bool c = buildingPanel.currentElement.proceduralBuilding.floors.Count >= 2;
                //bool c = currentBuilding.percentages != null && currentBuilding.percentages.Count > 0 && currentBuilding.percentages[floorIndex] != null && currentBuilding.percentages[floorIndex].Count > 0;
                //bool d = currentBuilding.percentages.Count == currentBuilding.levels.Count;
                //bool e = false;
                //if (currentBuilding.levels.Count > 0 && currentBuilding.levels[0] != null)
                //{
                //    e = currentBuilding.levels[0].Contains(0) && currentBuilding.levels[0].Count != 1;
                //}
                float offsetDist;
                bool f = !String.IsNullOrEmpty(offsetDistance.text) && float.TryParse(offsetDistance.text, out offsetDist);
                bool g = towerFloorPanel.CheckLevelOverlap();
                //float corridorWidth;
                //bool h = !String.IsNullOrEmpty(corridorWidthField.text) && float.TryParse(corridorWidthField.text, out corridorWidth);

                placeButton.interactable = a && b && c && f && !g;
                //placeButtonTooltip.gameObject.SetActive(!generateButton.interactable);
                placeButtonTooltip.warningMessage = "";
                //placeButtonTooltip.color = UnityEngine.Color.red;

                stringBuilder = new StringBuilder();

                if (!a)
                {
                    stringBuilder.AppendLine("The floor to floor height needs to be set! (ex. 3)");
                }

                if (!b)
                {
                    stringBuilder.AppendLine("The list of levels needs to be set! (ex. 0-1)");
                }

                if (!c)
                {
                    stringBuilder.AppendLine("You need to add at least one more floor other than the Ground Floor!");
                }

                //if (!d)
                //{
                //    generateButtonTooltip.text += "All floors need to have a brief!\r\n\r\n";
                //}

                //if (e)
                //{
                //    generateButtonTooltip.text += "The ground level (0) must be on its own floor layout!\r\n\r\n";
                //}

                if (!f)
                {
                    stringBuilder.AppendLine("The apartment depth must be set (ex. 6.8 m)!");
                }

                if (g)
                {
                    stringBuilder.AppendLine("There is overlap between the levels of different floors!");
                }

                //if (!h)
                //{
                //    generateButtonTooltip.text += "The corridor width must be set (ex. 1.2 m)!\r\n\r\n";
                //}

                placeButtonTooltip.warningMessage = stringBuilder.ToString();
            }
        }

        public void CheckToGenerate()
        {
            if (currentBuilding != null && currentBuilding.buildingType == (int)BuildingType.Linear)
            {
                bool a = currentBuilding.floorHeights != null && currentBuilding.floorHeights.Count > 0 && currentBuilding.floorHeights[floorIndex] != -1;
                bool b = currentBuilding.levels != null && currentBuilding.levels.Count > 0 && currentBuilding.levels[floorIndex] != null && currentBuilding.levels[floorIndex].Count > 0;
                bool c = currentBuilding.percentages != null && currentBuilding.percentages.Count > 0 && currentBuilding.percentages[floorIndex] != null && currentBuilding.percentages[floorIndex].Count > 0;
                bool d = currentBuilding.percentages.Count == currentBuilding.levels.Count;
                bool e = false;
                if (currentBuilding.levels.Count > 0 && currentBuilding.levels[0] != null)
                {
                    e = currentBuilding.levels[0].Contains(0) && currentBuilding.levels[0].Count != 1;
                }
                float offsetDist;
                bool f = !String.IsNullOrEmpty(offsetDistance.text) && float.TryParse(offsetDistance.text, out offsetDist);
                bool g = floorPanel.CheckLevelOverlap();
                float corridorWidth;
                bool h = !String.IsNullOrEmpty(corridorWidthField.text) && float.TryParse(corridorWidthField.text, out corridorWidth);
                bool i = CheckPercentages();

                generateButton.interactable = a && b && c && d && !e && f && !g && h && !i;
                //generateButtonTooltip.gameObject.SetActive(!generateButton.interactable);
                generateButtonTooltip.warningMessage = "";
                //generateButtonTooltip.color = UnityEngine.Color.red;
                stringBuilder = new StringBuilder();

                if (!a)
                {
                    /*generateButtonTooltip.text +=*/
                    stringBuilder.AppendLine("The floor to floor height needs to be set! (ex. 3)");
                }

                if (!b)
                {
                    /*generateButtonTooltip.text += */
                    stringBuilder.AppendLine("The list of levels needs to be set! (ex. 0-1)");
                }

                if (!c)
                {
                    /*generateButtonTooltip.text += */
                    stringBuilder.AppendLine("The list of brief percentages needs to be set! (ex. 100)");
                }

                if (!d)
                {
                    /*generateButtonTooltip.text += */
                    stringBuilder.AppendLine("All floors need to have a brief!");
                }

                if (e)
                {
                    /*generateButtonTooltip.text += */
                    stringBuilder.AppendLine("The ground level (0) must be on its own floor layout!");
                }

                if (!f)
                {
                    /*generateButtonTooltip.text += */
                    stringBuilder.AppendLine("The apartment depth must be set (ex. 6.8 m)!");
                }

                if (g)
                {
                    stringBuilder.AppendLine("There is overlap between the levels of different floors!");
                }

                if (!h)
                {
                    stringBuilder.AppendLine("The corridor width must be set (ex. 1.2 m)!");
                }

                if (i)
                {
                    stringBuilder.AppendLine("The percentages of one of your floors don't add up to 100%!");
                }

                generateButtonTooltip.warningMessage = stringBuilder.ToString();
            }
        }
        #endregion

        #region Private Methods
        private bool CheckPercentages()
        {
            bool issue = false;

            for (int j = 0; j < currentBuilding.percentages.Count; j++)
            {
                float total = 0;
                foreach (var item in currentBuilding.percentages[j])
                {
                    total += item.Value;
                }
                issue = total != 100;
            }

            return issue;
        }
        private void CheckForStacking()
        {
            bool issue = false;

            for (int i = 2; i < buildings[buildingIndex].percentages.Count; i++)
            {
                if (buildings[buildingIndex].percentages[i].Count != buildings[buildingIndex].percentages[i - 1].Count)
                {
                    issue = true;
                }
                else
                {
                    foreach (var item in buildings[buildingIndex].percentages[i])
                    {
                        if (!buildings[buildingIndex].percentages[i - 1].ContainsKey(item.Key))
                        {
                            issue = true;
                        }
                        else
                        {
                            if (buildings[buildingIndex].percentages[i - 1][item.Key] != item.Value)
                            {
                                issue = true;
                            }
                        }
                    }
                }
            }

            if (issue)
            {
                if (!Notifications.TaggedObject.activeNotifications.Contains("Stacking"))
                {
                    Notifications.TaggedObject.activeNotifications.Add("Stacking");
                }
            }
            else
            {
                if (Notifications.TaggedObject.activeNotifications.Contains("Stacking"))
                {
                    Notifications.TaggedObject.activeNotifications.Remove("Stacking");
                }
            }
            Notifications.TaggedObject.UpdateNotifications();
        }

        private IEnumerator AddGroundFloor()
        {
            yield return waitFrame;
            //floorPanel.AddFloorElement(new List<int>() { 0 }, 4.5f, true);
            briefPanel.AddBriefElement("Commercial", 100);
        }
        #endregion
    }

    /// <summary>
    /// A class for storing building information
    /// </summary>
    [System.Serializable]
    public class BuildingData
    {
        public string name;
        public List<List<int>> levels;
        public List<Dictionary<string, float>> percentages;
        public List<float> floorHeights;
        public List<float> heightPerLevel;
        public int coreAllignment;
        public int linearTypology;
        public int buildingType;
        public int exteriorBalconies;
        public float apartmentDepth;
        public float corridorWidth;
        public List<string[]> aptTypesTower;

        public BuildingData()
        {
            levels = new List<List<int>>();
            percentages = new List<Dictionary<string, float>>();
            floorHeights = new List<float>();
            heightPerLevel = new List<float>();
            aptTypesTower = new List<string[]>();
            coreAllignment = 0;
        }
    }

}
