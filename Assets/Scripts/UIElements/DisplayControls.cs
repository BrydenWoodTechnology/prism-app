﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BrydenWoodUnity.DesignData;
using BrydenWoodUnity.GeometryManipulation;
using System;

namespace BrydenWoodUnity.UIElements
{
    public class DisplayControls : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("SCene References:")]
        public Button buildingsButton;
        public Button floorsButton;
        public Button apartmentsButton;
        public Toggle populateInteriorToggle;
        public Dropdown layoutDropDown;
        public Text layoutText;
        public GameObject apartmentTypeStats;
        public Toggle moveColinear;
        public Camera maxCamera;
        public Camera orthoCamera;

        public PreviewMode currentPreview { get; private set; }
        #endregion

        #region Events
        /// <summary>
        /// Used when the preview mode has changed
        /// </summary>
        /// <param name="mode">The new preview mode</param>
        public delegate void OnPreviewChanged(PreviewMode mode);
        /// <summary>
        /// Triggered when the preview mode has changed
        /// </summary>
        public static event OnPreviewChanged previewChanged;

        private void ChangePreview()
        {
            if (previewChanged != null)
            {
                previewChanged(currentPreview);
            }
        }

        /// <summary>
        /// Used when the dropdown of the apartment type changes
        /// </summary>
        /// <param name="index">The new value of the dropdown</param>
        public delegate void OnDropDownChanged(int index);
        /// <summary>
        /// Triggered when the dropdown of the apartment type changes
        /// </summary>
        public static event OnDropDownChanged layoutChanged;
        /// <summary>
        /// Called when a new value for the apartment type dropdown is set
        /// </summary>
        /// <param name="index">The new value of the dropdown</param>
        public void LayoutChange(int index)
        {
            if (layoutChanged != null)
            {
                layoutChanged(index);
            }
        }

        /// <summary>
        /// Used when the colinear mode is toggled
        /// </summary>
        /// <param name="colinear">On or Off</param>
        public delegate void OnColinearChanged(bool colinear);
        /// <summary>
        /// Triggered when the colinear mode is toggled
        /// </summary>
        public static event OnColinearChanged colinearChanged;
        public void ColinearChanged(bool colinear)
        {
            if (colinearChanged != null)
            {
                colinearChanged(colinear);
            }
        }
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Awake()
        {
            OnPreviewChange(PreviewMode.Buildings);
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnDestroy()
        {
            if (colinearChanged != null)
            {
                foreach (Delegate eh in colinearChanged.GetInvocationList())
                {
                    colinearChanged -= (OnColinearChanged)eh;
                }
            }
            if (layoutChanged != null)
            {
                foreach (Delegate eh in layoutChanged.GetInvocationList())
                {
                    layoutChanged -= (OnDropDownChanged)eh;
                }
            }
            if (previewChanged != null)
            {
                foreach (Delegate eh in previewChanged.GetInvocationList())
                {
                    previewChanged -= (OnPreviewChanged)eh;
                }
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when the buildings preview mode is selected
        /// </summary>
        public void OnBuildingsClicked()
        {
            OnPreviewChange(PreviewMode.Buildings);
        }

        /// <summary>
        /// Called when the floors preview mode is selected
        /// </summary>
        public void OnFloorsClicked()
        {
            OnPreviewChange(PreviewMode.Floors);
        }

        /// <summary>
        /// Called when the apartments preview mode is selected
        /// </summary>
        public void OnApartmentsClicked()
        {
            OnPreviewChange(PreviewMode.Apartments);
        }
        #endregion

        #region Private Methods
        private void OnPreviewChange(PreviewMode mode)
        {
            currentPreview = mode;
            ChangePreview();
            PreviewControl();
        }

        private void PreviewControl()
        {
            switch (currentPreview)
            {
                case PreviewMode.Apartments:
                    layoutDropDown.gameObject.SetActive(true);
                    populateInteriorToggle.gameObject.SetActive(false);
                    layoutText.gameObject.SetActive(true);
                    buildingsButton.targetGraphic.color = buildingsButton.colors.normalColor;
                    floorsButton.targetGraphic.color = floorsButton.colors.normalColor;
                    apartmentsButton.targetGraphic.color = apartmentsButton.colors.disabledColor;
                    orthoCamera.gameObject.SetActive(true);
                    orthoCamera.gameObject.tag = "MainCamera";
                    maxCamera.gameObject.SetActive(false);
                    maxCamera.gameObject.tag = "Untagged";
                    //apartmentTypeStats.SetActive(false);
                    break;
                case PreviewMode.Buildings:
                    layoutDropDown.gameObject.SetActive(false);
                    populateInteriorToggle.gameObject.SetActive(true);
                    layoutText.gameObject.SetActive(false);
                    buildingsButton.targetGraphic.color = buildingsButton.colors.disabledColor;
                    floorsButton.targetGraphic.color = floorsButton.colors.normalColor;
                    apartmentsButton.targetGraphic.color = apartmentsButton.colors.normalColor;
                    orthoCamera.gameObject.SetActive(false);
                    orthoCamera.gameObject.tag = "Untagged";
                    maxCamera.gameObject.SetActive(true);
                    maxCamera.gameObject.tag = "MainCamera";
                    //maxCamera.clearFlags = CameraClearFlags.Skybox;
                    maxCamera.clearFlags = CameraClearFlags.SolidColor;
                    //apartmentTypeStats.SetActive(false);
                    break;
                case PreviewMode.Floors:
                    layoutDropDown.gameObject.SetActive(false);
                    populateInteriorToggle.gameObject.SetActive(true);
                    layoutText.gameObject.SetActive(false);
                    buildingsButton.targetGraphic.color = buildingsButton.colors.normalColor;
                    floorsButton.targetGraphic.color = floorsButton.colors.disabledColor;
                    apartmentsButton.targetGraphic.color = apartmentsButton.colors.normalColor;
                    orthoCamera.gameObject.SetActive(false);
                    orthoCamera.gameObject.tag = "Untagged";
                    maxCamera.gameObject.SetActive(true);
                    maxCamera.gameObject.tag = "MainCamera";
                    maxCamera.clearFlags = CameraClearFlags.SolidColor;
                    //apartmentTypeStats.SetActive(true);
                    break;
            }
        }
        #endregion
    }
}
