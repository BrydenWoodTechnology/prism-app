﻿using BrydenWoodUnity.DesignData;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{

    /// <summary>
    /// A MonoBehaviour component for a Floor UI Element
    /// </summary>
    public class FloorElement : MonoBehaviour, ICancelable
    {
        #region Public Fields and Methods
        [Header("Scene References:")]
        public InputField levelsField;
        public InputField floorHeightField;
        public Button removeButton;
        public Dropdown[] aptTypesDropdowns;
        public ProceduralFloor proceduralFloor;
        public FloorPanel floorPanel;
        public OnInputSubmitted valueSubmitted;
        public string[] aptTypes { get; set; }

        public List<int> levels { get; set; }
        [HideInInspector]
        public float floorHeight = -1;
        #endregion

        private string prevValue;

        #region Events
        /// <summary>
        /// Used when the element is selected
        /// </summary>
        /// <param name="sender">The selected element</param>
        public delegate void OnSelected(FloorElement sender);
        /// <summary>
        /// Triggered when the element is selected
        /// </summary>
        public static event OnSelected selected;
        /// <summary>
        /// Triggered when the element has changed
        /// </summary>
        public static event OnSelected changed;
        /// <summary>
        /// Triggered when the element has been deleted
        /// </summary>
        public static event OnSelected deleted;
        #endregion

        #region MonoBehaviour Methods
        private void OnDestroy()
        {
        }

        // Use this for initialization
        void Awake()
        {

            levelsField.GetComponent<InputFieldExtension>().selected += OnInputFieldSelected;
            floorHeightField.GetComponent<InputFieldExtension>().selected += OnInputFieldSelected;
            if (aptTypesDropdowns != null && aptTypesDropdowns.Length != 0)
            {
                aptTypes = new string[aptTypesDropdowns.Length];
                for (int i = 0; i < aptTypesDropdowns.Length; i++)
                {
                    if (aptTypesDropdowns[i] != null)
                    {
                        aptTypesDropdowns[i].GetComponent<DropdownExtension>().selected += OnDropDownSelected;
                        aptTypes[i] = aptTypesDropdowns[i].options[aptTypesDropdowns[i].value].text;
                    }
                }
            }
            levels = null;
            floorHeight = -1;
            GetComponent<Image>().color = GetComponent<Toggle>().colors.pressedColor;
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when the levels of the element have changed
        /// </summary>
        /// <param name="value">The levels as a string</param>
        public void OnLevelsChanged(string value)
        {
            int originalTop = levels != null && levels.Count > 0 ? levels[levels.Count - 1] : -1;

            if (!String.IsNullOrEmpty(value))
            {
                levels = new List<int>();
                var cells = value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < cells.Length; i++)
                {
                    try
                    {
                        if (cells[i].Contains("-"))
                        {
                            var ends = cells[i].Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToList();
                            for (int j = ends[0]; j <= ends[1]; j++)
                            {
                                levels.Add(j);
                            }
                            if (levels.Last() < levels[0])
                            {
                                throw new Exception("Wrong input!");
                            }
                        }
                        else
                        {
                            levels.Add(int.Parse(cells[i]));
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("Invalid Input for Floor Levels: " + e);
                        levels = null;
                    }
                }
            }
            else
            {
                Debug.LogError("Invalid Input for Floor Levels");
                levels = null;
            }

            if (aptTypesDropdowns != null && aptTypesDropdowns.Length > 0)
            {

                bool maxLevelIssue = floorPanel.CheckForMaxLevel(originalTop != -1 ? levels[levels.Count - 1] - originalTop : 0);

                if (maxLevelIssue)
                {
                    RefreshBuildingPopUp.TaggedObject.AddChangedValue(this, new string[] { prevValue, value });
                }
                else
                {
                    if (changed != null)
                    {
                        changed(this);
                    }
                    if (levels != null)
                    {
                        proceduralFloor.UpdateLevels(floorHeight, levels.ToArray());
                    }
                }
            }
            else
            {

                if (changed != null)
                {
                    changed(this);
                }
                if (levels != null)
                {
                    proceduralFloor.UpdateLevels(floorHeight, levels.ToArray());
                }
            }
        }

        /// <summary>
        /// Returns the delta between the first and the last level
        /// </summary>
        /// <returns>Int</returns>
        public int GetLevelsDelta()
        {
            return levels.Last() - levels[0];
        }

        /// <summary>
        /// Sets this level as the ground floor
        /// </summary>
        public void SetAsGroundFloor()
        {
            levelsField.interactable = false;
            removeButton.interactable = false;
            if (aptTypesDropdowns != null)
            {
                for (int i = 0; i < aptTypesDropdowns.Length; i++)
                {
                    if (aptTypesDropdowns[i] != null)
                    {
                        aptTypesDropdowns[i].options = new List<Dropdown.OptionData>();
                        aptTypesDropdowns[i].RefreshShownValue();
                        aptTypesDropdowns[i].interactable = false;
                    }
                }
            }
        }

        /// <summary>
        /// Called when the Floor to Floor height of the element has changed
        /// </summary>
        /// <param name="value">The floor to floor height as string</param>
        public void OnFloorHeightChanged(string value)
        {
            if (!String.IsNullOrEmpty(value))
            {
                if (float.TryParse(value, out floorHeight))
                {

                }
                else
                {
                    Debug.LogError("Invalid Input for Floor to Floor Height");
                    floorHeight = -1;
                }
            }
            else
            {
                Debug.LogError("Invalid Input for Floor to Floor Height");
                floorHeight = -1;
            }

            if (changed != null)
            {
                changed(this);
            }
            if (proceduralFloor != null && levels != null)
            {
                proceduralFloor.UpdateLevels(floorHeight, levels.ToArray());
            }
        }

        /// <summary>
        /// Called by the Unity UI when one of the apartments changes types (Tower only)
        /// </summary>
        /// <param name="sender">The dropdown of the specific apartment</param>
        public void OnDropdownChanged(Dropdown sender)
        {
            if (aptTypesDropdowns != null)
            {
                for (int i = 0; i < aptTypesDropdowns.Length; i++)
                {
                    if (aptTypesDropdowns[i] == sender)
                    {
                        aptTypes[i] = sender.options[sender.value].text;
                    }
                }
                var tower = proceduralFloor as ProceduralTower;
                if (tower != null)
                {

                    tower.SetApartmentTypes(aptTypes, GetTowerPercentages(aptTypes));
                    if (changed != null)
                    {
                        changed(this);
                    }
                }
            }
        }

        /// <summary>
        /// Returns the brief percentages based on a given list of apartment types
        /// </summary>
        /// <param name="types">The list of apartment types</param>
        /// <returns>String - Float Dictionary</returns>
        public static Dictionary<string, float> GetTowerPercentages(string[] types)
        {
            if (types != null)
            {
                Dictionary<string, float> percentages = new Dictionary<string, float>();

                for (int i = 0; i < types.Length; i++)
                {
                    if (!percentages.ContainsKey(types[i]))
                    {
                        percentages.Add(types[i], 1.0f / types.Length);
                    }
                    else
                    {
                        percentages[types[i]] += 1.0f / types.Length;
                    }
                }

                return percentages;
            }
            else return new Dictionary<string, float>() { { "Commercial", 100.0f } };
        }

        /// <summary>
        /// Called when an element is being deleted
        /// </summary>
        public void OnDelete()
        {
            if (deleted != null)
            {
                deleted(this);
            }

            proceduralFloor.Delete();
        }

        /// <summary>
        /// Called when the element is deselected
        /// </summary>
        public void Deselect()
        {
            GetComponent<Image>().color = GetComponent<Toggle>().colors.normalColor;
            GetComponent<Toggle>().SetValue(false);
        }

        /// <summary>
        /// Sets the values of the element
        /// </summary>
        /// <param name="levels">The new levels of the element</param>
        /// <param name="floorHeight">The new floor to floor height of the element</param>
        public void SetValues(List<int> levels, float floorHeight, string[] types = null)
        {
            if (levels != null && levels.Count > 0)
            {
                if (levels.Count > 1)
                {
                    levelsField.text = string.Format("{0}-{1}", levels[0], levels.Last());
                }
                else
                {
                    levelsField.text = levels[0].ToString();
                }
            }
            floorHeightField.text = floorHeight.ToString();
            this.levels = levels;
            this.floorHeight = floorHeight;
            if (types != null && aptTypesDropdowns != null && aptTypesDropdowns.Length > 0)
            {
                if (aptTypes == null)
                {
                    aptTypes = new string[types.Length];
                }
                for (int i = 0; i < types.Length; i++)
                {
                    if (aptTypesDropdowns[i] != null)
                    {
                        aptTypes[i] = types[i];
                        for (int j = 0; j < aptTypesDropdowns[i].options.Count; j++)
                        {
                            if (aptTypesDropdowns[i].options[j].text == types[i])
                            {
                                aptTypesDropdowns[i].SetValue(j);
                                break;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Loads the values of a floor element
        /// </summary>
        /// <param name="levels">The levels to be loaded</param>
        /// <param name="floorHeight">The floor to floor height to be loaded</param>
        /// <param name="types">The list of apartment types to be loaded (Null by default)</param>
        public void LoadValues(List<int> levels, float floorHeight, string[] types = null)
        {
            if (levels.Count > 1)
            {
                levelsField.SetValue(string.Format("{0}-{1}", levels[0], levels.Last()));
            }
            else
            {
                levelsField.SetValue(levels[0].ToString());
            }
            floorHeightField.SetValue(floorHeight.ToString());
            this.levels = levels;
            this.floorHeight = floorHeight;
            if (types != null && aptTypesDropdowns != null && aptTypesDropdowns.Length > 0)
            {
                for (int i = 0; i < types.Length; i++)
                {
                    if (aptTypesDropdowns[i] != null)
                    {
                        aptTypes[i] = types[i];
                        for (int j = 0; j < aptTypesDropdowns[i].options.Count; j++)
                        {
                            if (aptTypesDropdowns[i].options[j].text == types[i])
                            {
                                aptTypesDropdowns[i].SetValue(j);
                                break;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sets the levels of the elements and triggers an event
        /// </summary>
        /// <param name="levels">The new levels of the elements</param>
        public void SetLevelsWithEvent(List<int> levels)
        {
            if (levels.Count > 1)
            {
                levelsField.text = string.Format("{0}-{1}", levels[0], levels.Last());
            }
            else
            {
                levelsField.text = levels[0].ToString();
            }
            this.levels = levels;
            if (changed != null)
            {
                changed(this);
            }
            proceduralFloor.UpdateLevels(floorHeight, levels.ToArray());
        }

        /// <summary>
        /// Toggles the Remove button of the element
        /// </summary>
        /// <param name="toggle">Whether the button should be interactable</param>
        public void ToggleRemoveButton(bool toggle)
        {
            removeButton.interactable = toggle;
        }

        /// <summary>
        /// Called when the input field is selected
        /// </summary>
        /// <param name="inputField">The selected inputfield</param>
        public void OnInputFieldSelected(InputField inputField)
        {
            //GetComponent<Toggle>().isOn = true;
            Selected(true);
        }

        /// <summary>
        /// Called when one of the dropdowns of the element is selected
        /// </summary>
        /// <param name="dropdown">The selected dropdown</param>
        public void OnDropDownSelected(Dropdown dropdown)
        {
            //GetComponent<Toggle>().isOn = true;
            Selected(true);
        }

        /// <summary>
        /// Called when the element is selected
        /// </summary>
        /// <param name="isSelected">Whether the element is selected or not</param>
        public void Selected(bool isSelected)
        {
            if (isSelected)
            {
                if (selected != null)
                {
                    selected(this);
                }
                proceduralFloor.Select();
                GetComponent<Image>().color = GetComponent<Toggle>().colors.pressedColor;
                GetComponent<Toggle>().isOn = isSelected;
            }
            else
            {
                GetComponent<Toggle>().isOn = isSelected;
                GetComponent<Image>().color = GetComponent<Toggle>().colors.normalColor;
                proceduralFloor.DeSelect();
            }
        }

        public void ResetValue(string prevValue)
        {
            levelsField.SetValue(prevValue);
            this.prevValue = prevValue;
        }

        public void SubmitValue(string currentValue)
        {
            if (!String.IsNullOrEmpty(currentValue))
            {
                prevValue = currentValue;
                levels = new List<int>();
                var cells = currentValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < cells.Length; i++)
                {
                    try
                    {
                        if (cells[i].Contains("-"))
                        {
                            var ends = cells[i].Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToList();
                            for (int j = ends[0]; j <= ends[1]; j++)
                            {
                                levels.Add(j);
                            }
                            if (levels.Last() < levels[0])
                            {
                                throw new Exception("Wrong input!");
                            }
                        }
                        else
                        {
                            levels.Add(int.Parse(cells[i]));
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("Invalid Input for Floor Levels: " + e);
                        levels = null;
                    }
                }
            }
            else
            {
                Debug.LogError("Invalid Input for Floor Levels");
                levels = null;
            }

            if (changed != null)
            {
                changed(this);
            }

            proceduralFloor.UpdateLevels(floorHeight, levels.ToArray());
        }
        #endregion
    }
}
