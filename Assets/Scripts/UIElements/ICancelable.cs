﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// An interface for controlling whether changes in values should be canceled or accepted
    /// </summary>
    public interface ICancelable
    {
        void ResetValue(string prevValue);

        void SubmitValue(string currentValue);
    }
}
