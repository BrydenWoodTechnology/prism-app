﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for an interactive plot point
    /// </summary>
    public class InteractivePlotPoint : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        #region Public Fields and Properties
        public Text info;
        #endregion

        #region Private Fields and Properties
        private Image image;
        private RectTransform rectTransform;
        private Vector2 origSize;
        #endregion

        #region Events
        /// <summary>
        /// Used when one of the plot points is interacted
        /// </summary>
        /// <param name="sender">The interacted point</param>
        public delegate void OnPointInteracted(InteractivePlotPoint sender);
        /// <summary>
        /// Triggered when the pointer enters over the point
        /// </summary>
        public event OnPointInteracted pointerEnter;
        /// <summary>
        /// Triggered when the pointer exits over the point
        /// </summary>
        public event OnPointInteracted pointerExited;
        /// <summary>
        /// Triggered when the pointer clicks over the point
        /// </summary>
        public event OnPointInteracted pointerClocked;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region PUblic Methods
        /// <summary>
        /// Initializes the instance
        /// </summary>
        /// <param name="x">The x coordinate of the point</param>
        /// <param name="y">The y coordinate of the point</param>
        /// <param name="size">The size of the point</param>
        /// <param name="c">The color of the point</param>
        public void Initialize(int x, int y, float size, Color c)
        {
            image = GetComponent<Image>();
            rectTransform = GetComponent<RectTransform>();
            rectTransform.localPosition = new Vector3(x, y);
            rectTransform.sizeDelta = rectTransform.sizeDelta * size;
            origSize = rectTransform.sizeDelta;
            image.color = c;
        }
        #endregion

        #region Interface Methods
        public void OnPointerClick(PointerEventData eventData)
        {
            
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            rectTransform.sizeDelta = origSize * 1.2f;
            info.gameObject.SetActive(true);
            if (pointerEnter != null)
            {
                pointerEnter(this);
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            rectTransform.sizeDelta = origSize;
            info.gameObject.SetActive(false);
            if (pointerExited != null)
            {
                pointerExited(this);
            }
        }
        #endregion
    }
}
