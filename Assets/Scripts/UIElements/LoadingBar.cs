﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    public class LoadingBar : Tagged<LoadingBar>
    {

        public GameObject[] contents;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void Enable(bool on)
        {
            for (int i = 0; i < contents.Length; i++)
            {
                contents[i].SetActive(on);
            }
        }

        public void SetValue(string description, float value)
        {
            contents[0].GetComponent<Text>().text = description;
            contents[1].GetComponent<Slider>().value = value;
        }
    }
}
