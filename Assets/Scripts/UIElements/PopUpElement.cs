﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for a pop-up UI element
    /// </summary>
    public class PopUpElement : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Referenced Elements")]
        [Space(5)]
        public RectTransform popper;
        public Transform popper_content;
        public Toggle this_toggle;
        [Space(10)]
        [Header("UI Elements")]
        [Space(5)]
        [Tooltip("Define pop-up window color")]
        public Color32 color_start = new Color32();
        public int counter;
        public string routine_state;
        public bool delay_activated;
        #endregion

        #region Private Fields and Methods
        private bool tog_state { get; set; }
        private WaitForSeconds delay = new WaitForSeconds(0.35f);
        private int lastcounter { get; set; }
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            tog_state = false;
            lastcounter = 1;
            color_start = popper.GetComponent<Image>().color;

            if (popper_content == null)
            {

            }
            else
            {
                popper_content.gameObject.SetActive(false);
            }

        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Toggles the delay
        /// </summary>
        /// <param name="delay_bool">On or Off</param>
        public void DelayEnabled(bool delay_bool)
        {
            delay_activated = !delay_bool;
            Debug.Log("delay is: " + !delay_bool);
        }

        /// <summary>
        /// Starts the movement
        /// </summary>
        /// <param name="mytog">The toggle which enables the movement</param>
        public void StartPopping(Toggle mytog)
        {
            tog_state = !tog_state;
            StartCoroutine(SmoothPop(popper, 1f, tog_state));
        }

        /// <summary>
        /// Starts a smooth movement
        /// </summary>
        /// <param name="pop">The Rect Transform of the element</param>
        /// <param name="seconds">The seconds required for the movement</param>
        /// <param name="activeState">On or Off</param>
        /// <returns>IEnumerator</returns>
        public IEnumerator SmoothPop(RectTransform pop, float seconds, bool activeState)
        {
            float elapsedTime = 0;
            lastcounter = counter;
            Image image = pop.GetComponent<Image>();

            color_start = image.color;
            //Debug.Log(color_start.ToString());

            //float start_a = color_start.a;
            float start_a;
            float end_a;
            if (activeState == true)
            {
                pop.gameObject.SetActive(true);
                start_a = 0;
                end_a = 220;

                while (elapsedTime < seconds)
                {
                    float a_now = (Mathf.Lerp(start_a, end_a, (elapsedTime / seconds)));
                    image.color = new Color32(color_start.r, color_start.g, color_start.b, (byte)a_now);
                    elapsedTime += 0.05f;
                    routine_state = "true";

                    if (elapsedTime >= seconds)
                    {
                        image.color = new Color32(color_start.r, color_start.g, color_start.b, (byte)end_a);
                        popper_content.gameObject.SetActive(true);
                    }
                    if (counter != lastcounter)
                    {
                        //Debug.Log("STOP ME PLEASE");
                        image.color = new Color32(color_start.r, color_start.g, color_start.b, (byte)start_a);
                        popper_content.gameObject.SetActive(false);
                        this_toggle.isOn = false;
                        yield break;
                    }

                    yield return null;
                }
            }
            if (activeState == false)
            {
                start_a = color_start.a;
                end_a = 0;
                popper_content.gameObject.SetActive(false);

                yield return delay;
                while (elapsedTime < seconds)
                {
                    float a_now = (Mathf.Lerp(start_a, end_a, (elapsedTime / seconds)));
                    image.color = new Color32(color_start.r, color_start.g, color_start.b, (byte)a_now);
                    elapsedTime += 0.07f;
                    if (elapsedTime >= seconds)
                    {
                        image.color = new Color32(color_start.r, color_start.g, color_start.b, (byte)end_a);
                        pop.gameObject.SetActive(false);
                    }

                    if (counter != lastcounter)
                    {
                        //Debug.Log("STOP ME PLEASE");
                        image.color = new Color32(color_start.r, color_start.g, color_start.b, (byte)end_a);
                        popper_content.gameObject.SetActive(false);
                        this_toggle.isOn = false;
                        yield break;
                    }

                    yield return null;
                }
            }

        }

        #endregion
    }
}
