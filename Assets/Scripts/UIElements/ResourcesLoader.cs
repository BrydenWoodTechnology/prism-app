﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.InteropServices;
using BrydenWoodUnity.DesignData;
using System.Windows.Forms;
using Newtonsoft.Json;
using UnityEngine.SceneManagement;
using System;

namespace BrydenWoodUnity
{
    /// <summary>
    /// Different locationsto load the resources from
    /// </summary>
    public enum ResourcesLocation
    {
        GetFromUI,
        GetFromURL,
        GetFromStreamingAssets
    }

    /// <summary>
    /// A MonoBehaviour component from loading files and resources
    /// </summary>
    public class ResourcesLoader : Tagged<ResourcesLoader>
    {

        #region WebGL Methods
        [DllImport("__Internal")]
        private static extern string GetURLFromPage();

        [DllImport("__Internal")]
        public static extern void DownloadText(string data, string fileName, string type);

        [DllImport("__Internal")]
        private static extern void TextUploaderCaptureClick(string objectName, string methodName, string fileName);

        [DllImport("__Internal")]
        private static extern void DisplayNotification(string notification);

        [DllImport("__Internal")]
        private static extern void DisableMiddleMouseScroll();

        [DllImport("__Internal")]
        private static extern void ImportPDFLibrary();

        [DllImport("__Internal")]
        private static extern void ExportPdfReport(string data, string fileName);

        [DllImport("__Internal")]
        private static extern void DownloadFile(byte[] array, int bytLength, string fileName);
        #endregion

        #region Public Fields and Properties
        public ResourcesLocation resourcesLocation = ResourcesLocation.GetFromURL;
        public string contextData { get; set; }
        public string saveData { get; set; }
        #endregion

        #region Private Fields and Properties
        private OpenFileDialog ofd;
        private SaveFileDialog sfd;
        private bool stillLoading = true;
        private bool waitingData = true;
        #endregion

        #region Events
        /// <summary>
        /// Used when the requested data have been loaded
        /// </summary>
        public delegate void OnLoadedData();
        /// <summary>
        /// Triggered when the requested data have been loaded
        /// </summary>
        public static event OnLoadedData dataLoaded;
        /// <summary>
        /// Triggered when the site-related data have been loaded
        /// </summary>
        public static event OnLoadedData contextDataLoaded;
        #endregion

        #region MonoBehaviour Methods
        private void OnDestroy()
        {
            if (dataLoaded != null)
            {
                foreach (Delegate eh in dataLoaded.GetInvocationList())
                {
                    dataLoaded -= (OnLoadedData)eh;
                }
            }
            if (contextDataLoaded != null)
            {
                foreach (Delegate eh in contextDataLoaded.GetInvocationList())
                {
                    contextDataLoaded -= (OnLoadedData)eh;
                }
            }
        }

        // Use this for initialization
        void Start()
        {

            //DontDestroyOnLoad(gameObject);
            //dataLoaded += LoadScene;
#if UNITY_EDITOR
            resourcesLocation = ResourcesLocation.GetFromStreamingAssets;
            ofd = new OpenFileDialog();
            sfd = new SaveFileDialog();
#elif UNITY_STANDALONE_WIN
        resourcesLocation = ResourcesLocation.GetFromStreamingAssets;
        ofd = new OpenFileDialog();
        sfd = new SaveFileDialog();
#elif UNITY_WEBGL
        ImportPDFLibrary();
        resourcesLocation = ResourcesLocation.GetFromURL;
        DisableMiddleMouseScroll();
        switch (resourcesLocation)
        {
            case ResourcesLocation.GetFromURL:

                break;
            case ResourcesLocation.GetFromStreamingAssets:
                ofd = new OpenFileDialog();
                sfd = new SaveFileDialog();
                break;
        }
#endif
            GameObject shouldLoadFiles = GameObject.Find("ShouldLoadFiles");
            if (shouldLoadFiles != null)
            {
                Destroy(shouldLoadFiles);
                SetSaveFiles();
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (waitingData)
            {
                if (!String.IsNullOrEmpty(saveData))
                {
                    if (dataLoaded != null)
                    {
                        dataLoaded();
                    }
                    waitingData = false;
                }
            }
        }
        #endregion

        #region Public Methods
        public void ExportScreenShot(byte[] imgData)
        {
            
#if UNITY_EDITOR
            sfd.Filter = "png files (*.png)|*.png";
            sfd.Title = "Save Screen Shot";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllBytes(sfd.FileName, imgData);
            }
#elif UNITY_STANDALONE_WIN
            sfd.Filter = "png files (*.png)|*.png";
            sfd.Title = "Save Screen Shot";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllBytes(sfd.FileName, imgData);
            }
#elif UNITY_WEBGL
            string fileName = "PRISM_" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss")+"_img.png";
            DownloadFile(imgData, imgData.Length, fileName);
#endif
        }

        /// <summary>
        /// Used to export the pdf report
        /// </summary>
        /// <param name="data">The data to be included in the report</param>
        /// <param name="fileName">The name of the report</param>
        public void ExportReport(string data, string fileName)
        {
#if UNITY_WEBGL
            ExportPdfReport(data, fileName);
#endif
        }

        /// <summary>
        /// Used by the browser to display notifications
        /// </summary>
        /// <param name="not">The notification to be displayed</param>
        public void NotificationHelp(string not)
        {
#if UNITY_WEBGL
            DisplayNotification(not);
#endif
        }


        //Credit: https://forum.unity.com/threads/webgl-read-write.336171/
        /// <summary>
        /// Called from the browser when the context file has been selected
        /// </summary>
        /// <param name="url">The url of the file</param>
        public void ContextFileSelected(string url)
        {
            StartCoroutine(LoadContextText(url));
        }

        /// <summary>
        /// Called from the browser when the save file has been selected
        /// </summary>
        /// <param name="url">The url of the file</param>
        public void SaveFileSelected(string url)
        {
            StartCoroutine(LoadSaveText(url));
        }

        /// <summary>
        /// Sets the context file to be loaded
        /// </summary>
        public void SetContextFile()
        {
#if UNITY_EDITOR
            ofd.Filter = "obj files (*.obj)|*.obj|All files (*.*)|*.*";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                contextData = File.ReadAllText(ofd.FileName);
            }
            if (contextDataLoaded != null)
            {
                contextDataLoaded();
            }
#elif UNITY_WEBGL
        switch (resourcesLocation)
        {
            case ResourcesLocation.GetFromURL:
                TextUploaderCaptureClick(gameObject.name, "ContextFileSelected", "contextFiles");
                break;
            case ResourcesLocation.GetFromStreamingAssets:
                ofd.Filter = "json files (*.json)|*.json|All files (*.*)|*.*";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    contextData = File.ReadAllText(ofd.FileName);
                }
                if (contextDataLoaded != null)
                {
                    contextDataLoaded();
                }
                break;
        }
#elif UNITY_STANDALONE_WIN
        ofd.Filter = "obj files (*.obj)|*.obj|All files (*.*)|*.*";
        if(ofd.ShowDialog()== DialogResult.OK)
        {
            contextData = File.ReadAllText(ofd.FileName);
            contextLoading = false;
            contextLabel.text += " (set)";
        }
#endif
        }

        /// <summary>
        /// Sets the save file to be loaded
        /// </summary>
        public void SetSaveFiles()
        {
#if UNITY_EDITOR
            ofd.Filter = "json files (*.json)|*.json|All files (*.*)|*.*";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                saveData = File.ReadAllText(ofd.FileName);
            }
#elif UNITY_WEBGL
        switch (resourcesLocation)
        {
            case ResourcesLocation.GetFromURL:
                NotificationHelp("A browse button has been added to the top middle of the page in order for you to set the file.");
                TextUploaderCaptureClick(gameObject.name, "SaveFileSelected", "layoutsFiles");
                break;
            case ResourcesLocation.GetFromStreamingAssets:
                ofd.Filter = "json files (*.json)|*.json|All files (*.*)|*.*";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    saveData = File.ReadAllText(ofd.FileName);
                }
                break;
        }
#elif UNITY_STANDALONE_WIN
        ofd.Filter = "json files (*.json)|*.json|All files (*.*)|*.*";
        if (ofd.ShowDialog() == DialogResult.OK)
        {
            layoutsData = File.ReadAllText(ofd.FileName);
            layoutLoading = false;
            layoutLabel.text += " (set)";
        }
#endif
        }

        /// <summary>
        /// Called when the application is being reset
        /// </summary>
        public void OnReset()
        {
            saveData = null;
            contextData = null;
            stillLoading = true;
            waitingData = true;
            SceneManager.LoadScene(0, LoadSceneMode.Single);
        }

        /// <summary>
        /// Saves the session data
        /// </summary>
        /// <param name="newSave">The data to be saved</param>
        /// <param name="saveFileName">The name of the save file</param>
        public void OnSaveData(Save newSave, string saveFileName)
        {
            string saveJson = JsonConvert.SerializeObject(newSave, Formatting.Indented, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
#if UNITY_EDITOR
            sfd.Filter = "json files (*.json)|*.json|All files (*.*)|*.*";
            sfd.Title = "Save Building Json";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(sfd.FileName, saveJson);
            }
#elif UNITY_WEBGL
        switch (resourcesLocation)
        {
            case ResourcesLocation.GetFromURL:
                DownloadText(saveJson, saveFileName + ".json", "text/plain");
                break;
            case ResourcesLocation.GetFromStreamingAssets:
                sfd.Filter = "json files (*.json)|*.json|All files (*.*)|*.*";
                sfd.Title = "Save Building Json";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(sfd.FileName, saveJson);
                }
                break;
        }
#elif UNITY_STANDALONE_WIN
        sfd.Filter = "json files (*.json)|*.json|All files (*.*)|*.*";
                sfd.Title = "Save Building Json";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(sfd.FileName, saveJson);
                }
#endif
        }

        /// <summary>
        /// Exports the model data
        /// </summary>
        /// <param name="data">The obj dat to be exported</param>
        /// <param name="saveFileName">The name of the obj file</param>
        public void OnExportModel(string data, string saveFileName)
        {
#if UNITY_EDITOR
            sfd.Filter = "obj files (*.obj)|*.obj|All files (*.*)|*.*";
            sfd.Title = "Save Building Json";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(sfd.FileName, data);
            }
#elif UNITY_WEBGL
        switch (resourcesLocation)
        {
            case ResourcesLocation.GetFromURL:
                DownloadText(data, saveFileName + ".obj", "text/plain");
                break;
            case ResourcesLocation.GetFromStreamingAssets:
                sfd.Filter = "obj files (*.obj)|*.obj|All files (*.*)|*.*";
                sfd.Title = "Save Building Json";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(sfd.FileName, data);
                }
                break;
        }
#elif UNITY_STANDALONE_WIN
        sfd.Filter = "obj files (*.obj)|*.obj|All files (*.*)|*.*";
                sfd.Title = "Save Building Json";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(sfd.FileName, data);
                }
#endif
        }

        /// <summary>
        /// Exports the building raw metrics data
        /// </summary>
        /// <param name="data">The raw data to be exported</param>
        /// <param name="saveFileName">The name of the data file</param>
        public void OnExportBuildingData(string data, string saveFileName)
        {
#if UNITY_EDITOR
            sfd.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
            sfd.Title = "Save Building Data";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(sfd.FileName, data);
            }
#elif UNITY_WEBGL
        switch (resourcesLocation)
        {
            case ResourcesLocation.GetFromURL:
                DownloadText(data, saveFileName + ".csv", "text/plain");
                break;
            case ResourcesLocation.GetFromStreamingAssets:
                sfd.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
                sfd.Title = "Save Building Data";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(sfd.FileName, data);
                }
                break;
        }
#elif UNITY_STANDALONE_WIN
        sfd.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
                sfd.Title = "Save Building Data";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(sfd.FileName, data);
                }
#endif
        }

        public void OnExportModulesData(string data, string saveFileName)
        {
#if UNITY_EDITOR
            sfd.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
            sfd.Title = "Save Modules Data";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(sfd.FileName, data);
            }
#elif UNITY_WEBGL
        switch (resourcesLocation)
        {
            case ResourcesLocation.GetFromURL:
                DownloadText(data, saveFileName + ".csv", "text/plain");
                break;
            case ResourcesLocation.GetFromStreamingAssets:
                sfd.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
                sfd.Title = "Save Modules Data";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(sfd.FileName, data);
                }
                break;
        }
#elif UNITY_STANDALONE_WIN
        sfd.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
                sfd.Title = "Save Modules Data";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(sfd.FileName, data);
                }
#endif
        }

        /// <summary>
        /// Exports the floor raw metrics data
        /// </summary>
        /// <param name="data">The raw data to be exported</param>
        /// <param name="saveFileName">The name of the data file</param>
        public void OnExportFloorData(string data, string saveFileName)
        {
#if UNITY_EDITOR
            sfd.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
            sfd.Title = "Save Floor Data";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(sfd.FileName, data);
            }
#elif UNITY_WEBGL
        switch (resourcesLocation)
        {
            case ResourcesLocation.GetFromURL:
                DownloadText(data, saveFileName + ".csv", "text/plain");
                break;
            case ResourcesLocation.GetFromStreamingAssets:
                sfd.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
                sfd.Title = "Save Floor Data";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(sfd.FileName, data);
                }
                break;
        }
#elif UNITY_STANDALONE_WIN
        sfd.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
                sfd.Title = "Save Floor Data";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(sfd.FileName, data);
                }
#endif
        }
#endregion

#region Private Methods
        private IEnumerator LoadSaveText(string url)
        {
            WWW sav = new WWW(url);
            yield return sav;
            saveData = sav.text;
        }

        private IEnumerator LoadContextText(string url)
        {
            WWW sav = new WWW(url);
            yield return sav;
            contextData = sav.text;
            if (contextDataLoaded != null)
            {
                contextDataLoaded();
            }
        }
#endregion

        
    }
}
