﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    public class ScatterPlot : MonoBehaviour
    {

        public int gap = 10;
        public int minRadius = 10;
        public int maxRadius = 30;
        public Sprite pointImage;
        public int maxX { get; set; }
        public int maxY { get; set; }
        public Dictionary<string, int> numbers { get; set; }

        private Texture2D texture { get; set; }
        private int width { get; set; }
        private int height { get; set; }


        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void SetValues(List<List<float>> values, List<Color> colors)
        {
            Rect rect = GetComponent<Image>().rectTransform.rect;
            texture = new Texture2D((int)rect.width, (int)rect.height, TextureFormat.ARGB32, false);
            width = texture.width - 2 * gap;
            height = texture.height - 2 * gap;

            ResetImage();
            ImageLine(new Vector2(gap, gap), new Vector2(texture.width - gap, gap), Color.black, texture);
            ImageLine(new Vector2(gap, gap), new Vector2(gap, texture.height - gap), Color.black, texture);
            ImageLine(new Vector2(gap, texture.height - gap), new Vector2(texture.width - gap, texture.height - gap), Color.black, texture);
            ImageLine(new Vector2(texture.width - gap, gap), new Vector2(texture.width - gap, texture.height - gap), Color.black, texture);

            var widthInterval = width / (values.Count + 1);
            List<Dictionary<float, int>> numberOfDifference = new List<Dictionary<float, int>>();

            for (int i = 0; i < values.Count; i++)
            {
                Dictionary<float, int> differences = new Dictionary<float, int>();
                for (int j = 0; j < values[i].Count; j++)
                {
                    if (differences.ContainsKey(values[i][j]))
                    {
                        differences[values[i][j]]++;
                    }
                    else
                    {
                        differences.Add(values[i][j], 1);
                    }
                }
                numberOfDifference.Add(differences);
            }

            List<int> allDifferences = new List<int>();
            List<float> allSizes = new List<float>();
            for (int i = 0; i < numberOfDifference.Count; i++)
            {
                allDifferences.AddRange(numberOfDifference[i].Values.ToList());
                allSizes.AddRange(numberOfDifference[i].Keys.ToList());
            }
            allDifferences.Sort();
            allSizes.Sort();

            int maxDifference = allDifferences.Last();
            int minDifference = allDifferences[0];
            float maxSize = allSizes.Last();
            List<Dictionary<float, int>> radii = new List<Dictionary<float, int>>();

            for (int i = 0; i < numberOfDifference.Count; i++)
            {
                Dictionary<float, int> radius = new Dictionary<float, int>();
                foreach (var item in numberOfDifference[i])
                {
                    radius.Add(item.Key, (int)BrydenWoodUtils.Remap(item.Value, minDifference, maxDifference, minRadius, maxRadius));
                    //numberOfDifference[i][item.Key] = (int)Remap(item.Value, maxDifference);
                }
                radii.Add(radius);
            }

            for (int i = 0; i < radii.Count; i++)
            {
                int x = (i + 1) * widthInterval + gap;
                ImageLine(new Vector2(x, gap), new Vector3(x, gap + height), Color.black, texture);
                Color c = colors[i];
                foreach (var item in radii[i])
                {
                    int y = gap + (int)BrydenWoodUtils.Remap(item.Key, 0, maxSize, 0, height);
                    SetPixel(x, y, item.Value, c);
                }
            }
            UpdateUIImage();
        }

        private void SetPixel(int x, int y, int size, Color color)
        {
            for (float i = -size * 0.5f; i < size * 0.5f; i++)
            {
                for (float j = -size * 0.5f; j < size * 0.5f; j++)
                {
                    texture.SetPixel((int)(x + i), (int)(y + j), color);
                }
            }
        }

        public void UpdateUIImage()
        {
            texture.Apply();
            Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
            GetComponent<Image>().sprite = sprite;
        }

        public void ResetImage()
        {
            if (texture != null)
            {
                for (int i = 0; i < texture.width; i++)
                {
                    for (int j = 0; j < texture.height; j++)
                    {
                        texture.SetPixel(i, j, Color.white);
                    }
                }
                texture.Apply();
                Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
                GetComponent<Image>().sprite = sprite;
            }
        }

        public static void ImageLine(Vector2 start, Vector2 end, Color color, Texture2D texture)
        {
            for (float i = 0; i <= 1.0f; i += 0.001f)
            {
                Vector2 point = Vector2.Lerp(start, end, i);
                texture.SetPixel((int)point.x, (int)point.y, color);
            }
        }
    }
}
