﻿using UnityEngine.UI;
using System.Linq;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// Extensions for the Toggle Group component
    /// </summary>
    public static class ToggleGroupExtension
    {

        /// <summary>
        /// Returns the active element in the toggle group
        /// </summary>
        /// <param name="aGroup">The Toggle Group</param>
        /// <returns>Unity Toggle Component</returns>
        public static Toggle GetActive(this ToggleGroup aGroup)
        {
            return aGroup.ActiveToggles().FirstOrDefault();
        }
    }
}
