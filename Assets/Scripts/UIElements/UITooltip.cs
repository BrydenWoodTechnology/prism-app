﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for specific notifications above UI Elements
    /// </summary>
    public class UITooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        
        public Text tooltip;

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Interface Methods
        public void OnPointerEnter(PointerEventData eventData)
        {
            //if (tooltip.text != "")
            //{
            //    tooltip.gameObject.SetActive(true);
            //}
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            //tooltip.gameObject.SetActive(false);
        }
        #endregion
    }
}
